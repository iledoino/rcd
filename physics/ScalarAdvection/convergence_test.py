import os, sys, getopt
import numpy as np
from matplotlib import pyplot as plt


def main(argv):
    alpha = 1.0
    c0 = 1.0
    nxinit = 400
    ntinit = 10
    nconvtests = 6
    tf = 1.0
    typeofconv = "xt"
    opts, args = getopt.getopt(argv, "h", ["alpha=", "c0=", "nxinit=", "ntinit=", "tf=", "nconvtests=", "typeofconv="])
    for opt, arg in opts:
      if opt == '-h':
         print('convergence_test.py --alpha <alpha_value> --c0 <c0_value> --nxinit <initial_nof_x_points> --ntinit <initial_nof_t_points> --tf <final_time_value> --nconvtests <nof_conv_tests> --typeofconv <x, t, or xt>')
         sys.exit()
      elif opt == "--alpha":
         alpha = float(arg)
      elif opt == "--c0":
         c0 = float(arg)
      elif opt == "--nxinit":
         nxinit = int(arg)
      elif opt == "--ntinit":
         ntinit = int(arg)
      elif opt == "--tf":
         tf = float(arg)
      elif opt == "--nconvtests":
         nconvtests = int(arg)
      elif opt == "--typeofconv":
         if arg == 'x':
             typeofconv = "x"
         elif arg == 't':
             typeofconv = "t"
         elif arg == 'xt':
             typeofconv = "xt"
    print('alpha: ' + str(alpha))
    print('c0: ' + str(c0))
    print('nxinit: ' + str(nxinit))
    print('ntinit: ' + str(ntinit))
    print('tf: ' + str(tf))
    print('nconvtests: ' + str(nconvtests))
    print('typeofconv: ' + typeofconv)

    return alpha, c0, nxinit, ntinit, tf, nconvtests, typeofconv

if __name__ == "__main__":
    [alpha, c0, nxinit, ntinit, tf, nconvtests, typeofconv] = main(sys.argv[1:])

    def solution(xv, t):
        return [(np.sin(np.pi*(x-c0*t))**4)/(x**alpha) if ((x>c0*t) and (x<(c0*t+1.0))) else 0.0 for x in xv]

    nxarray = np.array([nxinit*(2**i)+1 if typeofconv=="x" or typeofconv=="xt" else nxinit for i in range(nconvtests)])
    ntarray = np.array([ntinit*(2**i)+1 if typeofconv=="t" or typeofconv=="xt" else ntinit for i in range(nconvtests)])

    dxs = []
    dts = []
    errors = []
    inDataFilename = 'inData.in'
    inConvTestFilename = 'inDataConvTest.in'
    simConfigFilename = 'cfgsol.out'
    simFilename = 'output.in'
    for nx, nt in zip(nxarray, ntarray):

        # copy contents of base in file to the conv test file
        with open(inDataFilename,'r') as inDataFile, open(inConvTestFilename,'w') as convTestFile:
            for line in inDataFile:
                convTestFile.write(line)

        # override some values on the conv test file
        with open(inConvTestFilename, 'a') as convTestFile:
            convTestFile.write('*ALPHA ' + str(alpha) + '\n')
            convTestFile.write('*C0 ' + str(c0) + '\n')
            convTestFile.write('*USENSMP 1\n')
            convTestFile.write('*NSPCMESHPTS ' + str(nx) + '\n')
            convTestFile.write('*USENTMP 1\n')
            convTestFile.write('*NTIMEMESHPTS ' + str(nt) + '\n')
            convTestFile.write('*FINALTIME ' + str(tf) + '\n')
            convTestFile.write('*SOLOUTPUTFREQUENCY ' + str(nt) + '\n')
            convTestFile.write('*SHOULDPLOT 0\n')

        # runs the simulation
        command = './advec -i ' + inConvTestFilename
        stream = os.popen('./advec -i ' + inConvTestFilename)
        output = stream.read()
        print(output)

        # reads from the configuration file
        with open(simConfigFilename) as f:
            nofsol, noftimes, nofpoints = np.fromfile(f, dtype=int, count=3, sep="\n")

        # reads from the solution file
        times = np.zeros(noftimes)
        solutions = np.zeros(shape=(noftimes, nofpoints, nofsol+1))
        with open(simFilename) as f:
            for t in range(noftimes):
                times[t] = np.fromfile(f, count=1, sep=" ")
                solutions[t] = np.fromfile(f, count=nofpoints*(nofsol+1), sep=" ").reshape((nofpoints, nofsol+1))

        # calculates the errors
        xtf = solutions[-1,:,0]
        solutiontf = solutions[-1,:,1]
        errortf = np.linalg.norm(solutiontf-solution(xtf, times[-1]), ord=np.inf)

        dxs = dxs + [xtf[1]-xtf[0]]
        dts = dts + [tf/nt]
        errors = errors + [errortf]

    if typeofconv=="x" or typeofconv=="xt":
        logdx = np.log10(dxs)
        logerror = np.log10(errors)
        plt.plot(-logdx, logerror)
        plt.title("Convergence test on x-coordinate")
        plt.xlabel("-log10(x)")
        plt.ylabel("log10(error)")
        plt.show()
        print("convergence rate in x: {}".format(np.divide(logerror[:-1]-logerror[1:], logdx[:-1]-logdx[1:])))

    if typeofconv=="t" or typeofconv=="xt":
        logdt = np.log10(dts)
        logerror = np.log10(errors)
        plt.plot(-logdt, logerror)
        plt.title("Convergence test on t-coordinate")
        plt.xlabel("-log10(t)")
        plt.ylabel("log10(error)")
        plt.show()
        print("convergence rate in t: {}".format(np.divide(logerror[:-1]-logerror[1:], logdt[:-1]-logdt[1:])))
