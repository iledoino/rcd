#include "RCDUtil.h"
#include "RiemannProblem.h"
#include "bdry.h"
#include "dct.h"
#include "keymatch.h"
#include "newton.h"
#include <cassert>
#include <cmath>
#include <fstream>
#include <iomanip>
#include <limits>

#ifdef ENABLE_GNUPLOT
#include "GnuplotReporter.h"
#endif
#ifdef ENABLE_MATPLOTLIB
#include "MatplotlibReporter.h"
#endif
#ifdef ENABLE_MULTID
#include "MultidReporter.h"
#endif
#include "FileReporter.h"

class ScalarAdvectionCore
{
public:
    ScalarAdvectionCore(const std::vector<double>& parameters_p)
        : parameters_(parameters_p)
    {
        assert(parameters_p.size() >= 3);
    }

protected:
    double solution(const double& x, const double& t) const
    {
        if(x>c0()*t && x<(c0()*t+1.0))
            return std::pow(std::sin(M_PI*(x-c0()*t)), 4.0)/std::pow(x, alpha());
        return 0.0;
    }

    const std::vector<double>& parameters_;

    double alpha() const { return parameters_[0]; }
    double c0() const { return parameters_[1]; }

    // this parameter controls numeric diffusion, to be used if Crank-Nicolson scheme is to be applied
    double diffeps() const { return parameters_[2]; }
};

/*
 * Scalar advection equation (x^a u)_t + c(x^a u)_x = 0,
 * with a=0 for cartesian coordinates, a=1 for cylindrical coordinates,
 * and a=2 for spherical coordinates (see https://arxiv.org/pdf/1701.04834.pdf)
 */
class ScalarAdvectionPhysics : public PhysicsRCD, public ScalarAdvectionCore
{
public:
    void setConstantValues(PhysicsDataHolder& phyDH) override;
    void
    jet(PhysicsDataHolder& phyDH, double* Vj, double* Un, double& p, double& u,
            const double& x, const double& t, const std::size_t& idx) override;
    void setPDEAndCstFunctions(PhysicsDataHolder& phyDH, double* Un, double& p,
            double& u, const double& x, const double& t,
            const std::size_t& idx) override;

    ScalarAdvectionPhysics(const std::vector<double>& parameters_p)
        : PhysicsRCD(), ScalarAdvectionCore(parameters_p)
    {
    }
};

void
ScalarAdvectionPhysics::setConstantValues(PhysicsDataHolder& /* phyDH */)
{
}

void
ScalarAdvectionPhysics::setPDEAndCstFunctions(PhysicsDataHolder& phyDH, double* Un,
        double& /* p */, double& /* uvel */, const double& x,
        const double& /* t */, const std::size_t& /* idx */)
{
    const double& u = Un[0];

    phyDH.G[0] = std::pow(x, alpha())*u;

    phyDH.F[0] = c0()*std::pow(x, alpha())*u;

    phyDH.B[0] = diffeps();

    phyDH.R[0] = 0.0;
}

void
ScalarAdvectionPhysics::jet(PhysicsDataHolder& phyDH, double* Vj, double* /* Un */, double& p,
        double& uvel, const double& x, const double& t, const std::size_t& idx)
{
    setPDEAndCstFunctions(phyDH, Vj, p, uvel, x, t, idx);

    phyDH.DG[0] = std::pow(x, alpha());

    phyDH.DF[0] = c0()*std::pow(x, alpha());

    phyDH.DB[0] = 0.0;

    phyDH.DR[0] = 0.0;
}

class ScalarAdvectionLeftBC : public PDE_RobinBC_FO_L, public ScalarAdvectionCore
{
public:
    ScalarAdvectionLeftBC(const std::vector<double>& parameters_p, const double& dx_, const double alpha_ = 1.0)
    : PDE_RobinBC_FO_L(1, 0, dx_, alpha_), ScalarAdvectionCore(parameters_p)
    {}

private:
    void SetBoundaryValues(double x, double t) override
    {
        a[0] = 1.0;
        b[0] = 0.0;
        c[0] = - solution(x, t);
    }
};

class ScalarAdvectionRightBC : public PDE_RobinBC_FO_R, public ScalarAdvectionCore
{
public:
    ScalarAdvectionRightBC(const std::vector<double>& parameters_p, const double& dx_, const double alpha_ = 1.0)
    : PDE_RobinBC_FO_R(1, 0, dx_, alpha_), ScalarAdvectionCore(parameters_p)
    {}

private:
    void SetBoundaryValues(double x, double t) override
    {
        a[0] = 1.0;
        b[0] = 0.0;
        c[0] = - solution(x, t);
    }
};

class ScalarAdvectionProblem : public RiemannProblem, public ScalarAdvectionCore
{
public:
    ScalarAdvectionProblem(const std::vector<Bdry*>& bdry_p, ScalarAdvectionPhysics& phys_p,
            Reporter& rptr_p, const std::vector<double>& parameters_p)
    : RiemannProblem(bdry_p, phys_p, rptr_p, 1), ScalarAdvectionCore(parameters_p)
    {}

    void setInitialSolution() override
    {
        // calling the base so that it fills the x vector for us
        RiemannProblem::setInitialSolution();

        if (dct_ && dct_->x && dct_->V)
        {
            for (std::size_t i = 0; i < static_cast<std::size_t>(dct_->nSpcMeshPts()); i++)
            {
                dct_->V[i]  = solution(dct_->x[i], 0.0);
            }
        }
    }
};

int
main(int argc, char** argv)
{
    const std::size_t PDEDim = 1, CSTDim = 0;
    bool hasPressure = false;
    bool hasVelocity = false;

    /*
     * parameters of the physics
     */
    std::vector<double> parameters(3);

    /*
     * Configuration of the discretization scheme
     */
    int hasFlux, fluxUpwind, hasDiffusion, diffConstant, hasReaction,
            cstIsRelaxed;
    double fluxTimeAvg, diffTimeAvg, reacTimeAvg, cstrTimeAvg, cstRelaxation;

    /*
     * left and right states
     */
    std::vector<double> leftState(PDEDim + CSTDim, 0.),
            rightState(PDEDim + CSTDim, 0.);

    /*
     * Configuration of the Riemann Problem simulation
     */
    int convergencePlot, adaptiveCFL, adapCFLNumDerivs, automaticSetUp;
    int useNSMP, useNTMP, parallel;
    std::size_t nSpcMeshPts, nTimeMeshPts, solOutputFrequency, adapCFLFrequency;
    double spcMeshStpSize, timeMeshStpSize, leftSpcDelim, rightSpcDelim,
            finalTime, cflCondPercent, speedPercentage;
    double ZEROTOL, DIFFTOL;

    /*
     * Selects whether to test derivatives or not
     */
    int TESTDERIVS;

    /*
     * percentage of newton time steps
     */
    std::vector<double> STEPPERCENT(PDEDim + CSTDim, 1.);

    /*
     * wether to plot or not (for convergence test purposes)
     */
    int shouldPlot = false;

    /*
     * Class to read parameters from file
     */
    KeyMatch keyMatch;

    /*
     * From arguments provided from command line, decides which file use
     * as input file for the parameters below
     */
    keyMatch.define_in_out(argc, argv);

    /*
     * Registration of entries to be read from file and their key names
     * PS: Register your parameters down below, so that they can be read
     * from files
     */
    keyMatch.register_entry(parameters[0], "*ALPHA");
    keyMatch.register_entry(parameters[1], "*C0");
    keyMatch.register_entry(parameters[2], "*DIFFEPS");

    keyMatch.register_entry(hasFlux, "*HASFLUX");
    keyMatch.register_entry(fluxUpwind, "*FLUXUPWIND");
    keyMatch.register_entry(hasDiffusion, "*HASDIFFUSION");
    keyMatch.register_entry(diffConstant, "*DIFFCONSTANT");
    keyMatch.register_entry(hasReaction, "*HASREACTION");
    keyMatch.register_entry(cstIsRelaxed, "*CSTISRELAXED");
    keyMatch.register_entry(fluxTimeAvg, "*FLUXTIMEAVG");
    keyMatch.register_entry(diffTimeAvg, "*DIFFTIMEAVG");
    keyMatch.register_entry(reacTimeAvg, "*REACTIMEAVG");
    keyMatch.register_entry(cstrTimeAvg, "*CSTRTIMEAVG");
    keyMatch.register_entry(cstRelaxation, "*CSTRELAXATION");

    keyMatch.register_entry(automaticSetUp, "*AUTOMATICSETUP");

    keyMatch.register_entry(leftState, "*LEFTSTATE");
    keyMatch.register_entry(rightState, "*RIGHTSTATE");


    keyMatch.register_entry(useNSMP, "*USENSMP");
    keyMatch.register_entry(nSpcMeshPts, "*NSPCMESHPTS");
    keyMatch.register_entry(spcMeshStpSize, "*SPCMESHSTPSIZE");
    keyMatch.register_entry(leftSpcDelim, "*LEFTSPCDELIM");
    keyMatch.register_entry(rightSpcDelim, "*RIGHTSPCDELIM");

    keyMatch.register_entry(useNTMP, "*USENTMP");
    keyMatch.register_entry(nTimeMeshPts, "*NTIMEMESHPTS");
    keyMatch.register_entry(timeMeshStpSize, "*TIMEMESHSTPSIZE");
    keyMatch.register_entry(finalTime, "*FINALTIME");
    keyMatch.register_entry(cflCondPercent, "*CFLCONDPERCENT");
    keyMatch.register_entry(adaptiveCFL, "*ADAPTIVECFL");
    keyMatch.register_entry(adapCFLNumDerivs, "*ADAPTCFLNUMDERIVS");
    keyMatch.register_entry(adapCFLFrequency, "*ADAPTCFLFREQUENCY");
    keyMatch.register_entry(speedPercentage, "*SPEEDPERCENTAGE");

    keyMatch.register_entry(ZEROTOL, "*ZEROTOL");
    keyMatch.register_entry(DIFFTOL, "*DIFFTOL");

    keyMatch.register_entry(parallel, "*PARALLEL");
    keyMatch.register_entry(convergencePlot, "*CONVERGENCEPLOT");
    keyMatch.register_entry(solOutputFrequency, "*SOLOUTPUTFREQUENCY");

    keyMatch.register_entry(TESTDERIVS, "*TESTDERIVS");
    keyMatch.register_entry(STEPPERCENT, "*STEPPERCENT");

    keyMatch.register_entry(shouldPlot, "*SHOULDPLOT");

    /*
     * Read parameters from file
     */
    keyMatch.make();

    /*
     * Class used to set the physics of the problem
     */
    ScalarAdvectionPhysics phys(parameters);

    /*
     * Tests if derivatives have been implemented correctly by comparing
     * them with finite differences
     */
    if (TESTDERIVS) {
        derivtest(phys, leftState, rightState, PDEDim, CSTDim, TESTDERIVS == 2);
        return 0;
    }

    std::size_t nOfVariables = PDEDim + CSTDim;

    if (hasPressure)
        ++nOfVariables;
    if (hasVelocity)
        ++nOfVariables;

    Reporters reporters;

#if defined(ENABLE_GNUPLOT) || defined(ENABLE_MATPLOTLIB)                      \
        || defined(ENABLE_MULTID)
    std::vector<std::vector<std::size_t>> reportGroups(nOfVariables);
    std::vector<std::string> yAxesNames(nOfVariables);
    for (std::size_t i = 0; i < nOfVariables; ++i) {
        reportGroups[i].push_back({ i });
        yAxesNames[i] = "var" + std::to_string(i + 1);
    }
    std::string xAxisName("x");
#endif
#ifdef ENABLE_GNUPLOT
    GnuplotReporter gnurptr(reportGroups, xAxisName, yAxesNames);
    if (shouldPlot) {
        reporters.addReporter(gnurptr);
    }
#endif
#ifdef ENABLE_MATPLOTLIB
    MatplotlibReporter matplotlibrptr(reportGroups, xAxisName, yAxesNames);
    if (shouldPlot) {
        matplotlibrptr.frameSecUpdate() = 0.5;
        reporters.addReporter(matplotlibrptr);
    }
#endif
#ifdef ENABLE_MULTID
    MultidReporter multidrptr(&argc, argv, "Scalar Advection Space", nOfVariables,
            reportGroups, xAxisName, yAxesNames);
    if (shouldPlot) {
        reporters.addReporter(multidrptr);
    }
#endif

    FileReporter frptr(keyMatch.output(), solOutputFrequency);
    reporters.addReporter(frptr);

    std::vector<Bdry*> myBCs(2);
    ScalarAdvectionProblem rp(
            myBCs, phys, reporters, parameters);

    RiemannProblemConfig rpc;
    rpc.automaticSetUp() = automaticSetUp;
    rpc.leftState() = leftState;
    rpc.rightState() = rightState;
    rpc.flowDirection() = fluxUpwind ? FlowDirection::POSITIVE :
                                       FlowDirection::BIDIRECTIONAL;
    rpc.useNSMP() = useNSMP;
    rpc.nSpcMeshPts() = nSpcMeshPts;
    rpc.spcMeshStpSize() = spcMeshStpSize;
    rpc.leftSpcDelim() = leftSpcDelim;
    rpc.rightSpcDelim() = rightSpcDelim;
    rpc.useNTMP() = useNTMP;
    rpc.nTimeMeshPts() = nTimeMeshPts;
    rpc.timeMeshStpSize() = timeMeshStpSize;
    rpc.infTimeDelim() = finalTime;
    rpc.cflCondPercent() = cflCondPercent;
    rpc.speedPercentage() = speedPercentage;
    rpc.convergencePlot() = convergencePlot;
    rpc.adaptiveCFL() = adaptiveCFL;
    rpc.adapCFLNumDerivs() = adapCFLNumDerivs;
    rpc.adapCFLFrequency() = adapCFLFrequency;
    rpc.parallel() = parallel;

    PDEMemberConfig pdemc;
    pdemc.hasFlux() = hasFlux;
    pdemc.fluxIsUpwind() = fluxUpwind;
    pdemc.impParamFlux() = fluxTimeAvg;
    pdemc.hasDiffusion() = hasDiffusion;
    pdemc.diffIsConstant() = diffConstant;
    pdemc.impParamDiffusion() = diffTimeAvg;
    pdemc.hasReaction() = hasReaction;
    pdemc.impParamReaction() = reacTimeAvg;
    pdemc.impParamConstraint() = cstrTimeAvg;
    pdemc.constraintIsRelaxed() = cstIsRelaxed;
    pdemc.constraintRelaxation() = cstRelaxation;

    rp.setRPC(rpc);
    rp.setPDEMC(pdemc);

    // IMPORTANT: fixing stabilization
    parameters[2] *= rp.getRPC().spcMeshStpSize();
    myBCs[0] = new ScalarAdvectionLeftBC(
            parameters, rp.getRPC().spcMeshStpSize());
    myBCs[1] = new ScalarAdvectionRightBC(
            parameters, rp.getRPC().spcMeshStpSize());
    rp.checkForConvergence() = false;
    rp.checkForChangesAtBoundary() = false;

    rp.startCalc("Scalar Advection Physics", 0., ZEROTOL, DIFFTOL);

    delete myBCs[0];
    delete myBCs[1];

    return 0;
}
