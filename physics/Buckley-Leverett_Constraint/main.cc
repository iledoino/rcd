#include "RCDUtil.h"
#include "RiemannProblem.h"
#include "bdry.h"
#include "dct.h"
#include "keymatch.h"
#include "newton.h"
#include <cmath>
#include <fstream>
#include <iomanip>
#include <limits>

#ifdef ENABLE_GNUPLOT
#include "GnuplotReporter.h"
#endif
#ifdef ENABLE_MATPLOTLIB
#include "MatplotlibReporter.h"
#endif
#ifdef ENABLE_MULTID
#include "MultidReporter.h"
#endif
#include "FileReporter.h"

#define CONSTRAINT_AS_SOURCE_FUNCTION

class BL : public PhysicsRCD
{
public:
    void setConstantValues(PhysicsDataHolder& phyDH) override;
    void
    jet(PhysicsDataHolder& phyDH, double* Vj, double* Un, double& p, double& u,
            const double& x, const double& t, const std::size_t& idx) override;
    void setPDEAndCstFunctions(PhysicsDataHolder& phyDH, double* Un, double& p,
            double& u, const double& x, const double& t,
            const std::size_t& idx) override;

    const double a, e, eps;

    BL(const double& a_, const double& e_, const double& eps_)
        : PhysicsRCD(), a(a_), e(e_), eps(eps_)
    {
    }
};

void
BL::setConstantValues(PhysicsDataHolder& phyDH)
{
    phyDH.DG[0] = 1.0;
#ifdef CONSTRAINT_AS_SOURCE_FUNCTION
    phyDH.DG[2] = e;
    phyDH.DG[3] = e;
    phyDH.DR[0] = 1.0;
    phyDH.DR[1] = 1.0;
#else
    phyDH.DH[0] = 1.0;
    phyDH.DH[1] = 1.0;
#endif
}

void
BL::setPDEAndCstFunctions(PhysicsDataHolder& phyDH, double* Un, double& /* p */,
        double& /* u */, const double& /* x */, const double& /* t */,
        const std::size_t& /* idx */)
{
    phyDH.G[0] = Un[0];
    phyDH.F[0] = (Un[0] * Un[0]) / (Un[0] * Un[0] + a * Un[1] * Un[1]);
#ifdef CONSTRAINT_AS_SOURCE_FUNCTION
    phyDH.G[1] = e * (Un[0] + Un[1] - 1.0);
    phyDH.R[0] = Un[0] + Un[1] - 1.0;
#else
    phyDH.H[0] = Un[0] + Un[1] - 1.0;
#endif

    phyDH.B[0] = eps;
}

void
BL::jet(PhysicsDataHolder& phyDH, double* Vj, double* /* Un */, double& /* p */,
        double& /* u */, const double& /* x */, const double& /* t */,
        const std::size_t& /* idx */)
{
    double ratio = 1.0 / (Vj[0] * Vj[0] + a * Vj[1] * Vj[1]);

    phyDH.G[0] = Vj[0];
    phyDH.F[0] = (Vj[0] * Vj[0]) * ratio;
#ifdef CONSTRAINT_AS_SOURCE_FUNCTION
    phyDH.G[1] = e * (Vj[0] + Vj[1] - 1.0);
    phyDH.R[0] = Vj[0] + Vj[1] - 1.0;
#else
    phyDH.H[0] = Vj[0] + Vj[1] - 1.0;
#endif

    phyDH.DF[0] = 2.0 * a * Vj[0] * Vj[1] * Vj[1] * ratio * ratio;
    phyDH.DF[1] = -2.0 * a * Vj[1] * Vj[0] * Vj[0] * ratio * ratio;

    phyDH.B[0] = eps;
}

int
main(int argc, char* argv[])
{
#ifdef CONSTRAINT_AS_SOURCE_FUNCTION
    const std::size_t PDEDim = 2, CSTDim = 0;
#else
    const std::size_t PDEDim = 1, CSTDim = 1;
#endif
    bool hasPressure = false;
    bool hasVelocity = false;

    /*
     * parameters of the physics
     */
    std::vector<double> parameters(3);

    /*
     * Configuration of the discretization scheme
     */
#ifdef CONSTRAINT_AS_SOURCE_FUNCTION
    int hasDiffusion = 0, hasReaction = 1;
#else
    int hasDiffusion = 0, hasReaction = 0;
#endif
    int hasFlux, fluxUpwind, diffConstant, cstIsRelaxed;
    double fluxTimeAvg, diffTimeAvg, reacTimeAvg, cstrTimeAvg, cstRelaxation;

    /*
     * left and right states
     */
    std::vector<double> leftState(PDEDim + CSTDim, 0.),
            rightState(PDEDim + CSTDim, 0.);

    /*
     * Configuration of the Riemann Problem simulation
     */
    int useNSMP, parallel, convergencePlot, adaptiveCFL, adapCFLNumDerivs;
    std::size_t nSpcMeshPts, solOutputFrequency, adapCFLFrequency;
    double spcMeshStpSize, finalTime, cflCondPercent;
    double speedPercentage;

    /*
     * Selects whether to test derivatives or not
     */
    int TESTDERIVS;

    /*
     * percentage of newton time steps
     */
    std::vector<double> STEPPERCENT(PDEDim + CSTDim, 1.);

    /*
     * Class to read parameters from file
     */
    KeyMatch keyMatch;

    /*
     * From arguments provided from command line, decides which file use
     * as input file for the parameters below
     */
    keyMatch.define_in_out(argc, argv);

    /*
     * Registration of entries to be read from file and their key names
     */
    keyMatch.register_entry(parameters[0], "*A");
    keyMatch.register_entry(parameters[1], "*E");
    keyMatch.register_entry(parameters[2], "*STABEPSOVERDX");

    keyMatch.register_entry(hasFlux, "*HASFLUX");
    keyMatch.register_entry(fluxUpwind, "*FLUXUPWIND");
    keyMatch.register_entry(diffConstant, "*DIFFCONSTANT");
    keyMatch.register_entry(cstIsRelaxed, "*CSTISRELAXED");
    keyMatch.register_entry(fluxTimeAvg, "*FLUXTIMEAVG");
    keyMatch.register_entry(diffTimeAvg, "*DIFFTIMEAVG");
    keyMatch.register_entry(reacTimeAvg, "*REACTIMEAVG");
    keyMatch.register_entry(cstrTimeAvg, "*CSTRTIMEAVG");
    keyMatch.register_entry(cstRelaxation, "*CSTRELAXATION");

    keyMatch.register_entry(leftState, "*LEFTSTATE");
    keyMatch.register_entry(rightState, "*RIGHTSTATE");
    keyMatch.register_entry(useNSMP, "*USENSMP");
    keyMatch.register_entry(parallel, "*PARALLEL");
    keyMatch.register_entry(convergencePlot, "*CONVERGENCEPLOT");
    keyMatch.register_entry(nSpcMeshPts, "*NSPCMESHPTS");
    keyMatch.register_entry(solOutputFrequency, "*SOLOUTPUTFREQUENCY");
    keyMatch.register_entry(spcMeshStpSize, "*SPCMESHSTPSIZE");
    keyMatch.register_entry(finalTime, "*FINALTIME");
    keyMatch.register_entry(cflCondPercent, "*CFLCONDPERCENT");
    keyMatch.register_entry(adaptiveCFL, "*ADAPTIVECFL");
    keyMatch.register_entry(adapCFLNumDerivs, "*ADAPTCFLNUMDERIVS");
    keyMatch.register_entry(adapCFLFrequency, "*ADAPTCFLFREQUENCY");
    keyMatch.register_entry(speedPercentage, "*SPEEDPERCENTAGE");

    keyMatch.register_entry(TESTDERIVS, "*TESTDERIVS");
    keyMatch.register_entry(STEPPERCENT, "*STEPPERCENT");

    /*
     * Read parameters from file
     */
    keyMatch.make();

    /*
     * Physics
     */
    BL phys(parameters[0], parameters[1], parameters[2]);

    if (TESTDERIVS) {
        derivtest(phys, leftState, rightState, PDEDim, CSTDim, TESTDERIVS == 2);
        return 0;
    }

    std::size_t nOfVariables = PDEDim + CSTDim;

    if (hasPressure)
        ++nOfVariables;
    if (hasVelocity)
        ++nOfVariables;

    Reporters reporters;

#if defined(ENABLE_GNUPLOT) || defined(ENABLE_MATPLOTLIB)                      \
        || defined(ENABLE_MULTID)
    std::vector<std::vector<std::size_t>> reportGroups({ { 0, 1 } });
    std::vector<std::string> yAxesNames({ { std::string("s,1-s") } });
    std::string xAxisName("x");
#endif
#ifdef ENABLE_GNUPLOT
    GnuplotReporter gnurptr(reportGroups, xAxisName, yAxesNames);
    reporters.addReporter(gnurptr);
#endif
#ifdef ENABLE_MATPLOTLIB
    MatplotlibReporter matplotlibrptr(reportGroups, xAxisName, yAxesNames);
    matplotlibrptr.frameSecUpdate() = 0.5;
    reporters.addReporter(matplotlibrptr);
#endif
#ifdef ENABLE_MULTID
    MultidReporter multidrptr(&argc, argv, "BL Space", nOfVariables,
            reportGroups, xAxisName, yAxesNames);
    reporters.addReporter(multidrptr);
#endif

    FileReporter frptr(keyMatch.output(), solOutputFrequency);
    reporters.addReporter(frptr);

    RiemannProblem rp(
            phys, reporters, PDEDim, CSTDim, hasPressure, hasVelocity);

    RiemannProblemConfig rpc;
    rpc.leftState() = leftState;
    rpc.rightState() = rightState;
    rpc.flowDirection() = fluxUpwind ? FlowDirection::POSITIVE :
                                       FlowDirection::BIDIRECTIONAL;
    rpc.nSpcMeshPts() = nSpcMeshPts;
    rpc.useNSMP() = useNSMP;
    rpc.infTimeDelim() = finalTime;
    rpc.cflCondPercent() = cflCondPercent;
    rpc.speedPercentage() = speedPercentage;
    rpc.parallel() = parallel;
    rpc.convergencePlot() = convergencePlot;
    rpc.adaptiveCFL() = adaptiveCFL;
    rpc.adapCFLNumDerivs() = adapCFLNumDerivs;
    rpc.adapCFLFrequency() = adapCFLFrequency;

    PDEMemberConfig pdemc;
    pdemc.hasFlux() = hasFlux;
    pdemc.fluxIsUpwind() = fluxUpwind;
    pdemc.impParamFlux() = fluxTimeAvg;
    pdemc.hasDiffusion() = hasDiffusion;
    pdemc.diffIsConstant() = diffConstant;
    pdemc.impParamDiffusion() = diffTimeAvg;
    pdemc.hasReaction() = hasReaction;
    pdemc.impParamReaction() = reacTimeAvg;
    pdemc.impParamConstraint() = cstrTimeAvg;
    pdemc.constraintIsRelaxed() = cstIsRelaxed;
    pdemc.constraintRelaxation() = cstRelaxation;

    rp.setRPC(rpc);
    rp.setPDEMC(pdemc);

    // IMPORTANT: fixing stabilization
    parameters[2] *= rp.getRPC().spcMeshStpSize();
    rp.checkForConvergence() = false;
    rp.checkForChangesAtBoundary() = false;

    rp.startCalc("Buckley-Leverett Physics");

    return 0;
}
