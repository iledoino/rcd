#!make
#!make run and in this way run before to change the input parameter

# definition of parameters of the physics
*A           1.
*GAMMA       2.
*E           0.07
*STABEPSOVERDX .5

# for when testing derivatives
*TESTDERIVS 0

# The following parameters are epsilon values that control the diffusion
#
# NUMDIFFEPSOVERDX is a percentage on the diffusion matrix dx*I, where
# dx is the space mesh step size and I is the identity matrix. This term
# acts as a stabilization term for Crank-Nicolson schemes
#
# DIFFMATRIXEPS is multiplied by the diffusion matrix implemented in
# physics: eps * B
#
*NUMDIFFEPSOVERDX 0.0
*DIFFMATRIXEPS 1.0e-10

# definition of constants to the riemann problem
*LEFTSTATE  2. 1. 1.
*RIGHTSTATE 1. 0.13962 1.

# Space mesh configuration: if USENSMP is set to 1, then RCD will
# use NSPCMESHPTS, which is the number of mesh points. If USENSMP is set
# to 0, then RCD will use SPCMESHSTPSIZE, which is the space mesh step
# size, comonly identified with the letter h.
#
# PS: The left and right delimiters will be calculated automatically
# based on the final time below and the maximum of the speeds considering
# the LEFTSTATE and RIGHTSTATE above
*USENSMP 1
*NSPCMESHPTS 350
*SPCMESHSTPSIZE 0.001

# Time mesh configuration: the final time is the final time of the
# simulation, and CFLCONDPERCENT is a percentage of the expression
# h * |s|, where |s| is the maximum speed in modulus, considering
# the LEFTSTATE and RIGHTSTATE above
*FINALTIME 500.
*CFLCONDPERCENT 0.005
*ADAPTIVECFL 1
*ADAPTCFLNUMDERIVS 0
*ADAPTCFLFREQUENCY 500

# Solution reporter configuration: SOLOUTPUTFREQUENCY tells the frequency
# which the output must be printed to file: for example, if SOLOUTPUTFREQUENCY
# is set to 100, then in the 0th, 100th, 200th, 300th, ..., time steps
# the solution will be printed to file. PARALLEL, CONVERGENCEPLOT and
# SPEEDPERCENTAGE are configurations available only when the multid
# package is provided.
*SOLOUTPUTFREQUENCY 100
*PARALLEL 1
*CONVERGENCEPLOT 0
*SPEEDPERCENTAGE .6


# Configuration of the finite differences scheme: type 1 to turn on or
# 0 to turn off. The variables FLUXUPWIND and DIFFCONSTANT refer to
# when the flux is approximated via upwind scheme (central otherwise)
# and when the diffusion matrix is constant (variable otherwise),
# respectively
*HASFLUX       1
*FLUXUPWIND    0
*HASDIFFUSION  1
*DIFFCONSTANT  1
*HASREACTION   0

# Configuration of the finite differences scheme: type 0. for fully
# explicit or 1. for fully implicit terms. If 0.5, then the Crank-Nicolson
# scheme is used. Any other value, if it makes sense, will also be accepted
#
# Ps: Notice that when a variable is set to 0, the corresponding term
# does not need to have derivatives implemented inside function jet.
# However, for every other value, the derivatives must be provided.
*FLUXTIMEAVG   .5
*DIFFTIMEAVG   .5
*REACTIMEAVG   .5

# Constraint configuration: when algebraic constraints are provided, the
# user may need to use a relaxation technique in the constraint term. Lets
# say the constraint term has the expression
#
# H(V) = 0.
#
# Depending on the non-linearity of the vector-valued function H, the
# linear solver used in the Newton method may be unstable. In that case,
# one use the relaxed term
#
# eps d/dt H(V) = - H(V),
#
# since d/dt H(V) = d/dt 0 = 0. The eps value may be small and still
# effective into stabilizing the linear system.
#
# In the configuration settings below, CSTISRELAXED refers to whether
# or not the constraint term is relaxed, CSTRELAXATION refers to the
# eps constant above, and CSTRTIMEAVG has the same meaning of FLUXTIMEAVG,
# explained above
*CSTISRELAXED  1
*CSTRELAXATION 1.0e-2
*CSTRTIMEAVG   1.0
# CSTRELAXATION is the same as RELAXEPS

