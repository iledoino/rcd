#include "RCDUtil.h"
#include "RiemannProblem.h"
#include "bdry.h"
#include "dct.h"
#include "keymatch.h"
#include "newton.h"
#include <cmath>
#include <fstream>
#include <iomanip>
#include <limits>

#ifdef ENABLE_GNUPLOT
#include "GnuplotReporter.h"
#endif
#ifdef ENABLE_MATPLOTLIB
#include "MatplotlibReporter.h"
#endif
#ifdef ENABLE_MULTID
#include "MultidReporter.h"
#endif
#include "FileReporter.h"

class stiff : public PhysicsRCD
{
public:
    void setConstantValues(PhysicsDataHolder& phyDH) override;
    void
    jet(PhysicsDataHolder& phyDH, double* Vj, double* Un, double& p, double& u,
            const double& x, const double& t, const std::size_t& idx) override;
    void setPDEAndCstFunctions(PhysicsDataHolder& phyDH, double* Un, double& p,
            double& u, const double& x, const double& t,
            const std::size_t& idx) override;

    const double& a;
    const double& gama;
    const double& e;
    const double& eps;

    stiff(const double& a_, const double& gama_, const double& e_,
            const double& eps_)
        : PhysicsRCD(), a(a_), gama(gama_), e(e_), eps(eps_)
    {
    }
};

void
stiff::setConstantValues(PhysicsDataHolder& phyDH)
{
    /*
     * Derivadas da Acumulacao
     */
    phyDH.DG[0] = phyDH.DG[4] = phyDH.DG[8] = 1.0;

    /*
     * Derivadas do Fluxo
     */
    phyDH.DF[1] = 1.0;

    /*
     * Derivada da Fonte
     */
    phyDH.DR[8] = -(1.0 / e);

    /*
     * Funcao de Difusao, as vezes necessaria para controlar as oscilacoes
     * do metodo de crank nicolson
     */
    phyDH.B[0] = phyDH.B[4] = phyDH.B[8] = eps;
}

void
stiff::setPDEAndCstFunctions(PhysicsDataHolder& phyDH, double* Un,
        double& /* p */, double& /* u */, const double& /* x */,
        const double& /* t */, const std::size_t& /* idx */)
{
    double rho = Un[0];
    double rho_u = Un[1];
    double E = Un[2];

    double E_barra = a * a * rho / (gama - 1.0) + rho_u * rho_u * 0.5 / rho;

    phyDH.G[0] = rho;
    phyDH.G[1] = rho_u;
    phyDH.G[2] = E;

    phyDH.F[0] = rho_u;
    phyDH.F[1] = rho_u * rho_u / rho + rho;
    phyDH.F[2] = (E / rho + a * a) * rho_u;

    phyDH.R[2] = (1.0 / e) * (E_barra - E);
}

void
stiff::jet(PhysicsDataHolder& phyDH, double* Vj, double* /* Un */, double& p,
        double& u, const double& x, const double& t, const std::size_t& idx)
{
    setPDEAndCstFunctions(phyDH, Vj, p, u, x, t, idx);

    double rho = Vj[0];
    double rho_u = Vj[1];
    double E = Vj[2];

    phyDH.DF[3] = 1.0 - rho_u * rho_u / (rho * rho);
    phyDH.DF[4] = 2.0 * rho_u / rho;

    phyDH.DF[6] = -E * rho_u / (rho * rho);
    phyDH.DF[7] = E / rho + a * a;
    phyDH.DF[8] = rho_u / rho;

    phyDH.DR[6] = a * a / (gama - 1.0) - rho_u * rho_u * 0.5 / (rho * rho);
    phyDH.DR[7] = rho_u / rho;
}

int
main(int argc, char** argv)
{
    const std::size_t PDEDim = 3, CSTDim = 0;
    bool hasPressure = false;
    bool hasVelocity = false;

    /*
     * parameters of the physics
     */
    std::vector<double> parameters(20);

    /*
     * Configuration of the discretization scheme
     */
    int hasFlux, fluxUpwind, hasDiffusion, diffConstant, hasReaction,
            cstIsRelaxed;
    double fluxTimeAvg, diffTimeAvg, reacTimeAvg, cstrTimeAvg, cstRelaxation;

    /*
     * left and right states
     */
    std::vector<double> leftState(PDEDim + CSTDim, 0.),
            rightState(PDEDim + CSTDim, 0.);

    /*
     * Configuration of the Riemann Problem simulation
     */
    int useNSMP, parallel, convergencePlot, adaptiveCFL, adapCFLNumDerivs;
    std::size_t nSpcMeshPts, solOutputFrequency, adapCFLFrequency;
    double spcMeshStpSize, finalTime, cflCondPercent;
    double speedPercentage;

    /*
     * Selects whether to test derivatives or not
     */
    int TESTDERIVS;

    /*
     * percentage of newton time steps
     */
    std::vector<double> STEPPERCENT(PDEDim + CSTDim, 1.);

    /*
     * Class to read parameters from file
     */
    KeyMatch keyMatch;

    /*
     * From arguments provided from command line, decides which file use
     * as input file for the parameters below
     */
    keyMatch.define_in_out(argc, argv);

    /*
     * Registration of entries to be read from file and their key names
     */
    keyMatch.register_entry(parameters[0], "*A");
    keyMatch.register_entry(parameters[1], "*GAMMA");
    keyMatch.register_entry(parameters[2], "*E");
    keyMatch.register_entry(parameters[3], "*STABEPSOVERDX");

    keyMatch.register_entry(hasFlux, "*HASFLUX");
    keyMatch.register_entry(fluxUpwind, "*FLUXUPWIND");
    keyMatch.register_entry(hasDiffusion, "*HASDIFFUSION");
    keyMatch.register_entry(diffConstant, "*DIFFCONSTANT");
    keyMatch.register_entry(hasReaction, "*HASREACTION");
    keyMatch.register_entry(cstIsRelaxed, "*CSTISRELAXED");
    keyMatch.register_entry(fluxTimeAvg, "*FLUXTIMEAVG");
    keyMatch.register_entry(diffTimeAvg, "*DIFFTIMEAVG");
    keyMatch.register_entry(reacTimeAvg, "*REACTIMEAVG");
    keyMatch.register_entry(cstrTimeAvg, "*CSTRTIMEAVG");
    keyMatch.register_entry(cstRelaxation, "*CSTRELAXATION");

    keyMatch.register_entry(leftState, "*LEFTSTATE");
    keyMatch.register_entry(rightState, "*RIGHTSTATE");
    keyMatch.register_entry(useNSMP, "*USENSMP");
    keyMatch.register_entry(parallel, "*PARALLEL");
    keyMatch.register_entry(convergencePlot, "*CONVERGENCEPLOT");
    keyMatch.register_entry(nSpcMeshPts, "*NSPCMESHPTS");
    keyMatch.register_entry(solOutputFrequency, "*SOLOUTPUTFREQUENCY");
    keyMatch.register_entry(spcMeshStpSize, "*SPCMESHSTPSIZE");
    keyMatch.register_entry(finalTime, "*FINALTIME");
    keyMatch.register_entry(cflCondPercent, "*CFLCONDPERCENT");
    keyMatch.register_entry(adaptiveCFL, "*ADAPTIVECFL");
    keyMatch.register_entry(adapCFLNumDerivs, "*ADAPTCFLNUMDERIVS");
    keyMatch.register_entry(adapCFLFrequency, "*ADAPTCFLFREQUENCY");
    keyMatch.register_entry(speedPercentage, "*SPEEDPERCENTAGE");

    keyMatch.register_entry(TESTDERIVS, "*TESTDERIVS");
    keyMatch.register_entry(STEPPERCENT, "*STEPPERCENT");

    /*
     * Read parameters from file
     */
    keyMatch.make();

    /*
     * Class used to set the physics of the problem
     */
    stiff phys(parameters[0], parameters[1], parameters[2], parameters[3]);

    if (TESTDERIVS) {
        derivtest(phys, leftState, rightState, PDEDim, CSTDim, TESTDERIVS == 2);
        return 0;
    }

    std::size_t nOfVariables = PDEDim + CSTDim;

    if (hasPressure)
        ++nOfVariables;
    if (hasVelocity)
        ++nOfVariables;

    Reporters reporters;

#if defined(ENABLE_GNUPLOT) || defined(ENABLE_MATPLOTLIB)                      \
        || defined(ENABLE_MULTID)
    std::vector<std::vector<std::size_t>> reportGroups({ { 0, 1 }, { 2 } });
    std::vector<std::string> yAxesNames({ "rho,rho_u", "E" });
    std::string xAxisName("x");
#endif
#ifdef ENABLE_GNUPLOT
    GnuplotReporter gnurptr(reportGroups, xAxisName, yAxesNames);
    reporters.addReporter(gnurptr);
#endif
#ifdef ENABLE_MATPLOTLIB
    MatplotlibReporter matplotlibrptr(reportGroups, xAxisName, yAxesNames);
    matplotlibrptr.frameSecUpdate() = 0.5;
    reporters.addReporter(matplotlibrptr);
#endif
#ifdef ENABLE_MULTID
    MultidReporter multidrptr(&argc, argv, "Stiff Space", nOfVariables,
            reportGroups, xAxisName, yAxesNames);
    reporters.addReporter(multidrptr);
#endif

    FileReporter frptr(keyMatch.output(), solOutputFrequency);
    reporters.addReporter(frptr);

    std::vector<Bdry*> myBCs(2);
    RiemannProblem rp(
            myBCs, phys, reporters, PDEDim, CSTDim, hasPressure, hasVelocity);

    RiemannProblemConfig rpc;
    rpc.leftState() = leftState;
    rpc.rightState() = rightState;
    rpc.flowDirection() = fluxUpwind ? FlowDirection::POSITIVE :
                                       FlowDirection::BIDIRECTIONAL;
    rpc.nSpcMeshPts() = nSpcMeshPts;
    rpc.useNSMP() = useNSMP;
    rpc.infTimeDelim() = finalTime;
    rpc.cflCondPercent() = cflCondPercent;
    rpc.speedPercentage() = speedPercentage;
    rpc.parallel() = parallel;
    rpc.convergencePlot() = convergencePlot;
    rpc.adaptiveCFL() = adaptiveCFL;
    rpc.adapCFLNumDerivs() = adapCFLNumDerivs;
    rpc.adapCFLFrequency() = adapCFLFrequency;

    PDEMemberConfig pdemc;
    pdemc.hasFlux() = hasFlux;
    pdemc.fluxIsUpwind() = fluxUpwind;
    pdemc.impParamFlux() = fluxTimeAvg;
    pdemc.hasDiffusion() = hasDiffusion;
    pdemc.diffIsConstant() = diffConstant;
    pdemc.impParamDiffusion() = diffTimeAvg;
    pdemc.hasReaction() = hasReaction;
    pdemc.impParamReaction() = reacTimeAvg;
    pdemc.impParamConstraint() = cstrTimeAvg;
    pdemc.constraintIsRelaxed() = cstIsRelaxed;
    pdemc.constraintRelaxation() = cstRelaxation;

    rp.setRPC(rpc);
    rp.setPDEMC(pdemc);

    // IMPORTANT: fixing stabilization
    parameters[3] *= rp.getRPC().spcMeshStpSize();
    myBCs[0] = new PDE_RobinBC_FO_L(PDEDim, CSTDim,
            rp.getRPC().spcMeshStpSize(), std::vector<double>(3, 0.),
            std::vector<double>(3, 1.), std::vector<double>(3, 0.));
    myBCs[1] = new PDE_RobinBC_FO_R(PDEDim, CSTDim,
            rp.getRPC().spcMeshStpSize(), std::vector<double>(3, 0.),
            std::vector<double>(3, 1.), std::vector<double>(3, 0.));
    rp.checkForConvergence() = false;
    rp.checkForChangesAtBoundary() = false;

    rp.startCalc("Stiff Physics");

    delete myBCs[0];
    delete myBCs[1];

    return 0;
}
