#include "RCDUtil.h"
#include "RiemannProblem.h"
#include "bdry.h"
#include "dct.h"
#include "keymatch.h"
#include "newton.h"
#include <cassert>
#include <cmath>
#include <fstream>
#include <iomanip>
#include <limits>

/*
 * PS: This class has been created so that you have a startpoint for
 * your own physics.
 */

#ifdef ENABLE_GNUPLOT
#include "GnuplotReporter.h"
#endif
#ifdef ENABLE_MATPLOTLIB
#include "MatplotlibReporter.h"
#endif
#ifdef ENABLE_MULTID
#include "MultidReporter.h"
#endif
#include "FileReporter.h"

/*
 * This physics has been implemented by modifying the original
 * Burgers-Fischer equation, whose solution is well known in the
 * literature
 */
class Fill_in : public PhysicsRCD
{
public:
    void setConstantValues(PhysicsDataHolder& phyDH) override;
    void
    jet(PhysicsDataHolder& phyDH, double* Vj, double* Un, double& p, double& u,
            const double& x, const double& t, const std::size_t& idx) override;
    void setPDEAndCstFunctions(PhysicsDataHolder& phyDH, double* Un, double& p,
            double& u, const double& x, const double& t,
            const std::size_t& idx) override;

    Fill_in(const std::vector<double>& parameters_p)
        : PhysicsRCD(), parameters_(parameters_p)
    {
        // PS: security check: remember to replace 4 by the number of
        // parameters you need
        assert(parameters_p.size() >= 4);
    }

private:
    const std::vector<double>& parameters_;

    double alpha() const { return parameters_[0]; }
    double beta() const { return parameters_[1]; }
    double delta() const { return parameters_[2]; }
    double diffeps() const { return parameters_[3]; }
};

void
Fill_in::setConstantValues(PhysicsDataHolder& /* phyDH */)
{
}

void
Fill_in::setPDEAndCstFunctions(PhysicsDataHolder& phyDH, double* Un,
        double& /* p */, double& /* uvel */, const double& /* x */,
        const double& /* t */, const std::size_t& /* idx */)
{
    double u1 = Un[0];
    double u2 = Un[1];

    phyDH.G[0] = alpha() * u1;

    phyDH.F[0] = beta() * u1 * u1 * 0.5;

    phyDH.B[0] = diffeps();

    phyDH.R[0] = 0.0;

    phyDH.H[0] = u1 - delta() * u2;
}

void
Fill_in::jet(PhysicsDataHolder& phyDH, double* Vj, double* /* Un */, double& p,
        double& uvel, const double& x, const double& t, const std::size_t& idx)
{
    setPDEAndCstFunctions(phyDH, Vj, p, uvel, x, t, idx);

    double u1 = Vj[0];

    phyDH.DG[0] = alpha();

    phyDH.DF[0] = beta() * u1;

    phyDH.DH[0] = 1.0;
    phyDH.DH[1] = -delta();
}

int
main(int argc, char** argv)
{
    // PS: Alter these next lines according to your own physics
    const std::size_t PDEDim = 1, CSTDim = 1;
    bool hasPressure = false;
    bool hasVelocity = false;

    /*
     * parameters of the physics
     * PS: Alter 20 to the number of parameters you need
     */
    std::vector<double> parameters(20);

    /*
     * Configuration of the discretization scheme
     */
    int hasFlux, fluxUpwind, hasDiffusion, diffConstant, hasReaction,
            cstIsRelaxed;
    double fluxTimeAvg, diffTimeAvg, reacTimeAvg, cstrTimeAvg, cstRelaxation;

    /*
     * left and right states
     */
    std::vector<double> leftState(PDEDim + CSTDim, 0.),
            rightState(PDEDim + CSTDim, 0.);

    /*
     * Robin boundary conditions
     */
    std::vector<double> leftA(PDEDim, 0.), rightA(PDEDim, 0.);
    std::vector<double> leftB(PDEDim, 0.), rightB(PDEDim, 0.);
    std::vector<double> leftC(PDEDim, 0.), rightC(PDEDim, 0.);

    /*
     * Configuration of the Riemann Problem simulation
     */
    int convergencePlot, adaptiveCFL, adapCFLNumDerivs, automaticSetUp;
    int useNSMP, useNTMP, parallel;
    std::size_t nSpcMeshPts, nTimeMeshPts, solOutputFrequency, adapCFLFrequency;
    double spcMeshStpSize, timeMeshStpSize, leftSpcDelim, rightSpcDelim,
            finalTime, cflCondPercent, speedPercentage;
    double ZEROTOL, DIFFTOL;

    /*
     * Selects whether to test derivatives or not
     */
    int TESTDERIVS;

    /*
     * percentage of newton time steps
     */
    std::vector<double> STEPPERCENT(PDEDim + CSTDim, 1.);

    /*
     * Class to read parameters from file
     */
    KeyMatch keyMatch;

    /*
     * From arguments provided from command line, decides which file use
     * as input file for the parameters below
     */
    keyMatch.define_in_out(argc, argv);

    /*
     * Registration of entries to be read from file and their key names
     * PS: Register your parameters down below, so that they can be read
     * from files
     */
    keyMatch.register_entry(parameters[0], "*ALPHA");
    keyMatch.register_entry(parameters[1], "*BETA");
    keyMatch.register_entry(parameters[2], "*DELTA");
    keyMatch.register_entry(parameters[3], "*DIFFEPSOVERDX");

    keyMatch.register_entry(hasFlux, "*HASFLUX");
    keyMatch.register_entry(fluxUpwind, "*FLUXUPWIND");
    keyMatch.register_entry(hasDiffusion, "*HASDIFFUSION");
    keyMatch.register_entry(diffConstant, "*DIFFCONSTANT");
    keyMatch.register_entry(hasReaction, "*HASREACTION");
    keyMatch.register_entry(cstIsRelaxed, "*CSTISRELAXED");
    keyMatch.register_entry(fluxTimeAvg, "*FLUXTIMEAVG");
    keyMatch.register_entry(diffTimeAvg, "*DIFFTIMEAVG");
    keyMatch.register_entry(reacTimeAvg, "*REACTIMEAVG");
    keyMatch.register_entry(cstrTimeAvg, "*CSTRTIMEAVG");
    keyMatch.register_entry(cstRelaxation, "*CSTRELAXATION");

    keyMatch.register_entry(automaticSetUp, "*AUTOMATICSETUP");

    keyMatch.register_entry(leftState, "*LEFTSTATE");
    keyMatch.register_entry(rightState, "*RIGHTSTATE");

    keyMatch.register_entry(leftA, "*LEFTA");
    keyMatch.register_entry(leftB, "*LEFTB");
    keyMatch.register_entry(leftC, "*LEFTC");
    keyMatch.register_entry(rightA, "*RIGHTA");
    keyMatch.register_entry(rightB, "*RIGHTB");
    keyMatch.register_entry(rightC, "*RIGHTC");

    keyMatch.register_entry(useNSMP, "*USENSMP");
    keyMatch.register_entry(nSpcMeshPts, "*NSPCMESHPTS");
    keyMatch.register_entry(spcMeshStpSize, "*SPCMESHSTPSIZE");
    keyMatch.register_entry(leftSpcDelim, "*LEFTSPCDELIM");
    keyMatch.register_entry(rightSpcDelim, "*RIGHTSPCDELIM");

    keyMatch.register_entry(useNTMP, "*USENTMP");
    keyMatch.register_entry(nTimeMeshPts, "*NTIMEMESHPTS");
    keyMatch.register_entry(timeMeshStpSize, "*TIMEMESHSTPSIZE");
    keyMatch.register_entry(finalTime, "*FINALTIME");
    keyMatch.register_entry(cflCondPercent, "*CFLCONDPERCENT");
    keyMatch.register_entry(adaptiveCFL, "*ADAPTIVECFL");
    keyMatch.register_entry(adapCFLNumDerivs, "*ADAPTCFLNUMDERIVS");
    keyMatch.register_entry(adapCFLFrequency, "*ADAPTCFLFREQUENCY");
    keyMatch.register_entry(speedPercentage, "*SPEEDPERCENTAGE");

    keyMatch.register_entry(ZEROTOL, "*ZEROTOL");
    keyMatch.register_entry(DIFFTOL, "*DIFFTOL");

    keyMatch.register_entry(parallel, "*PARALLEL");
    keyMatch.register_entry(convergencePlot, "*CONVERGENCEPLOT");
    keyMatch.register_entry(solOutputFrequency, "*SOLOUTPUTFREQUENCY");

    keyMatch.register_entry(TESTDERIVS, "*TESTDERIVS");
    keyMatch.register_entry(STEPPERCENT, "*STEPPERCENT");

    /*
     * Read parameters from file
     */
    keyMatch.make();

    /*
     * Class used to set the physics of the problem
     */
    Fill_in phys(parameters);

    /*
     * Tests if derivatives have been implemented correctly by comparing
     * them with finite differences
     */
    if (TESTDERIVS) {
        derivtest(phys, leftState, rightState, PDEDim, CSTDim, TESTDERIVS == 2);
        return 0;
    }

    std::size_t nOfVariables = PDEDim + CSTDim;

    if (hasPressure)
        ++nOfVariables;
    if (hasVelocity)
        ++nOfVariables;

    Reporters reporters;

#if defined(ENABLE_GNUPLOT) || defined(ENABLE_MATPLOTLIB)                      \
        || defined(ENABLE_MULTID)
    std::vector<std::vector<std::size_t>> reportGroups(nOfVariables);
    std::vector<std::string> yAxesNames(nOfVariables);
    for (std::size_t i = 0; i < nOfVariables; ++i) {
        reportGroups[i].push_back({ i });
        yAxesNames[i] = "var" + std::to_string(i + 1);
    }
    std::string xAxisName("x");
#endif
#ifdef ENABLE_GNUPLOT
    GnuplotReporter gnurptr(reportGroups, xAxisName, yAxesNames);
    reporters.addReporter(gnurptr);
#endif
#ifdef ENABLE_MATPLOTLIB
    MatplotlibReporter matplotlibrptr(reportGroups, xAxisName, yAxesNames);
    matplotlibrptr.frameSecUpdate() = 0.5;
    reporters.addReporter(matplotlibrptr);
#endif
#ifdef ENABLE_MULTID
    MultidReporter multidrptr(&argc, argv, "Fill_in Space", nOfVariables,
            reportGroups, xAxisName, yAxesNames);
    reporters.addReporter(multidrptr);
#endif

    FileReporter frptr(keyMatch.output(), solOutputFrequency);
    reporters.addReporter(frptr);

    std::vector<Bdry*> myBCs(2);
    RiemannProblem rp(
            myBCs, phys, reporters, PDEDim, CSTDim, hasPressure, hasVelocity);

    RiemannProblemConfig rpc;
    rpc.automaticSetUp() = automaticSetUp;
    rpc.leftState() = leftState;
    rpc.rightState() = rightState;
    rpc.flowDirection() = fluxUpwind ? FlowDirection::POSITIVE :
                                       FlowDirection::BIDIRECTIONAL;
    rpc.useNSMP() = useNSMP;
    rpc.nSpcMeshPts() = nSpcMeshPts;
    rpc.spcMeshStpSize() = spcMeshStpSize;
    rpc.leftSpcDelim() = leftSpcDelim;
    rpc.rightSpcDelim() = rightSpcDelim;
    rpc.useNTMP() = useNTMP;
    rpc.nTimeMeshPts() = nTimeMeshPts;
    rpc.timeMeshStpSize() = timeMeshStpSize;
    rpc.infTimeDelim() = finalTime;
    rpc.cflCondPercent() = cflCondPercent;
    rpc.speedPercentage() = speedPercentage;
    rpc.convergencePlot() = convergencePlot;
    rpc.adaptiveCFL() = adaptiveCFL;
    rpc.adapCFLNumDerivs() = adapCFLNumDerivs;
    rpc.adapCFLFrequency() = adapCFLFrequency;
    rpc.parallel() = parallel;

    PDEMemberConfig pdemc;
    pdemc.hasFlux() = hasFlux;
    pdemc.fluxIsUpwind() = fluxUpwind;
    pdemc.impParamFlux() = fluxTimeAvg;
    pdemc.hasDiffusion() = hasDiffusion;
    pdemc.diffIsConstant() = diffConstant;
    pdemc.impParamDiffusion() = diffTimeAvg;
    pdemc.hasReaction() = hasReaction;
    pdemc.impParamReaction() = reacTimeAvg;
    pdemc.impParamConstraint() = cstrTimeAvg;
    pdemc.constraintIsRelaxed() = cstIsRelaxed;
    pdemc.constraintRelaxation() = cstRelaxation;

    rp.setRPC(rpc);
    rp.setPDEMC(pdemc);

    // IMPORTANT: fixing stabilization
    parameters[3] *= rp.getRPC().spcMeshStpSize();
    myBCs[0] = new PDE_RobinBC_FO_L(
            PDEDim, CSTDim, rp.getRPC().spcMeshStpSize(), leftA, leftB, leftC);
    myBCs[1] = new PDE_RobinBC_FO_R(PDEDim, CSTDim,
            rp.getRPC().spcMeshStpSize(), rightA, rightB, rightC);
    rp.checkForConvergence() = false;
    rp.checkForChangesAtBoundary() = false;

    rp.startCalc("Fill_in Physics", 0., ZEROTOL, DIFFTOL);

    delete myBCs[0];
    delete myBCs[1];

    return 0;
}
