#ifndef SRC_RIEMANNPROBLEM_RIEMANNPROBLEM_H
#define SRC_RIEMANNPROBLEM_RIEMANNPROBLEM_H


#include "Eigenproblem.h"
#include "Reporter.h"
#include "dct.h"
#include "newton.h"
#include "physicsRCD.h"
#include "RPEnum.h"
#include <array>
#include <ctime>
#include <memory>
#include <vector>
//#define DEBUG_RIEMANN_PROBLEM

class RiemannProblem;

class RiemannProblemConfig
{
public:
    RiemannProblemConfig();
    ~RiemannProblemConfig() noexcept = default;

    bool automaticSetUp() const { return automaticSetUp_; }
    bool& automaticSetUp() { return automaticSetUp_; }

    /* conf. available for automatic and manual set up */
    std::vector<double> leftState() const { return leftState_; }
    std::vector<double>& leftState() { return leftState_; }
    std::vector<double> rightState() const { return rightState_; }
    std::vector<double>& rightState() { return rightState_; }

    double spcMeshStpSize() const { return spcMeshStpSize_; }
    double& spcMeshStpSize() { return spcMeshStpSize_; }
    std::size_t nSpcMeshPts() const { return nSpcMeshPts_; }
    std::size_t& nSpcMeshPts() { return nSpcMeshPts_; }
    bool useNSMP() const { return useNSMP_; }
    bool& useNSMP() { return useNSMP_; }

    double infTimeDelim() const { return infTimeDelim_; }
    double& infTimeDelim() { return infTimeDelim_; }

    double timeStepConvCheck() const { return timeStepConvCheck_; }
    double& timeStepConvCheck() { return timeStepConvCheck_; }

    bool parallel() const { return parallel_; }
    bool& parallel() { return parallel_; }

    /* conf. available only for automatic set up */
    FlowDirection flowDirection() const { return flowDirection_; }
    FlowDirection& flowDirection() { return flowDirection_; }

    double speedPercentage() const { return speedPercentage_; }
    double& speedPercentage() { return speedPercentage_; }
    double cflCondPercent() const { return cflCondPercent_; }
    double& cflCondPercent() { return cflCondPercent_; }
    bool adaptiveCFL() const { return adaptiveCFL_; }
    bool& adaptiveCFL() { return adaptiveCFL_; }
    bool adapCFLNumDerivs() const { return adapCFLNumDerivs_; }
    bool& adapCFLNumDerivs() { return adapCFLNumDerivs_; }
    std::size_t adapCFLFrequency() const { return adapCFLFrequency_; }
    std::size_t& adapCFLFrequency() { return adapCFLFrequency_; }

    bool convergencePlot() const { return convergencePlot_; }
    bool& convergencePlot() { return convergencePlot_; }

    /* conf. available only for manual set up */
    double leftSpcDelim() const { return leftSpcDelim_; }
    double& leftSpcDelim() { return leftSpcDelim_; }
    double rightSpcDelim() const { return rightSpcDelim_; }
    double& rightSpcDelim() { return rightSpcDelim_; }
    double jumSpcPt() const { return jumSpcPt_; }
    double& jumSpcPt() { return jumSpcPt_; }

    double timeMeshStpSize() const { return timeMeshStpSize_; }
    double& timeMeshStpSize() { return timeMeshStpSize_; }
    std::size_t nTimeMeshPts() const { return nTimeMeshPts_; }
    std::size_t& nTimeMeshPts() { return nTimeMeshPts_; }
    bool useNTMP() const { return useNTMP_; }
    bool& useNTMP() { return useNTMP_; }

private:
    bool automaticSetUp_;

    std::vector<double> leftState_, rightState_;

    FlowDirection flowDirection_;

    double leftSpcDelim_, rightSpcDelim_, jumSpcPt_, spcMeshStpSize_;
    std::size_t nSpcMeshPts_;
    bool useNSMP_;

    double infTimeDelim_, timeMeshStpSize_, cflCondPercent_;
    std::size_t nTimeMeshPts_;
    bool useNTMP_;

    double speedPercentage_ = 0.2, timeStepConvCheck_ = 5.0;

    bool parallel_, convergencePlot_;

    bool adaptiveCFL_{ false }, adapCFLNumDerivs_{ false };
    std::size_t adapCFLFrequency_{ 100 };

    friend class RiemannProblem;
};

class RiemannProblem
{
public:
    RiemannProblem(PhysicsRCD& phys, Reporter& rptr, std::size_t PDEDim,
            std::size_t CSTDim = 0, bool hasPress = false, bool hasVel = false);
    RiemannProblem(const std::vector<Bdry*>& bdry, PhysicsRCD& phys,
            Reporter& rptr, std::size_t PDEDim, std::size_t CSTDim = 0,
            bool hasPress = false, bool hasVel = false);
    RiemannProblem(const RiemannProblem&) = delete;
    RiemannProblem& operator=(const RiemannProblem&) = delete;
    virtual ~RiemannProblem() noexcept;

    RiemannProblemConfig getRPC() const;
    void setRPC(RiemannProblemConfig rpc);

    PDEMemberConfig getPDEMC() const;
    void setPDEMC(PDEMemberConfig pdemc);

    void startCalc(std::string physicsName, double time_ = 0.,
            double ZEROTOL = 1.0e-10, double DIFFTOL = 1.0e-8,
            bool boundaryCheck = true);
    void continueCalc(std::string physicsName, double ZEROTOL = 1.0e-10,
            double DIFFTOL = 1.0e-8);
    void solve(std::string physicsName, double ZEROTOL = 1.0e-10,
            double DIFFTOL = 1.0e-8);

    std::size_t PDEDim() const { return PDEDim_; }
    std::size_t CSTDim() const { return CSTDim_; }
    std::size_t SYSDim() const { return SYSDim_; }
    std::size_t Dim() const { return Dim_; }
    bool hasPress() const { return hasPress_; }
    bool hasVel() const { return hasVel_; }

    void getSolution(std::vector<std::vector<double>>& solution, double& time);
    void getSpcMesh(std::vector<double>& spcMesh);

    bool checkForConvergence() const { return checkForConvergence_; }
    bool checkForChangesAtBoundary() const
    {
        return checkForChangesAtBoundary_;
    }

    bool& checkForConvergence() { return checkForConvergence_; }
    bool& checkForChangesAtBoundary() { return checkForChangesAtBoundary_; }

    /*
     * Functions that may be overriden by users
     */
    virtual void setUpBdryConditions();
    virtual void setInitialSolution();
    virtual void adaptTimeMeshStpSize(
            std::size_t timeCounter, double currentTime, double finalTime);

protected:
    PhysicsRCD& phys_;
    Reporter& rptr_;

    const std::size_t PDEDim_, CSTDim_;
    const std::size_t SYSDim_, Dim_;
    const bool hasPress_, hasVel_;

    const bool bdryCondsProvided_;
    std::vector<Bdry*> bdry_internal_;
    const std::vector<Bdry*>& bdry_;
    std::unique_ptr<Discretization> dct_;
    std::unique_ptr<Newton> nwt_;

    RiemannProblemConfig rpc_;
    PDEMemberConfig pdemc_;

    double currentTime = 0.;
    double xi_mod_max = 0, xi_min = 0., xi_max = 1.;
    std::size_t convCheckTimes = 1;
    std::array<std::vector<std::vector<double>>, 2> prevCheckedSolutions_;
    double refNorm_ = 1.;
    bool hasConverged = false;

    bool checkForConvergence_, checkForChangesAtBoundary_;

    double
    shockSpeed(const std::vector<double>& gL, const std::vector<double>& gR,
            const std::vector<double>& fL, const std::vector<double>& fR);
    void getEigenvalues(PhysicsDataHolder& pdh_, Eigenproblem& ep,
            std::vector<double>& state_, double p_p, double u_p,
            std::vector<double>& vl, std::vector<double>& vr,
            std::vector<double>& ev_r, std::vector<double>& ev_i,
            std::vector<bool>& isReal);
    void setUpSpeedLimits(const RiemannProblemConfig& rpc);
    void updateCFLCondition(double currentTime, double finalTime);
    bool checkConvergence();
    void load_dct();
    double sqr_(const double& a) const { return a * a; }
    double norm(const std::vector<double>& sol);
    double
    norm(const std::vector<double>& sol1, const std::vector<double>& sol2);
    double norm(const std::vector<std::vector<double>>& sol);
    double norm(const std::vector<std::vector<double>>& sol1,
            const std::vector<std::vector<double>>& sol2);
    void shrinkSolution();
    void makeReport(bool makeanyway = false);
    bool reachedBoundary(double tol);
};

#endif /* SRC_RIEMANNPROBLEM_RIEMANNPROBLEM_H */
