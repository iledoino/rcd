#ifndef SRC_INTERFACE_MATPLOTLIBREPORTER_H
#define SRC_INTERFACE_MATPLOTLIBREPORTER_H

#ifdef ENABLE_MATPLOTLIB

#include "PlotReporter.h"
#include "matplotlibcpp.h"

namespace plt = matplotlibcpp;

class MatplotlibReporter : public PlotReporter
{
public:
    MatplotlibReporter(std::vector<std::vector<std::size_t>> reportGroups_p,
            std::string xAxisName_p, std::vector<std::string> yAxesNames_p)
        : PlotReporter(reportGroups_p, xAxisName_p, yAxesNames_p)
    {
    }
    virtual ~MatplotlibReporter() noexcept = default;

    void make(const std::vector<std::vector<double>>& solution,
            const std::vector<double>& spcMesh, double time,
            bool makeanyway = false) override;
    void finish() override;
};

#endif /* ENABLE_MATPLOTLIB */

#endif /* SRC_INTERFACE_MATPLOTLIBREPORTER_H */
