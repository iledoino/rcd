#ifndef PHYSICS_PHYSICSRCD_H
#define PHYSICS_PHYSICSRCD_H


#include <cmath>
#include <iostream>

/*
 * This class holds data used to fill up the matrix from newton method
 */
class PhysicsDataHolder
{
private:
    PhysicsDataHolder(const PhysicsDataHolder&) = delete;
    PhysicsDataHolder& operator=(const PhysicsDataHolder&) = delete;

    /*
     * this variable specifies wether an instance was already
     * allocated or not (because the constructor does not allocate)
     */
    bool allocated;

public:
    /*
     * arrays used to hold the several PDE terms, which are
     *
     * G --> Accumulation, DG is its jacobian
     * B --> Diffusion, DB is its tensor
     * F --> Flux, DF is its jacobian
     * R --> Reaction, DR is its jacobian
     *
     * H --> Constraint, DH is its jacobian
     *
     * these variables will be used to fill up whats necessary to
     * solve numerically via finite differences the following equations
     *
     * firstly solve:
     * ∂/∂t G(V) + ∂/∂x F(V) = ∂/∂x( B(V) ∂V/∂x ) + R(V)
     *                  H(V) = 0
     *
     * where V is the solution we are seeking
     *
     */
    double *G, *B, *F, *R, *H;
    double *DG, *DB, *DF, *DR, *DH;
    /*
     * u is the velocity
     * p is the pressure
     * k is the ratio between the permeability and the viscosity
     * q is the flow rate
     *
     * these variables will be used to fill up whats necessary to
     * solve numerically via splitting the following equations
     *
     *
     *
     *
     * if there is pressure
     *
     * secondly solve:
     * ∇ . ( -k ∇p ) = q
     *
     * thirdly solve:
     * u = -k ∇p
     *
     *
     *
     *
     *
     *
     * if there is no pressure
     *
     * use two equations in the form
     *
     * (g1)_t+(uf1)=0
     * (g2)_t+(uf2)=0
     *
     * g1 and f1 can be already part of the PDE, but g2 and f2 have
     * to be new expressions in order to solve for u. In this case,
     * the user has to set the variables as follows
     *
     * G1n1 = g1(V^{n+1})
     * G2n1 = g2(V^{n+1})
     *
     * G1n  = g1(V^{n})
     * G2n  = g2(V^{n})
     *
     * F1n1 = f1(V^{n+1});
     * F2n1 = f2(V^{n+1});
     *
     * At this point, both V^{n+1} and V^{n} have been found, and the
     * value of u^{n+1} will be calculated explicitly
     */
    double k, q, G1n1, G2n1, G1n, G2n, F1n1, F2n1;
    /*
     * Auxiliar Arrays
     */
    double *BtdV, *BtdVNext, *DBtdV, *DBtdVNext;
    /*
     * Variables that define dimensions and sizes
     */
    std::size_t sqrBcksDim, rectBcksDim, zeroBcksDim;
    std::size_t sqrBcksSize, rectBcksSize, zeroBcksSize;
    /*
     * Auxiliar Functions
     */
    void MatrixProduct(double* dV, double* dVNext, const double& ct);
    void TensorProduct(double* dV, double* dVNext);
    void allocate(const std::size_t& PDEDimension,
            const std::size_t& ConstraintDimension);

    /*
     * Constructor
     */
    PhysicsDataHolder()
        : allocated(false)
        , G()
        , B()
        , F()
        , R()
        , H()
        , DG()
        , DB()
        , DF()
        , DR()
        , DH()
        , k()
        , q()
        , G1n1()
        , G2n1()
        , G1n()
        , G2n()
        , F1n1()
        , F2n1()
        , BtdV()
        , BtdVNext()
        , DBtdV()
        , DBtdVNext()
        , sqrBcksDim(1)
        , rectBcksDim()
        , zeroBcksDim()
        , sqrBcksSize(1)
        , rectBcksSize()
        , zeroBcksSize()
    {
    }
    /*
     * Destructor
     */
    ~PhysicsDataHolder();
};

/*
 * This class is supposed to be inherited from by a user's class. It
 * provides a way to set up the particular physics of the problem
 */
class PhysicsRCD
{
    /*
     * OBSERVATION: WHEN INHERITING FROM THIS CLASS, REMIND YOURSELF THAT
     * THERE IS NO NEED TO SET UP ZERO VALUES ON PhysicsDataHolder INSTANCES,
     * UNLESS IT IS PRODUCT OF A NONCONSTANT EXPRESSION.
     */

public:
    /*
     * Set only the constant values for the physics. For example, if
     * the diffusion is constant in a single heat equation
     *
     * ∂/∂t G = ∂/∂x( B ∂V/∂x ),
     *
     *
     * then
     *
     * G[0] = V[0];
     * B[0] = D;
     *
     * DG[0] = 1.0;
     *
     * In this case, the values for B[0] and DG[0] are set inside this
     * function, because they are constant. The value for G[0] is not
     * constant (depends on V[0]), therefore it may not be defined here
     */
    virtual void setConstantValues(PhysicsDataHolder& /* phyDH */) {}
    /*
     * Set only the terms, not its derivatives. For example, in the
     * heat equation above, the value for G[0] should be defined here
     */
    virtual void
    setPDEAndCstFunctions(PhysicsDataHolder& phyDH, double* Un, double& p,
            double& u, const double& x, const double& t, const std::size_t& idx)
            = 0;

    /*
     * Set up terms as well as its derivatives. If the function above
     * is called here, then this function may only define the derivatives
     */
    virtual void
    jet(PhysicsDataHolder& phyDH, double* Vj, double* Un, double& p, double& u,
            const double& x, const double& t, const std::size_t& idx)
            = 0;

    /*
     * Set up the splitting functions, k and q
     */
    virtual void SetSplittingFunctions(PhysicsDataHolder& /* phyDH */,
            double* /* Un1 */, double* /* Un */, double& /* p */,
            double& /* u */, const double& /* x */, const double& /* t */,
            const std::size_t& /* idx */)
    {
    }

    PhysicsRCD() = default;
    virtual ~PhysicsRCD() = default;
};

#endif /* PHYSICS_PHYSICSRCD_H */
