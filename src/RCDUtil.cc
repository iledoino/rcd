#include "RCDUtil.h"

void
derivtest(PhysicsRCD& phys, const std::vector<double>& leftState_,
        const std::vector<double>& rightState_, const std::size_t& PDEDim,
        const std::size_t& CSTDim, bool convTestRatio)
{
    std::vector<double> leftState(leftState_), rightState(rightState_);
    /*
     * Code for testing the derivatives
     */
    std::size_t nofTestPoints = 2;
    std::size_t noDerivCheckings = 6;
    double tol_zero = 1.0e-12;

    std::vector<double> states_diff(leftState.size(), 0);
    for (std::size_t i = 0; i < leftState.size(); ++i) {
        states_diff[i] = rightState[i] - leftState[i];
        if (std::abs(states_diff[i]) < tol_zero) {
            states_diff[i] = leftState[i] * 0.01;
            leftState[i] -= states_diff[i] * 0.5;
            rightState[i] += states_diff[i] * 0.5;
        }
    }

    std::vector<PhysicsDataHolder> phyDHVector(noDerivCheckings);
    for (auto& phyDHAverage : phyDHVector)
        phyDHAverage.allocate(PDEDim, CSTDim);

    for (std::size_t i = 1; i < nofTestPoints; ++i) {
        for (std::size_t l = 0; l < noDerivCheckings; ++l) {
            double p = 0., u = 0., x = 0., t = 0.;
            if (leftState.size() == PDEDim + CSTDim + 1)
                u = p = leftState[PDEDim + CSTDim];
            else if (leftState.size() == PDEDim + CSTDim + 2) {
                p = leftState[PDEDim + CSTDim];
                u = leftState[PDEDim + CSTDim + 1];
            }
            std::size_t idx = 0;

            std::vector<double> currState(leftState.size(), 0);
            for (std::size_t k = 0; k < leftState.size(); ++k)
                currState[k] = leftState[k]
                        + (states_diff[k] / static_cast<double>(nofTestPoints))
                                * static_cast<double>(k);

            PhysicsDataHolder phyDHPrev;
            phyDHPrev.allocate(PDEDim, CSTDim);
            phys.setConstantValues(phyDHPrev);

            PhysicsDataHolder phyDHCurr;
            phyDHCurr.allocate(PDEDim, CSTDim);
            phys.setConstantValues(phyDHCurr);
            phys.jet(phyDHCurr, currState.data(), currState.data(), p, u, x, t,
                    idx);

            PhysicsDataHolder phyDHCurr2;
            phyDHCurr2.allocate(PDEDim, CSTDim);
            phys.setConstantValues(phyDHCurr2);
            phys.jet(phyDHCurr2, currState.data(), currState.data(), p, u, x, t,
                    idx);

            PhysicsDataHolder phyDHNext;
            phyDHNext.allocate(PDEDim, CSTDim);
            phys.setConstantValues(phyDHNext);

            for (std::size_t j = 0; j < PDEDim + CSTDim; ++j) {
                std::vector<double> prevState(leftState.size(), 0);
                std::vector<double> nextState(leftState.size(), 0);

                for (std::size_t k = 0; k < leftState.size(); ++k)
                    prevState[k] = currState[k];
                prevState[j] -= (states_diff[j]
                        / (pow(2., static_cast<double>(l))
                                * static_cast<double>(nofTestPoints)));
                phys.setConstantValues(phyDHPrev);
                phys.setPDEAndCstFunctions(
                        phyDHPrev, prevState.data(), p, u, x, t, idx);

                for (std::size_t k = 0; k < leftState.size(); ++k)
                    nextState[k] = currState[k];
                nextState[j] += (states_diff[j]
                        / (pow(2., static_cast<double>(l))
                                * static_cast<double>(nofTestPoints)));
                phys.setConstantValues(phyDHNext);
                phys.setPDEAndCstFunctions(
                        phyDHNext, nextState.data(), p, u, x, t, idx);

                for (std::size_t k = 0; k < PDEDim; ++k) {
                    phyDHCurr.DG[k * (PDEDim + CSTDim) + j]
                            -= (phyDHNext.G[k] - phyDHPrev.G[k])
                            / (2. * states_diff[j]
                                    / (pow(2., static_cast<double>(l))
                                            * static_cast<double>(
                                                    nofTestPoints)));
                    phyDHCurr.DF[k * (PDEDim + CSTDim) + j]
                            -= (phyDHNext.F[k] - phyDHPrev.F[k])
                            / (2. * states_diff[j]
                                    / (pow(2., static_cast<double>(l))
                                            * static_cast<double>(
                                                    nofTestPoints)));
                    phyDHCurr.DR[k * (PDEDim + CSTDim) + j]
                            -= (phyDHNext.R[k] - phyDHPrev.R[k])
                            / (2. * states_diff[j]
                                    / (pow(2., static_cast<double>(l))
                                            * static_cast<double>(
                                                    nofTestPoints)));
                }

                for (std::size_t k = 0; k < CSTDim; ++k) {
                    phyDHCurr.DH[k * (PDEDim + CSTDim) + j]
                            -= (phyDHNext.H[k] - phyDHPrev.H[k])
                            / (2. * states_diff[j]
                                    / (pow(2., static_cast<double>(l))
                                            * static_cast<double>(
                                                    nofTestPoints)));
                }

                if (convTestRatio) {
                    for (std::size_t k = 0; k < leftState.size(); ++k)
                        prevState[k] = currState[k];
                    prevState[j] -= (states_diff[j]
                            / (pow(2., static_cast<double>(l) + 1.)
                                    * static_cast<double>(nofTestPoints)));
                    phys.setConstantValues(phyDHPrev);
                    phys.setPDEAndCstFunctions(
                            phyDHPrev, prevState.data(), p, u, x, t, idx);

                    for (std::size_t k = 0; k < leftState.size(); ++k)
                        nextState[k] = currState[k];
                    nextState[j] += (states_diff[j]
                            / (pow(2., static_cast<double>(l) + 1.)
                                    * static_cast<double>(nofTestPoints)));
                    phys.setConstantValues(phyDHNext);
                    phys.setPDEAndCstFunctions(
                            phyDHNext, nextState.data(), p, u, x, t, idx);

                    for (std::size_t k = 0; k < PDEDim; ++k) {
                        phyDHCurr2.DG[k * (PDEDim + CSTDim) + j]
                                -= (phyDHNext.G[k] - phyDHPrev.G[k])
                                / (2. * states_diff[j]
                                        / (pow(2., static_cast<double>(l) + 1.)
                                                * static_cast<double>(
                                                        nofTestPoints)));
                        phyDHCurr2.DF[k * (PDEDim + CSTDim) + j]
                                -= (phyDHNext.F[k] - phyDHPrev.F[k])
                                / (2. * states_diff[j]
                                        / (pow(2., static_cast<double>(l) + 1.)
                                                * static_cast<double>(
                                                        nofTestPoints)));
                        phyDHCurr2.DR[k * (PDEDim + CSTDim) + j]
                                -= (phyDHNext.R[k] - phyDHPrev.R[k])
                                / (2. * states_diff[j]
                                        / (pow(2., static_cast<double>(l) + 1.)
                                                * static_cast<double>(
                                                        nofTestPoints)));
                    }

                    for (std::size_t k = 0; k < CSTDim; ++k) {
                        phyDHCurr2.DH[k * (PDEDim + CSTDim) + j]
                                -= (phyDHNext.H[k] - phyDHPrev.H[k])
                                / (2. * states_diff[j]
                                        / (pow(2., static_cast<double>(l) + 1.)
                                                * static_cast<double>(
                                                        nofTestPoints)));
                    }
                }
            }

            auto& phyDHAverage = phyDHVector[l];
            if (convTestRatio) {
                for (std::size_t k = 0; k < PDEDim * (PDEDim + CSTDim); ++k) {
                    phyDHAverage.DG[k]
                            += std::abs(phyDHCurr2.DG[k]) > tol_zero ?
                            phyDHCurr.DG[k] / phyDHCurr2.DG[k] :
                            4.;
                    phyDHAverage.DF[k]
                            += std::abs(phyDHCurr2.DF[k]) > tol_zero ?
                            phyDHCurr.DF[k] / phyDHCurr2.DF[k] :
                            4.;
                    phyDHAverage.DR[k]
                            += std::abs(phyDHCurr2.DR[k]) > tol_zero ?
                            phyDHCurr.DR[k] / phyDHCurr2.DR[k] :
                            4.;
                }
                for (std::size_t k = 0; k < CSTDim * (PDEDim + CSTDim); ++k)
                    phyDHAverage.DH[k]
                            += std::abs(phyDHCurr2.DH[k]) > tol_zero ?
                            phyDHCurr.DH[k] / phyDHCurr2.DH[k] :
                            4.;
            }
            else {
                for (std::size_t k = 0; k < PDEDim * (PDEDim + CSTDim); ++k) {
                    phyDHAverage.DG[k] += phyDHCurr.DG[k];
                    phyDHAverage.DF[k] += phyDHCurr.DF[k];
                    phyDHAverage.DR[k] += phyDHCurr.DR[k];
                }
                for (std::size_t k = 0; k < CSTDim * (PDEDim + CSTDim); ++k)
                    phyDHAverage.DH[k] += phyDHCurr.DH[k];
            }
        }
    }
    std::cerr << std::setprecision(std::numeric_limits<double>::digits10 + 1);
    for (auto& phyDHAverage : phyDHVector) {
        for (std::size_t k = 0; k < PDEDim * (PDEDim + CSTDim); ++k) {
            phyDHAverage.DG[k] /= static_cast<double>(nofTestPoints) - 1.;
            phyDHAverage.DF[k] /= static_cast<double>(nofTestPoints) - 1.;
            phyDHAverage.DR[k] /= static_cast<double>(nofTestPoints) - 1.;
        }
        for (std::size_t k = 0; k < CSTDim * (PDEDim + CSTDim); ++k)
            phyDHAverage.DH[k] /= static_cast<double>(nofTestPoints) - 1.;
    }
    std::cerr << "%Printing the errors of the numerical derivatives "
                 "evaluations to matlab\n";
    std::cerr << "DG_err=[";
    for (auto& phyDHAverage : phyDHVector) {
        for (std::size_t i = 0; i < PDEDim * (PDEDim + CSTDim); ++i)
            std::cerr << phyDHAverage.DG[i] << " ";
        std::cerr << std::endl;
    }
    std::cerr << "];\nDF_err=[";
    for (auto& phyDHAverage : phyDHVector) {
        for (std::size_t i = 0; i < PDEDim * (PDEDim + CSTDim); ++i)
            std::cerr << phyDHAverage.DF[i] << " ";
        std::cerr << std::endl;
    }
    std::cerr << "];\nDR_err=[";
    for (auto& phyDHAverage : phyDHVector) {
        for (std::size_t i = 0; i < PDEDim * (PDEDim + CSTDim); ++i)
            std::cerr << phyDHAverage.DR[i] << " ";
        std::cerr << std::endl;
    }
    std::cerr << "];\nDH_err=[";
    for (auto& phyDHAverage : phyDHVector) {
        for (std::size_t i = 0; i < CSTDim * (PDEDim + CSTDim); ++i)
            std::cerr << phyDHAverage.DH[i] << " ";
        std::cerr << std::endl;
    }
    std::cerr << "];\n";
    std::cerr << "for i=1:size(DG_err, 2)\n";
    std::cerr << "\tfigure(i);\n";
    std::cerr << "\tplot((1:size(DG_err, 1))',DG_err(:,i),'bo-');\n";
    std::cerr << "end\n";
    std::cerr << "pause;\n";
    std::cerr << "for i=1:size(DF_err, 2)\n";
    std::cerr << "\tfigure(i);\n";
    std::cerr << "\tplot((1:size(DF_err, 1))',DF_err(:,i),'bo-');\n";
    std::cerr << "end\n";
    std::cerr << "pause;\n";
    std::cerr << "for i=1:size(DR_err, 2)\n";
    std::cerr << "\tfigure(i);\n";
    std::cerr << "\tplot((1:size(DR_err, 1))',DR_err(:,i),'bo-');\n";
    std::cerr << "end\n";
    std::cerr << "pause;\n";
    std::cerr << "for i=1:size(DH_err, 2)\n";
    std::cerr << "\tfigure(i);\n";
    std::cerr << "\tplot((1:size(DH_err, 1))',DH_err(:,i),'bo-');\n";
    std::cerr << "end\n";
    std::cerr << "pause;\n";
}

void
smoothJumpSolution(double* V, double* x, std::size_t XSIZE, double XJUMP,
        double XJUMPSIZE, const std::size_t& PDEDim_,
        const std::size_t& CstDim_, const std::vector<double>& leftState_,
        const std::vector<double>& rightState_, std::string filesolutionname)
{
    for (std::size_t k = 0; k < PDEDim_ + CstDim_; ++k) {
        if (rightState_[k] != leftState_[k])
            for (std::size_t i = 0, j = 0; i != XSIZE;
                    i++, j += (PDEDim_ + CstDim_)) {
                V[j + k] = leftState_[k]
                        + (rightState_[k] - leftState_[k])
                                * (1.0 + std::tanh((x[i] - XJUMP) / XJUMPSIZE))
                                * 0.5;
            }
        else
            for (std::size_t i = 0, j = 0; i != XSIZE;
                    i++, j += (PDEDim_ + CstDim_)) {
                V[j + k] = leftState_[k];
            }
    }

    std::ifstream myfile(filesolutionname);
    if (myfile.is_open()) {
        for (std::size_t i = 0, j = 0; i != XSIZE;
                i++, j += (PDEDim_ + CstDim_)) {
            double xx;
            myfile >> xx;
            for (std::size_t k = 0; k < PDEDim_ + CstDim_; ++k) {
                myfile >> V[j + k];
            }
        }
        myfile.close();
        std::cout << "Read file successfully\n";
    }
}

void
convertDctSolution(Discretization& dct_, std::vector<std::vector<double>>& sol_,
        std::vector<double>& x_)
{
    std::size_t SYSDim_
            = static_cast<std::size_t>(dct_.PDEDim() + dct_.cstDim());
    std::size_t Dim_
            = SYSDim_ + (dct_.hasPress() ? 1 : 0) + (dct_.hasVel() ? 1 : 0);
    sol_.resize(Dim_);
    for (std::size_t i = 0; i < Dim_; ++i)
        sol_[i].resize(dct_.nSpcMeshPts());
    x_.resize(dct_.nSpcMeshPts());
    std::size_t jj = SYSDim_;
    std::size_t jjj = dct_.hasPress() ? SYSDim_ + 1 : SYSDim_;
    for (std::size_t i = 0, ii = 0;
            i < static_cast<std::size_t>(dct_.nSpcMeshPts());
            ++i, ii += SYSDim_) {
        for (std::size_t j = 0; j < SYSDim_; ++j)
            sol_[j][i] = dct_.V[j + ii];
        if (dct_.hasPress())
            sol_[jj][i] = dct_.V[jj + ii];
        if (dct_.hasVel())
            sol_[jjj][i] = dct_.V[jjj + ii];
        x_[i] = dct_.x[i];
    }
}
