#ifndef SRC_INTERFACE_FILEREPORTER_H
#define SRC_INTERFACE_FILEREPORTER_H


#include "Reporter.h"
#include <string>

class FileReporter : public Reporter
{
public:
    FileReporter(std::string solution_file_name = "solution.out",
            std::size_t solution_frequency = 100);
    virtual ~FileReporter() noexcept = default;

    void start() override;
    bool timetomake() override;
    void make(const std::vector<std::vector<double>>& solution,
            const std::vector<double>& spcMesh, double time,
            bool makeanyway = false) override;
    void finish() override;

private:
    std::string solution_file_name_;
    std::size_t solution_frequency_;
    std::size_t nvar, nprint, nsize, solcounter;
};

#endif /* SRC_INTERFACE_FILEREPORTER_H */
