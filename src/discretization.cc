#include "dct.h"
#include <fstream>
#include <iostream>

/*
 * Function that sets variables that depend on dt
 */
void
Discretization::DT(const double& dt)
{
    for (std::size_t i = 0; i != pdeMembers.size(); ++i)
        pdeMembers[i]->SetLambda(phyDH[0], dt);
}

/*
 * This function implements allocation that is commum to all constructors
 */
void
Discretization::AllocateVirtualData()
{
    /*
     * Data for physics, which includes PDE, Constraint, Pressure and
     * Velocity data holders
     */
    phyDH = new PhysicsDataHolder[1];
    phyDH[0].allocate(PDEDimension, ConstraintDimension);
    phy.setConstantValues(phyDH[0]);

    /*
     * BCs to the PDE
     *
     * Dependind on what kind of BC we are dealing with, different linear
     * systems will be solved, and therefore, a different structure for
     * the matrix used
     */
    switch (bdry[0]->BCtype) {
    case 0: {
        M = new NUBCS(PDEDimension + ConstraintDimension, PDEDimension,
                nOfSpaceMeshPoints);
    } break;

    case 1: {
        M = new NUBTS(PDEDimension + ConstraintDimension, PDEDimension,
                nOfSpaceMeshPoints);
    } break;

    case 2: {
        M = new NUSBTS(PDEDimension + ConstraintDimension, PDEDimension,
                nOfSpaceMeshPoints);
    } break;

    default: {
        std::cerr << "Please make sure to use the correct BCtype for your "
                     "discretized PDE. Please have a look to the class Bdry"
                  << std::endl;
        exit(1);
    }
    }

    /*
     * BCs to the pressure equation
     */
    if (hasPressure) {
        switch (bdry[3]->BCtype) {
        case 0: {
            A = new NUBCS(1, 1, nOfSpaceMeshPoints);
        } break;

        case 1: {
            A = new NUBTS(1, 1, nOfSpaceMeshPoints);
        } break;

        case 2: {
            A = new NUSBTS(1, 1, nOfSpaceMeshPoints);
        } break;

        default: {
            std::cerr << "Please make sure to use the correct BCtype for your "
                         "pressure equation. Please have a look to the class "
                         "Bdry"
                      << std::endl;
            exit(1);
        }
        }
        /*
         * Once the matrix and RHS have been defined for the pressure
         * equation, then the pressure array is defined
         */
        p = A->RHS;

        if (hasVelocity)
            Calculate_Velocity
                    = &Discretization::Calculate_Velocity_withPressure;
    }
    else if (hasVelocity)
        Calculate_Velocity
                = &Discretization::Calculate_Velocity_withoutPressure;

    /*
     * Auxiliary data for asembling
     */
    LHS = M->RHS;
    RHS = new double[M->RHSArraySize];
    V = new double[M->RHSArraySize];
    Vn = new double[M->RHSArraySize];
    k = new double[nOfSpaceMeshPoints];
    u = new double[nOfSpaceMeshPoints];
    dV = new double[M->RHSArraySize + M->sqrBcksDim];
    x = new double[nOfSpaceMeshPoints];
    DummyMemory = new double[M->sqrBcksSize];

    /*
     * Setting up pointers to the matrix, in order to set BCs
     */
    bdry[0]->setPointers(V, V + M->sqrBcksDim, V + 2 * M->sqrBcksDim, Vn,
            Vn + M->sqrBcksDim, Vn + 2 * M->sqrBcksDim, LHS + M->zeroBcksDim,
            M->mDiag + M->zeroBcksSize, M->mUpper, M->leftBdryExtraBck);
    bdry[1]->setPointers(V + M->RHSArraySize - M->sqrBcksDim,
            V + M->RHSArraySize - 2 * M->sqrBcksDim,
            V + M->RHSArraySize - 3 * M->sqrBcksDim,
            Vn + M->RHSArraySize - M->sqrBcksDim,
            Vn + M->RHSArraySize - 2 * M->sqrBcksDim,
            Vn + M->RHSArraySize - 3 * M->sqrBcksDim,
            LHS + M->RHSArraySize - M->rectBcksDim,
            M->mDiag + M->sqrArraySize - M->rectBcksSize,
            M->mLower + M->rectArraySize - M->rectBcksSize,
            M->rightBdryExtraBck);
    /*
     * pointers to pressure BC values
     */
    if (hasPressure) {
        bdry[2]->setPointers(V, V + M->sqrBcksDim, V + 2 * M->sqrBcksDim, Vn,
                Vn + M->sqrBcksDim, Vn + 2 * M->sqrBcksDim, A->RHS, A->mDiag,
                A->mUpper, A->leftBdryExtraBck);
        bdry[3]->setPointers(V + M->RHSArraySize - M->sqrBcksDim,
                V + M->RHSArraySize - 2 * M->sqrBcksDim,
                V + M->RHSArraySize - 3 * M->sqrBcksDim,
                Vn + M->RHSArraySize - M->sqrBcksDim,
                Vn + M->RHSArraySize - 2 * M->sqrBcksDim,
                Vn + M->RHSArraySize - 3 * M->sqrBcksDim,
                A->RHS + A->RHSArraySize - 1, A->mDiag + A->sqrArraySize - 1,
                A->mLower + A->rectArraySize - 1, A->rightBdryExtraBck);
    }
    else if (hasVelocity)
        bdry[2]->setPointers(V, V + M->sqrBcksDim, DummyMemory, Vn,
                Vn + M->sqrBcksDim, DummyMemory, u, DummyMemory, DummyMemory,
                DummyMemory);
}

void
Discretization::setUpPDEMembers(std::vector<PDEMember*> pdeMembers_)
{
    if (PDEMembersAllocatedInside)
        for (std::size_t i = 0; i != pdeMembers.size(); ++i)
            delete pdeMembers[i];
    pdeMembers = pdeMembers_;
    PDEMembersAllocatedInside = false;

    /*
     * Sets constants for each member of the PDE
     */
    for (std::size_t i = 0; i != pdeMembers.size(); ++i)
        pdeMembers[i]->SetAlphaAndBeta(phyDH[0], dx);
}

void
Discretization::setUpPDEMembers(const PDEMemberConfig& pdemc)
{
    if (PDEMembersAllocatedInside)
        for (std::size_t i = 0; i != pdeMembers.size(); ++i)
            delete pdeMembers[i];

    /*
     * PDE Members have their spot allocated
     */
    pdeMembers.clear();
    pdeMembers.resize(5);
    PDEMembersAllocatedInside = true;
    std::size_t pdeCounter = 0;

    /*
     * Decides whether there is constraint equations or not
     */
    if (ConstraintDimension > 0) {
        if (pdemc.constraintIsRelaxed())
            pdeMembers[pdeCounter++] = new RelaxedConstraint(
                    pdemc.impParamConstraint(), pdemc.constraintRelaxation());
        else
            pdeMembers[pdeCounter++]
                    = new Constraint(pdemc.impParamConstraint());
    }

    /*
     * There will always be a accumulation member, once that newton
     * method is assured to be used
     */
    pdeMembers[pdeCounter++] = new Accumulation();

    /*
     * Decides whether there is a flux member, and if its discretization
     * is upwind or central space
     */
    if (pdemc.hasFlux()) {
        if (pdemc.fluxIsUpwind())
            pdeMembers[pdeCounter++] = new Flux_UpWind(pdemc.impParamFlux());
        else
            pdeMembers[pdeCounter++]
                    = new Flux_CentralDifference(pdemc.impParamFlux());
    }

    /*
     * Decides whether there is a diffusion matrix and if it is constant
     * or not
     */
    if (pdemc.hasDiffusion()) {
        if (pdemc.diffIsConstant())
            pdeMembers[pdeCounter++]
                    = new Diffusion_Constant(pdemc.impParamDiffusion());
        else
            pdeMembers[pdeCounter++] = new Diffusion(pdemc.impParamDiffusion());
    }

    /*
     * Decides whether there is a reation/font term or not
     */
    if (pdemc.hasReaction())
        pdeMembers[pdeCounter++] = new Reaction(pdemc.impParamReaction());

    pdeMembers.resize(pdeCounter);
    /*
     * Sets constants for each member of the PDE
     */
    for (std::size_t i = 0; i != pdeMembers.size(); ++i)
        pdeMembers[i]->SetAlphaAndBeta(phyDH[0], dx);
}

/*
 * Constructor which allocates when the user sets its pdeMembers
 */
Discretization::Discretization(PhysicsRCD& phy_, std::vector<Bdry*> bdry_,
        const std::size_t& PDEDimension_,
        const std::size_t& ConstraintDimension_,
        const std::size_t& nOfSpaceMeshPoints_, const double& dx_,
        bool hasPressure_, bool hasVelocity_)
    : PDEMembersAllocatedInside(false)
    , PDEDimension(PDEDimension_)
    , ConstraintDimension(ConstraintDimension_)
    , nOfSpaceMeshPoints(nOfSpaceMeshPoints_)
    , hasPressure(hasPressure_)
    , hasVelocity(hasVelocity_)
    , x()
    , V()
    , p()
    , u()
    , RHS()
    , dV()
    , Vn()
    , k()
    , DummyMemory()
    , LHS()
    , dx(dx_)
    , pdeMembers()
    , phy(phy_)
    , phyDH()
    , bdry(bdry_)
    , M()
    , A()
    , Calculate_Velocity()
{
    AllocateVirtualData();
}

/*
 * Destructor
 */
Discretization::~Discretization()
{
    if (PDEMembersAllocatedInside)
        for (std::size_t i = 0; i != pdeMembers.size(); ++i)
            delete pdeMembers[i];

    delete[] phyDH;
    delete M;
    if (hasPressure)
        delete A;
    delete[] RHS;
    delete[] V;
    delete[] Vn;
    delete[] k;
    delete[] u;
    delete[] dV;
    delete[] x;
    delete[] DummyMemory;
}

/*
 * This function calculates an array that saves computations when
 * executing the products B_i * (V_i-V_{i-1}) and B_i * (V_{i+1}-V_i)
 */
void
Discretization::CalculatedV()
{
    /*
     * Left Boundary, here it is calculated V_0-V_N, in order to set
     * periodic boundary conditions, if this is the case
     */
    for (std::size_t i = 0, iP = (M->RHSArraySize - M->sqrBcksDim);
            i < M->sqrBcksDim; ++i, ++iP)
        dV[i] = V[i] - V[iP];

    /*
     * Inside Domain, here it is calculated V_i-V_{i-1}
     */
    for (std::size_t i = M->sqrBcksDim, iP = 0; i != M->RHSArraySize; ++iP, ++i)
        dV[i] = V[i] - V[iP];

    /*
     * Right Boundary, here it is calculated V_0-V_N, in order to set
     * periodic boundary conditions, if this is the case
     */
    for (std::size_t i = M->RHSArraySize,
                     iP = (M->RHSArraySize - M->sqrBcksDim), iC = 0;
            i != (M->RHSArraySize + M->sqrBcksDim); ++iP, ++i, ++iC)
        dV[i] = V[iC] - V[iP];
}

/*
 * Calculates the right hand side of the equation F(V^{j+1})=Y(V^{j})
 */
void
Discretization::SetRHS(const double& t)
{
    CalculatedV();
    for (std::size_t i = 0; i != M->RHSArraySize; ++i)
        RHS[i] = 0;

    std::size_t j = 0, iP = 0, iC = 0, iN = M->sqrBcksDim;
    {
        /*
         * The function jet calculates necessary terms to set up the physics
         * desired by the user
         */
        phy.setPDEAndCstFunctions(phyDH[0], Vn + iC, p[j], u[j], x[j], t, j);

        for (std::size_t i = 0; i != pdeMembers.size(); ++i)
            pdeMembers[i]->AddMemberToRHS(phyDH[0], DummyMemory, RHS + iC,
                    RHS + iN, dV + iC, dV + iN);

        /*
         * The following lines of code are necessary only for periodic
         * boundary conditions. If the boundary conditions are not periodic,
         * then they do not influence the results.
         */
        j = M->nRectBcks;
#if 0
        // NOTE: this variable is never used
        iP = (j - 1) * M->sqrBcksDim;
#endif
        iC = j * M->sqrBcksDim;
        iN = (j + 1) * M->sqrBcksDim;

        for (std::size_t i = 0; i != pdeMembers.size(); ++i)
            pdeMembers[i]->AddMemberToRHS(phyDH[0], RHS + iC, DummyMemory,
                    DummyMemory, dV + iN, DummyMemory);
    }
    for (j = 1, iP = 0, iC = M->sqrBcksDim, iN = 2 * M->sqrBcksDim;
            j != M->nRectBcks; iP = iC, iC = iN, iN += M->sqrBcksDim, ++j) {
        /*
         * The function jet calculates necessary terms to set up the physics
         * desired by the user
         */
        phy.setPDEAndCstFunctions(phyDH[0], Vn + iC, p[j], u[j], x[j], t, j);

        for (std::size_t i = 0; i != pdeMembers.size(); ++i)
            pdeMembers[i]->AddMemberToRHS(
                    phyDH[0], RHS + iP, RHS + iC, RHS + iN, dV + iC, dV + iN);
    }
    j = M->nRectBcks;
    iP = (j - 1) * M->sqrBcksDim;
    iC = j * M->sqrBcksDim;
    iN = (j + 1) * M->sqrBcksDim;
    {
        /*
         * The function jet calculates necessary terms to set up the physics
         * desired by the user
         */
        phy.setPDEAndCstFunctions(phyDH[0], Vn + iC, p[j], u[j], x[j], t, j);

        for (std::size_t i = 0; i != pdeMembers.size(); ++i)
            pdeMembers[i]->AddMemberToRHS(phyDH[0], RHS + iP, RHS + iC,
                    DummyMemory, dV + iC, dV + iN);

            /*
             * The following lines of code are necessary only for periodic
             * boundary conditions. If the boundary conditions are not periodic,
             * then they do not influence the results.
             */
#if 0
        // NOTE: this variable is never used
        j = 0;
        iP = 0;
#endif
        iC = 0;
#if 0
        // NOTE: this variable is never used
        iN = M->sqrBcksDim;
#endif

        for (std::size_t i = 0; i != pdeMembers.size(); ++i)
            pdeMembers[i]->AddMemberToRHS(phyDH[0], DummyMemory, DummyMemory,
                    RHS + iC, DummyMemory, dV + iC);
    }

    SetBC_RHS(t);
}

/*
 * Calculates the left hand side of the equation F(V^{j+1})=Y(V^{j})
 * Calculates the jacobian of G(V^{j+1})=F(V^{j+1})-Y(V^{j})
 */
void
Discretization::SetJacAndLHS(const double& tPrev, const double& tCurr)
{
    CalculatedV();

    for (std::size_t i = 0; i != M->RHSArraySize; i++)
        LHS[i] = 0.0;

    for (std::size_t i = 0; i != M->sqrArraySize; i++)
        M->mDiag[i] = 0.0;

    for (std::size_t i = 0; i != M->rectArraySize; i++)
        M->mUpper[i] = M->mLower[i] = 0.0;

    for (std::size_t i = 0; i != M->rectBcksSize; ++i)
        M->leftBdryExtraBck[i] = M->rightBdryExtraBck[i] = 0.0;

    /*
     * ===================== LEGEND FOR INDEXES ========================
     *
     * iCF  --> index for current  F (F_i    )
     * iPF  --> index for previous F (F_{i-1})
     * iNF  --> index for next     F (F_{i+1})
     *
     * iPJG --> index for previous square block from the jacobian of G (D_{i-1})
     * iCJG --> index for current  square block from the jacobian of G (D_{i  })
     * iNJG --> index for next     square block from the jacobian of G (D_{i+1})
     *
     * iPRJG --> index for previous rectangular block from the jacobian of G
     ***(L_{i  } or U_{i-1})
     * iCRJG --> index for current  rectangular block from the jacobian of G
     ***(L_{i+1} or U_{i}  )
     */
    std::size_t i = 0, iPLHS = 0, iCLHS = 0, iNLHS = M->sqrBcksDim, iPJG = 0,
                iCJG = 0, iNJG = M->sqrBcksSize, iPRJG = 0, iCRJG = 0;
    {
        /*
         * The function jet calculates necessary terms to set up the physics
         * desired by the user
         */
        phy.jet(phyDH[0], V + iCLHS, Vn + iCLHS, p[i], u[i], x[i], tCurr, i);

        for (std::size_t j = 0; j != pdeMembers.size(); ++j)
            pdeMembers[j]->AddMemberToLHS(phyDH[0], DummyMemory, LHS + iCLHS,
                    LHS + iNLHS, dV + iCLHS, dV + iNLHS);

        for (std::size_t j = 0; j != pdeMembers.size(); ++j)
            pdeMembers[j]->AddMemberToJG(phyDH[0], DummyMemory, M->mDiag + iCJG,
                    M->mDiag + iNJG, DummyMemory, M->mUpper + iCRJG,
                    M->leftBdryExtraBck, M->mLower + iCRJG, dV + iCLHS,
                    dV + iNLHS);

        /*
         * The following lines of code are necessary only for periodic
         * boundary conditions. If the boundary conditions are not periodic,
         * then they do not influence the results.
         */
        i = M->nRectBcks;
#if 0
        // NOTE: this variable is never used
        iPLHS = (i - 1) * M->sqrBcksDim;
#endif
        iCLHS = i * M->sqrBcksDim;
        iNLHS = (i + 1) * M->sqrBcksDim;
#if 0
        // NOTE: this variable is never used
        iPJG = (i - 1) * M->sqrBcksSize;
#endif
        iCJG = i * M->sqrBcksSize;
#if 0
        // NOTE: these variables are never used
        iNJG = (i + 1) * M->sqrBcksSize;
        iPRJG = (i - 1) * M->rectBcksSize;
        iCRJG = i * M->rectBcksSize;
#endif

        for (std::size_t j = 0; j != pdeMembers.size(); ++j)
            pdeMembers[j]->AddMemberToLHS(phyDH[0], LHS + iCLHS, DummyMemory,
                    DummyMemory, dV + iNLHS, DummyMemory);

        for (std::size_t j = 0; j != pdeMembers.size(); ++j)
            pdeMembers[j]->AddMemberToJG(phyDH[0], M->mDiag + iCJG, DummyMemory,
                    DummyMemory, M->rightBdryExtraBck, DummyMemory, DummyMemory,
                    DummyMemory, dV + iNLHS, DummyMemory);
    }
    for (i = 1, iPLHS = 0, iCLHS = M->sqrBcksDim, iNLHS = 2 * M->sqrBcksDim,
        iPJG = 0, iCJG = M->sqrBcksSize, iNJG = 2 * M->sqrBcksSize, iPRJG = 0,
        iCRJG = M->rectBcksSize;
            i != M->nRectBcks; iPLHS = iCLHS, iCLHS = iNLHS,
        iNLHS += M->sqrBcksDim, iPJG = iCJG, iCJG = iNJG,
        iNJG += M->sqrBcksSize, iPRJG = iCRJG, iCRJG += M->rectBcksSize, ++i) {
        /*
         * The function jet calculates necessary terms to set up the physics
         * desired by the user
         */
        phy.jet(phyDH[0], V + iCLHS, Vn + iCLHS, p[i], u[i], x[i], tCurr, i);

        for (std::size_t j = 0; j != pdeMembers.size(); ++j)
            pdeMembers[j]->AddMemberToLHS(phyDH[0], LHS + iPLHS, LHS + iCLHS,
                    LHS + iNLHS, dV + iCLHS, dV + iNLHS);

        for (std::size_t j = 0; j != pdeMembers.size(); ++j)
            pdeMembers[j]->AddMemberToJG(phyDH[0], M->mDiag + iPJG,
                    M->mDiag + iCJG, M->mDiag + iNJG, M->mUpper + iPRJG,
                    M->mUpper + iCRJG, M->mLower + iPRJG, M->mLower + iCRJG,
                    dV + iCLHS, dV + iNLHS);
    }
    i = M->nRectBcks;
    iPLHS = (i - 1) * M->sqrBcksDim;
    iCLHS = i * M->sqrBcksDim;
    iNLHS = (i + 1) * M->sqrBcksDim;
    iPJG = (i - 1) * M->sqrBcksSize;
    iCJG = i * M->sqrBcksSize;
#if 0
    // NOTE: this variable is never used
    iNJG = (i + 1) * M->sqrBcksSize;
#endif
    iPRJG = (i - 1) * M->rectBcksSize;
#if 0
    // NOTE: this variable is never used
    iCRJG = i * M->rectBcksSize;
#endif
    {
        /*
         * The function jet calculates necessary terms to set up the physics
         * desired by the user
         */
        phy.jet(phyDH[0], V + iCLHS, Vn + iCLHS, p[i], u[i], x[i], tCurr, i);

        for (std::size_t j = 0; j != pdeMembers.size(); ++j)
            pdeMembers[j]->AddMemberToLHS(phyDH[0], LHS + iPLHS, LHS + iCLHS,
                    DummyMemory, dV + iCLHS, dV + iNLHS);

        for (std::size_t j = 0; j != pdeMembers.size(); ++j)
            pdeMembers[j]->AddMemberToJG(phyDH[0], M->mDiag + iPJG,
                    M->mDiag + iCJG, DummyMemory, M->mUpper + iPRJG,
                    M->rightBdryExtraBck, M->mLower + iPRJG, DummyMemory,
                    dV + iCLHS, dV + iNLHS);

            /*
             * The following lines of code are necessary only for periodic
             * boundary conditions. If the boundary conditions are not periodic,
             * then they do not influence the results.
             */
#if 0
        // NOTE: these variables are never used
        i = 0;
        iPLHS = 0;
#endif
        iCLHS = 0;
#if 0
        // NOTE: these variables are never used
        iNLHS = M->sqrBcksDim;
        iPJG = 0;
#endif
        iCJG = 0;
#if 0
        // NOTE: these variables are never used
        iNJG = M->sqrBcksSize;
        iPRJG = 0;
        iCRJG = 0;
#endif

        for (std::size_t j = 0; j != pdeMembers.size(); ++j)
            pdeMembers[j]->AddMemberToLHS(phyDH[0], DummyMemory, DummyMemory,
                    LHS + iCLHS, DummyMemory, dV + iCLHS);

        for (std::size_t j = 0; j != pdeMembers.size(); ++j)
            pdeMembers[j]->AddMemberToJG(phyDH[0], DummyMemory, DummyMemory,
                    M->mDiag + iCJG, DummyMemory, DummyMemory, DummyMemory,
                    M->leftBdryExtraBck, DummyMemory, dV + iCLHS);
    }

    SetBC_JacAndLHS(tPrev, tCurr);

#ifdef PRINT_LS_MATLAB
    std::cerr << "dtriplets = zeros(" << M->sqrArraySize << ",3);\n";
    std::cerr << "utriplets = zeros(" << M->rectArraySize << ",3);\n";
    std::cerr << "ltriplets = zeros(" << M->rectArraySize << ",3);\n";
    std::cerr << "rhs = zeros(" << M->RHSArraySize << ",1);\n";
    std::cerr << "\n% block diagonal entries\n";
    for (std::size_t i = 0, ii = 1; i != M->nSqrBcks; ++i)
        for (std::size_t j = 0; j != M->sqrBcksDim; ++j)
            for (std::size_t k = 0; k != M->sqrBcksDim; ++k, ++ii)
                std::cerr
                        << "dtriplets(" << ii << ", :) = ["
                        << 1 + i * M->sqrBcksDim + j << ", "
                        << 1 + i * M->sqrBcksDim + k << ", "
                        << M->mDiag[i * M->sqrBcksSize + j * M->sqrBcksDim + k]
                        << "];\n";
    std::cerr << "\n% block lower entries\n";
    for (std::size_t i = 0, ii = 1; i != M->nRectBcks; ++i)
        for (std::size_t j = 0; j != M->rectBcksDim; ++j)
            for (std::size_t k = 0; k != M->sqrBcksDim; ++k, ++ii)
                std::cerr << "ltriplets(" << ii << ", :) = ["
                          << 1 + (i + 1) * M->sqrBcksDim + M->zeroBcksDim + j
                          << ", " << 1 + i * M->sqrBcksDim + k << ", "
                          << M->mLower[i * M->rectBcksSize + j * M->sqrBcksDim
                                     + k]
                          << "];\n";
    std::cerr << "\n% block upper entries\n";
    for (std::size_t i = 0, ii = 1; i != M->nRectBcks; ++i)
        for (std::size_t j = 0; j != M->rectBcksDim; ++j)
            for (std::size_t k = 0; k != M->sqrBcksDim; ++k, ++ii)
                std::cerr << "utriplets(" << ii << ", :) = ["
                          << 1 + i * M->sqrBcksDim + M->zeroBcksDim + j << ", "
                          << 1 + (i + 1) * M->sqrBcksDim + k << ", "
                          << M->mUpper[i * M->rectBcksSize + j * M->sqrBcksDim
                                     + k]
                          << "];\n";
    std::cerr << "\n% rhs entries\n";
    std::cerr << "rhs(:,1) = [";
    for (std::size_t i = 0; i != M->RHSArraySize; i++)
        std::cerr << LHS[i] << std::endl;
    std::cerr << "];\n";
    std::cerr << "\n% bulding the sparse matrix\n";
    std::cerr << "alltriplets = [dtriplets ; utriplets ; ltriplets];\n";
    std::cerr << "S = "
                 "sparse(alltriplets(:,1),alltriplets(:,2),alltriplets(:,3));"
                 "\n\n\n";
#endif
}

/*
 * Function that sets the boundary conditions
 */
void
Discretization::SetBC_JacAndLHS(const double& tPrev, const double& tCurr)
{
    bdry[0]->pdeMembers_ = pdeMembers;
    bdry[0]->SetBoundaryCondition(x[0], tPrev, tCurr, 0);
    bdry[1]->pdeMembers_ = pdeMembers;
    bdry[1]->SetBoundaryCondition(x[M->nRectBcks], tPrev, tCurr, M->nRectBcks);
}

/*
 * Function that sets the boundary conditions
 */
void
Discretization::SetBC_RHS(const double& /* t */)
{
    /*
     * This loop assures that BCs will be used only if they are not periodic
     */
    if (bdry[0]->BCtype != 0) {
        /*
         * If there is no right BC, then there is no need do set RHS[iR]
         * to zero
         */
        if (bdry[1]->BCtype == 3)
            for (std::size_t iL = M->zeroBcksDim; iL != M->sqrBcksDim; ++iL)
                RHS[iL] = 0.0;
        else
            for (std::size_t iL = M->zeroBcksDim,
                             iR = (M->RHSArraySize - M->rectBcksDim);
                    iL != M->sqrBcksDim; ++iL, ++iR)
                RHS[iL] = RHS[iR] = 0.0;
    }
}

/*
 * Function that sets up the matriz used to solve a linear system for
 * pressure
 */
void
Discretization::SetPressureMatrix(const double& tPrev, const double& tCurr)
{
    std::size_t ip, i, in, iv;
    double Q, ct_Q = -2.0 * dx * dx;

    /*
     * Filling up with zeros
     */
    for (i = 0; i != A->nRectBcks; ++i)
        A->mDiag[i] = A->mLower[i] = A->mUpper[i] = 0.0;
    A->mDiag[A->nRectBcks] = 0.0;

    /*
     * Calculation of each k_i and q_i and addition of them to the right
     * spots
     */
    i = 0;
    in = 1;
    iv = 0;
    {
        phy.SetSplittingFunctions(
                phyDH[0], V + iv, Vn + iv, p[i], u[i], x[i], tCurr, i);

        k[i] = phyDH[0].k;
        Q = ct_Q * phyDH[0].q;

        /*
         * Calculation refers to part (k_{i }+k_{i-1})p_{i-1}
         */
        A->mLower[i] += k[i];

        /*
         * Calculation refers to part (k_{i+1}+k_{i  })p_{i+1}
         */
        A->mUpper[i] += k[i];

        /*
         * Calculation refers to part -(k_{i+1}+2k_{i  }+k_{i-1})p_{i  }
         */
        A->mDiag[i] -= 2.0 * k[i];
        A->mDiag[in] -= k[i];

        /*
         * Calculation refers to part - 2 dx^2 q_i
         */
        A->RHS[i] = Q;
    }
    for (ip = 0, i = 1, in = 2, iv = M->sqrBcksDim; i != A->nRectBcks;
            ip = i, i = in, ++in, iv += M->sqrBcksDim) {
        phy.SetSplittingFunctions(
                phyDH[0], V + iv, Vn + iv, p[i], u[i], x[i], tCurr, i);

        k[i] = phyDH[0].k;
        Q = ct_Q * phyDH[0].q;

        /*
         * Calculation refers to part (k_{i }+k_{i-1})p_{i-1}
         */
        A->mLower[ip] += k[i];
        A->mLower[i] += k[i];

        /*
         * Calculation refers to part (k_{i+1}+k_{i  })p_{i+1}
         */
        A->mUpper[ip] += k[i];
        A->mUpper[i] += k[i];

        /*
         * Calculation refers to part -(k_{i+1}+2k_{i  }+k_{i-1})p_{i  }
         */
        A->mDiag[ip] -= k[i];
        A->mDiag[i] -= 2.0 * k[i];
        A->mDiag[in] -= k[i];

        /*
         * Calculation refers to part - 2 dx^2 q_i
         */
        A->RHS[i] = Q;
    }
    i = A->nRectBcks;
    ip = i - 1;
    iv = i * M->sqrBcksDim;
    {
        phy.SetSplittingFunctions(
                phyDH[0], V + iv, Vn + iv, p[i], u[i], x[i], tCurr, i);

        k[i] = phyDH[0].k;
        Q = ct_Q * phyDH[0].q;

        /*
         * Calculation refers to part (k_{i }+k_{i-1})p_{i-1}
         */
        A->mLower[ip] += k[i];

        /*
         * Calculation refers to part (k_{i+1}+k_{i  })p_{i+1}
         */
        A->mUpper[ip] += k[i];

        /*
         * Calculation refers to part -(k_{i+1}+2k_{i  }+k_{i-1})p_{i  }
         */
        A->mDiag[ip] -= k[i];
        A->mDiag[i] -= 2.0 * k[i];

        /*
         * Calculation refers to part - 2 dx^2 q_i
         */
        A->RHS[i] = Q;
    }

    SetBC_Pressure(tPrev, tCurr);
}

/*
 * Function that sets the boundary conditions
 */
void
Discretization::SetBC_Pressure(const double& tPrev, const double& tCurr)
{
    bdry[2]->pdeMembers_ = pdeMembers;
    bdry[2]->SetBoundaryCondition(x[0], tPrev, tCurr, 0);
    bdry[3]->pdeMembers_ = pdeMembers;
    bdry[3]->SetBoundaryCondition(x[A->nRectBcks], tPrev, tCurr, A->nRectBcks);
}

/*
 * Function that calculates the velocity
 */
void
Discretization::Calculate_Velocity_withPressure(
        const double& /* tPrev */, const double& /* tCurr */)
{
    std::size_t ip, i, in;
    double ct_u = -0.25 / dx, ct_ubdry = -0.5 / dx;

    /*
     * boundary conditions are not necessary when u is given from pressure
     * and the other variables
     */

    /*
     * u here is given by the formula
     *
     *   u_0 = -1/(2 dx) * k_0 * ( -3p_0 + 4 p_1 - p_{2} )
     */
    i = 0;
    in = 1;
    u[i] = ct_ubdry * k[i] * (-3.0 * p[i] + 4.0 * p[in] - p[in + 1]);

    /*
     * u here is given by the formula
     *
     *   u_i = -1/(4 dx) *( ( k_{i+1}+k_i )*( p_{i+1}-p_i ) + ( k_i+k_{i-1} )*(
     ***p_i-p_{i-1} ) )
     */
    for (ip = 0, i = 1, in = 2; i != A->nRectBcks; ip = i, i = in, ++in)
        u[i] = ct_u
                * ((k[in] + k[i]) * (p[in] - p[i])
                        + (k[i] + k[ip]) * (p[i] - p[ip]));

    /*
     * u here is given by the formula
     *
     *   u_M = -1/(2 dx) * k_M * ( 3 p_M - 4 p_{M-1} + p_{M-2} )
     */
    i = A->nRectBcks;
    ip = A->nRectBcks - 1;
    u[i] = ct_ubdry * k[i] * (3.0 * p[i] - 4.0 * p[ip] + p[ip - 1]);
}

/*
 * Function that calculates the velocity
 */
void
Discretization::Calculate_Velocity_withoutPressure(
        const double& tPrev, const double& tCurr)
{
    std::size_t ip, i, iv;
    double F1p, F2p, ratio;

    /*
     * boundary conditions for velocity
     */
    bdry[2]->pdeMembers_ = pdeMembers;
    bdry[2]->SetBoundaryCondition(x[0], tPrev, tCurr, 0);

    /*
     * F1 _0 and F2_0 are calculated
     */
    phy.SetSplittingFunctions(
            phyDH[0], V + 0, Vn + 0, p[0], u[0], x[0], tCurr, 0);
    F1p = phyDH[0].F1n1;
    F2p = phyDH[0].F2n1;

    /*
     * aproximates u_i through the formula
     *
     *   u_i = u_{i-1} ( [G2]_i F1_{i-1} - [G1]_i F2_{i-1} ) / ( [G2]_i F1_i -
     ***[G1]_i F2_i ),
     *
     * where time indices are all j+1, except for [Gk], which is given
     * by the formula
     *
     *   [Gk] = Gk^{n+1}-Gk^{n}
     */
    for (ip = 0, i = 1, iv = M->sqrBcksDim; i != M->nSqrBcks;
            ip = i, ++i, iv += M->sqrBcksDim) {
        phy.SetSplittingFunctions(
                phyDH[0], V + iv, Vn + iv, p[i], u[i], x[i], tCurr, i);

        ratio = (phyDH[0].G2n1 - phyDH[0].G2n) * phyDH[0].F1n1
                - (phyDH[0].G1n1 - phyDH[0].G1n) * phyDH[0].F2n1;

        /*
         * if ratio is 0, then u can be proven to be constant at that point
         */
        if (fabs(ratio) > 1.0e-16)
            u[i] = u[ip]
                    * ((phyDH[0].G2n1 - phyDH[0].G2n) * F1p
                            - (phyDH[0].G1n1 - phyDH[0].G1n) * F2p)
                    / ratio;
        else
            u[i] = u[ip];

        F1p = phyDH[0].F1n1;
        F2p = phyDH[0].F2n1;
    }
}

/*
 * Function that calculates the initial pressure and velocity
 */
int
Discretization::SetInitialPressureAndVelocity()
{
    if (hasPressure) {
        int PressureLSResult;

        /*
         * Fills up the matrix and its RHS
         */
        SetPressureMatrix(0.0, 0.0);

        /*
         * Solves the Linear System
         */
        PressureLSResult = Solve_Pressure();

        if (PressureLSResult != 1) {
            std::cerr << "\tPressure System has not been solved\n";
            return 0;
        }
    }

    if (hasVelocity)
        (this->*Calculate_Velocity)(0.0, 0.0);

    return 1;
}

/*
 * Function used on Discretization parallelized
 */
void
AssemblyIndices::setIndices(const std::size_t& iStart_,
        const std::size_t& iEnd_, const std::size_t& sqrBcksDim,
        const std::size_t& rectBcksDim)
{
    iStart = iStart_;
    iEnd = iEnd_;
    iPG = iStart_ * sqrBcksDim;
    iCG = ((iStart_ + 1) * sqrBcksDim);
    iNG = ((iStart_ + 2) * sqrBcksDim);
    iPJG = (iStart_ * sqrBcksDim * sqrBcksDim);
    iCJG = ((iStart_ + 1) * sqrBcksDim * sqrBcksDim);
    iNJG = ((iStart_ + 2) * sqrBcksDim * sqrBcksDim);
    iPRJG = (iStart_ * sqrBcksDim * rectBcksDim);
    iCRJG = ((iStart_ + 1) * sqrBcksDim * rectBcksDim);
}

/*
 * This variable holds the number of threads used to parallelize the code.
 * If the user wants to override this number (which is the maximum number
 * available), then it must override the environment variable OMP_NUM_THREADS
 */
#ifdef ENABLE_OPENMP
std::size_t DiscretizationParallelized::nThreads = omp_get_max_threads();
#else // ifdef ENABLE_OPENMP
std::size_t DiscretizationParallelized::nThreads = 3;
#endif
std::size_t DiscretizationParallelized::nPartitions
        = DiscretizationParallelized::nThreads;

/*
 * Function that sets variables that depend on dt
 */
void
DiscretizationParallelized::DT(const double& dt)
{
    for (std::size_t i = 0; i != pdeMembers.size(); ++i)
        for (std::size_t j = 0; j != nThreads; ++j)
            pdeMembers[i]->SetLambda(phyDH[j], dt);
}

void
DiscretizationParallelized::setUpPDEMembers(std::vector<PDEMember*> pdeMembers_)
{
    Discretization::setUpPDEMembers(pdeMembers_);
    for (std::size_t i = 0; i != pdeMembers.size(); ++i)
        for (std::size_t j = 0; j != nThreads; ++j)
            pdeMembers[i]->SetAlphaAndBeta(phyDH[j], dx);
}

void
DiscretizationParallelized::setUpPDEMembers(const PDEMemberConfig& pdemc)
{
    Discretization::setUpPDEMembers(pdemc);
    for (std::size_t i = 0; i != pdeMembers.size(); ++i)
        for (std::size_t j = 0; j != nThreads; ++j)
            pdeMembers[i]->SetAlphaAndBeta(phyDH[j], dx);
}

/*
 * Allocation commun to all constructors
 */
void
DiscretizationParallelized::AllocateParallelData()
{
    /*
     * The following line os code is necesssary to undo what was done on
     * base class.
     * OBS: I could have used another pointer though.
     */
    delete[] phyDH;

    /*
     * Every thread running in the future will have its own data
     */
    phyDH = new PhysicsDataHolder[nThreads];
    for (std::size_t i = 0; i != nThreads; ++i) {
        phyDH[i].allocate(M->rectBcksDim, M->zeroBcksDim);
        phy.setConstantValues(phyDH[i]);
    }

    /*
     * indx is an array of instances of Assembly indices which holds the
     * right instances for each thread.
     */
    indx = new AssemblyIndices[nThreads];

    std::size_t* s = new std::size_t[nThreads];
    std::size_t idealSize = (M->nSqrBcks - M->nSqrBcks % nThreads) / nThreads;

    for (std::size_t i = 0; i < (M->nSqrBcks % nThreads); ++i)
        s[i] = idealSize + 1;
    for (std::size_t i = (M->nSqrBcks % nThreads); i < nThreads; ++i)
        s[i] = idealSize;
    for (std::size_t i = 1; i < nThreads; ++i)
        s[i] += s[i - 1];

    indx[0].setIndices(0, s[0] - 1, M->sqrBcksDim, M->rectBcksDim);
    for (std::size_t i = 1; i < nThreads; ++i)
        indx[i].setIndices(s[i - 1], s[i] - 1, M->sqrBcksDim, M->rectBcksDim);

    delete[] s;

    if (M->zeroBcksDim == 0) {
        CDMNUBTS_is_applicable = false;
        if (bdry[0]->BCtype == 1) {
            CDMBTS_is_applicable = true;
            N = new CDMBTS(M->mDiag, M->mLower, M->mUpper, M->RHS, M->nSqrBcks,
                    M->sqrBcksDim, nPartitions, nThreads);
        }
        else
            CDMBTS_is_applicable = false;
    }
    else {
        CDMBTS_is_applicable = false;
        if (bdry[0]->BCtype == 1) {
            CDMNUBTS_is_applicable = true;
            NUN = new CDMNUBTS(M->mDiag, M->mLower, M->mUpper, M->RHS,
                    M->sqrBcksDim, M->rectBcksDim, M->nSqrBcks, nPartitions,
                    nThreads);
        }
        else
            CDMNUBTS_is_applicable = false;
    }
}

/*
 * Constructor which allocates when the user sets its pdeMembers
 */
DiscretizationParallelized::DiscretizationParallelized(PhysicsRCD& phy_p,
        std::vector<Bdry*> bdry_p, const std::size_t& PDEDimension_p,
        const std::size_t& ConstraintDimension_p,
        const std::size_t& nOfSpaceMeshPoints_p, const double& dx_p,
        bool hasPressure_p, bool hasVelocity_p)
    : Discretization(phy_p, bdry_p, PDEDimension_p, ConstraintDimension_p,
            nOfSpaceMeshPoints_p, dx_p, hasPressure_p, hasVelocity_p)
    , indx()
    , N()
    , NUN()
    , CDMBTS_is_applicable()
    , CDMNUBTS_is_applicable()
{
    AllocateParallelData();
}

/*
 * Destructor
 */
DiscretizationParallelized::~DiscretizationParallelized()
{
    delete[] indx;

    if (CDMBTS_is_applicable)
        delete N;
    if (CDMNUBTS_is_applicable)
        delete NUN;
}

/*
 * This function calculates an array that saves computations when
 * executing the products B_i * (V_i-V_{i-1}) and B_i * (V_{i+1}-V_i)
 */
void
DiscretizationParallelized::CalculatedV()
{
    /*
     * Left Boundary, here it is calculated V_0-V_N, in order to set
     * periodic boundary conditions, if this is the case
     */
    for (std::size_t i = 0, iP = (M->RHSArraySize - M->sqrBcksDim);
            i < M->sqrBcksDim; ++i, ++iP)
        dV[i] = V[i] - V[iP];

    /*
     * Inside Domain, here it is calculated V_i-V_{i-1}
     */
    for (std::size_t i = M->sqrBcksDim, iP = 0; i != M->RHSArraySize; ++iP, ++i)
        dV[i] = V[i] - V[iP];

    /*
     * Right Boundary, here it is calculated V_0-V_N, in order to set
     * periodic boundary conditions, if this is the case
     */
    for (std::size_t i = M->RHSArraySize,
                     iP = (M->RHSArraySize - M->sqrBcksDim), iC = 0;
            i != (M->RHSArraySize + M->sqrBcksDim); ++iP, ++i, ++iC)
        dV[i] = V[iC] - V[iP];
}

/*
 * Calculates the right hand side of the equation F(V^{j+1})=Y(V^{j})
 */
void
DiscretizationParallelized::SetRHS(const double& t)
{
    CalculatedV();

#ifdef ENABLE_OPENMP
#pragma omp parallel default(shared)
    {
        std::size_t i, tID = omp_get_thread_num();
#else
    for (std::size_t tID = 0; tID < nThreads; ++tID) {
        std::size_t i;
#endif /* ENABLE_OPENMP */
        std::size_t iCG = indx[tID].iCG, iPG = indx[tID].iPG,
                    iNG = indx[tID].iNG;

        std::size_t lG = (indx[tID].iEnd + 1) * M->sqrBcksDim,
                    l = (indx[tID].iEnd - 1);

        /*
         * Filling up with zeros is necessary because as the linear
         * systems is solved, the matrix is changed. Once the following
         * operations add stuff to the matrix, it is necessary to have
         * clean matrix
         */
        for (i = indx[tID].iStart * M->sqrBcksDim; i < lG; ++i)
            RHS[i] = 0.0;

        /*
         * This first part o the algorithm works adding stuff dividing the
         * total work among threads. Each thread adds leaving behind one
         * index for its right boundary and one for its left boundary. As
         * a result, there is no chance for one thread access memory already
         * being accessed by another
         */
        for (i = indx[tID].iStart + 1; i <= l; ++i) {
            /*
             * The function jet calculates necessary terms to set up the physics
             * desired by the user
             */
            phy.setPDEAndCstFunctions(
                    phyDH[tID], Vn + iCG, p[i], u[i], x[i], t, i);

            /*
             * The following lines of code use the physics data holder
             * to set up the discretization specified by the user
             */
            for (std::size_t j = 0; j != pdeMembers.size(); ++j)
                pdeMembers[j]->AddMemberToRHS(phyDH[tID], RHS + iPG, RHS + iCG,
                        RHS + iNG, dV + iCG, dV + iNG);

            /*
             * Update of indices is necessary to move on
             */
            iPG = iCG;
            iCG = iNG;
            iNG += M->sqrBcksDim;
        }

#ifdef ENABLE_OPENMP
/*
 * This barrier forces all threads running to have their memory
 * consistent with each other
 */
#pragma omp barrier
#endif /* ENABLE_OPENMP */

        /*
         * The following lines of code fill the gap left behing by the
         * threads. Once each thread left behing to indices (one at its
         * left range and one at its right range), there are still
         * nThreads+1 gaps to fill. The following lines of code force the
         * thread zero to fill the left and right general gaps, while
         * a thread n that is not o fill its left gap and the right gap
         * of the thread n-1.
         */
        if (tID == 0) {
            /*
             * Setting up for index 0
             */
            i = 0;
            iPG = 0;
            iCG = 0;
            iNG = M->sqrBcksDim;
            phy.setPDEAndCstFunctions(
                    phyDH[0], Vn + iCG, p[i], u[i], x[i], t, i);
            for (std::size_t j = 0; j != pdeMembers.size(); ++j)
                pdeMembers[j]->AddMemberToRHS(phyDH[0], DummyMemory, RHS + iCG,
                        RHS + iNG, dV + iCG, dV + iNG);

            /*
             * The following lines of code are necessary only for periodic
             * boundary conditions. If the boundary conditions are not periodic,
             * then they do not influence the results.
             */
            i = M->nRectBcks;
            iPG = (i - 1) * M->sqrBcksDim;
            iCG = i * M->sqrBcksDim;
            iNG = (i + 1) * M->sqrBcksDim;
            for (std::size_t j = 0; j != pdeMembers.size(); ++j)
                pdeMembers[j]->AddMemberToRHS(phyDH[0], RHS + iCG, DummyMemory,
                        DummyMemory, dV + iNG, DummyMemory);

            /*
             * Setting up for last index
             */
            i = M->nRectBcks;
            iPG = (i - 1) * M->sqrBcksDim;
            iCG = i * M->sqrBcksDim;
            iNG = (i + 1) * M->sqrBcksDim;
            phy.setPDEAndCstFunctions(
                    phyDH[0], Vn + iCG, p[i], u[i], x[i], t, i);
            for (std::size_t j = 0; j != pdeMembers.size(); ++j)
                pdeMembers[j]->AddMemberToRHS(phyDH[0], RHS + iPG, RHS + iCG,
                        DummyMemory, dV + iCG, dV + iNG);

            /*
             * The following lines of code are necessary only for periodic
             * boundary conditions. If the boundary conditions are not periodic,
             * then they do not influence the results.
             */
            i = 0;
            iPG = 0;
            iCG = 0;
            iNG = M->sqrBcksDim;
            for (std::size_t j = 0; j != pdeMembers.size(); ++j)
                pdeMembers[j]->AddMemberToRHS(phyDH[0], DummyMemory,
                        DummyMemory, RHS + iCG, DummyMemory, dV + iCG);
        }
        else
            for (i = (indx[tID].iStart - 1); i <= indx[tID].iStart; ++i) {
                iPG = (i - 1) * M->sqrBcksDim;
                iCG = (i) *M->sqrBcksDim;
                iNG = (i + 1) * M->sqrBcksDim;

                /*
                 * The function jet calculates necessary terms to set up the
                 ***physics
                 * desired by the user
                 */
                phy.setPDEAndCstFunctions(
                        phyDH[tID], Vn + iCG, p[i], u[i], x[i], t, i);

                for (std::size_t j = 0; j != pdeMembers.size(); ++j)
                    pdeMembers[j]->AddMemberToRHS(phyDH[tID], RHS + iPG,
                            RHS + iCG, RHS + iNG, dV + iCG, dV + iNG);
            }
    }

    SetBC_RHS(t);
}

/*
 * Calculates the left hand side of the equation F(V^{j+1})=Y(V^{j})
 * Calculates the jacobian of G(V^{j+1})=F(V^{j+1})-Y(V^{j})
 */
void
DiscretizationParallelized::SetJacAndLHS(
        const double& tPrev, const double& tCurr)
{
    CalculatedV();

#ifdef ENABLE_OPENMP
#pragma omp parallel default(shared)
    {
        std::size_t tID = omp_get_thread_num();
#else
    for (std::size_t tID = 0; tID < nThreads; ++tID) {
#endif /* ENABLE_OPENMP */
        std::size_t iCG = indx[tID].iCG, iPG = indx[tID].iPG,
                    iNG = indx[tID].iNG, iPJG = indx[tID].iPJG,
                    iCJG = indx[tID].iCJG, iNJG = indx[tID].iNJG,
                    iPRJG = indx[tID].iPRJG, iCRJG = indx[tID].iCRJG;

        std::size_t lG = (indx[tID].iEnd + 1) * M->sqrBcksDim,
                    lSJG = (indx[tID].iEnd + 1) * M->sqrBcksSize,
                    lRJG = tID == (nThreads - 1) ?
                indx[tID].iEnd * M->rectBcksSize :
                (indx[tID].iEnd + 1) * M->rectBcksSize,
                    l = (indx[tID].iEnd - 1);

        /*
         * Filling up with zeros is necessary because as the linear
         * systems is solved, the matrix is changed. Once the following
         * operations add stuff to the matrix, it is necessary to have
         * clean matrix
         */
        for (std::size_t i = indx[tID].iStart * M->sqrBcksDim; i < lG; ++i)
            LHS[i] = 0.0;

        for (std::size_t i = indx[tID].iStart * M->sqrBcksSize; i < lSJG; ++i)
            M->mDiag[i] = 0.0;

        for (std::size_t i = indx[tID].iStart * M->rectBcksSize; i < lRJG; ++i)
            M->mUpper[i] = M->mLower[i] = 0.0;

        /*
         * This first part o the algorithm works adding stuff dividing the
         * total work among threads. Each thread adds leaving behind one
         * index for its right boundary and one for its left boundary. As
         * a result, there is no chance for one thread access memory already
         * being accessed by another
         */
        for (std::size_t i = indx[tID].iStart + 1; i <= l; ++i) {
            /*
             * The function jet calculates necessary terms to set up the physics
             * desired by the user
             */
            phy.jet(phyDH[tID], V + iCG, Vn + iCG, p[i], u[i], x[i], tCurr, i);

            /*
             * The following lines of code use the physics data holder
             * to set up the discretization specified by the user
             */
            for (std::size_t j = 0; j != pdeMembers.size(); ++j)
                pdeMembers[j]->AddMemberToLHS(phyDH[tID], LHS + iPG, LHS + iCG,
                        LHS + iNG, dV + iCG, dV + iNG);

            for (std::size_t j = 0; j != pdeMembers.size(); ++j)
                pdeMembers[j]->AddMemberToJG(phyDH[tID], M->mDiag + iPJG,
                        M->mDiag + iCJG, M->mDiag + iNJG, M->mUpper + iPRJG,
                        M->mUpper + iCRJG, M->mLower + iPRJG, M->mLower + iCRJG,
                        dV + iCG, dV + iNG);

            /*
             * Update of indices is necessary to move on
             */
            iPG = iCG;
            iCG = iNG;
            iNG += M->sqrBcksDim;

            iPJG = iCJG;
            iCJG = iNJG;
            iNJG += M->sqrBcksSize;

            iPRJG = iCRJG;
            iCRJG += M->rectBcksSize;
        }

#ifdef ENABLE_OPENMP
/*
 * This barrier forces all threads running to have their memory
 * consistent with each other
 */
#pragma omp barrier
#endif /* ENABLE_OPENMP */

        /*
         * The following lines of code fill the gap left behing by the
         * threads. Once each thread left behing to indices (one at its
         * left range and one at its right range), there are still
         * nThreads+1 gaps to fill. The following lines of code force the
         * thread zero to fill the left and right general gaps, while
         * a thread n that is not o fill its left gap and the right gap
         * of the thread n-1.
         */
        if (tID == 0) {
            for (std::size_t i = 0; i != M->rectBcksSize; ++i)
                M->leftBdryExtraBck[i] = M->rightBdryExtraBck[i] = 0.0;

            /*
             * Setting up for index 0
             */
            std::size_t i = 0;
            iPG = 0;
            iCG = 0;
            iNG = M->sqrBcksDim;
            iPJG = 0;
            iCJG = 0;
            iNJG = M->sqrBcksSize;
            iPRJG = 0;
            iCRJG = 0;
            /*
             * The function jet calculates necessary terms to set up the physics
             * desired by the user
             */
            phy.jet(phyDH[0], V + iCG, Vn + iCG, p[i], u[i], x[i], tCurr, i);

            for (std::size_t j = 0; j != pdeMembers.size(); ++j)
                pdeMembers[j]->AddMemberToLHS(phyDH[0], DummyMemory, LHS + iCG,
                        LHS + iNG, dV + iCG, dV + iNG);

            for (std::size_t j = 0; j != pdeMembers.size(); ++j)
                pdeMembers[j]->AddMemberToJG(phyDH[0], DummyMemory,
                        M->mDiag + iCJG, M->mDiag + iNJG, DummyMemory,
                        M->mUpper + iCRJG, M->leftBdryExtraBck,
                        M->mLower + iCRJG, dV + iCG, dV + iNG);

            /*
             * The following lines of code are necessary only for periodic
             * boundary conditions. If the boundary conditions are not periodic,
             * then they do not influence the results.
             */
            i = M->nRectBcks;
            iPG = (i - 1) * M->sqrBcksDim;
            iCG = i * M->sqrBcksDim;
            iNG = (i + 1) * M->sqrBcksDim;
            iPJG = (i - 1) * M->sqrBcksSize;
            iCJG = i * M->sqrBcksSize;
            iNJG = (i + 1) * M->sqrBcksSize;
            iPRJG = (i - 1) * M->rectBcksSize;
            iCRJG = i * M->rectBcksSize;

            for (std::size_t j = 0; j != pdeMembers.size(); ++j)
                pdeMembers[j]->AddMemberToLHS(phyDH[0], LHS + iCG, DummyMemory,
                        DummyMemory, dV + iNG, DummyMemory);

            for (std::size_t j = 0; j != pdeMembers.size(); ++j)
                pdeMembers[j]->AddMemberToJG(phyDH[0], M->mDiag + iCJG,
                        DummyMemory, DummyMemory, M->rightBdryExtraBck,
                        DummyMemory, DummyMemory, DummyMemory, dV + iNG,
                        DummyMemory);

            i = M->nRectBcks;
            iPG = (i - 1) * M->sqrBcksDim;
            iCG = i * M->sqrBcksDim;
            iNG = (i + 1) * M->sqrBcksDim;
            iPJG = (i - 1) * M->sqrBcksSize;
            iCJG = i * M->sqrBcksSize;
            iNJG = (i + 1) * M->sqrBcksSize;
            iPRJG = (i - 1) * M->rectBcksSize;
            iCRJG = i * M->rectBcksSize;
            /*
             * The function jet calculates necessary terms to set up the physics
             * desired by the user
             */
            phy.jet(phyDH[0], V + iCG, Vn + iCG, p[i], u[i], x[i], tCurr, i);

            /*
             * Setting up for last index
             */
            for (std::size_t j = 0; j != pdeMembers.size(); ++j)
                pdeMembers[j]->AddMemberToLHS(phyDH[0], LHS + iPG, LHS + iCG,
                        DummyMemory, dV + iCG, dV + iNG);

            for (std::size_t j = 0; j != pdeMembers.size(); ++j)
                pdeMembers[j]->AddMemberToJG(phyDH[0], M->mDiag + iPJG,
                        M->mDiag + iCJG, DummyMemory, M->mUpper + iPRJG,
                        M->rightBdryExtraBck, M->mLower + iPRJG, DummyMemory,
                        dV + iCG, dV + iNG);

            /*
             * The following lines of code are necessary only for periodic
             * boundary conditions. If the boundary conditions are not periodic,
             * then they do not influence the results.
             */
            i = 0;
            iPG = 0;
            iCG = 0;
            iNG = M->sqrBcksDim;
            iPJG = 0;
            iCJG = 0;
            iNJG = M->sqrBcksSize;
            iPRJG = 0;
            iCRJG = 0;

            for (std::size_t j = 0; j != pdeMembers.size(); ++j)
                pdeMembers[j]->AddMemberToLHS(phyDH[0], DummyMemory,
                        DummyMemory, LHS + iCG, DummyMemory, dV + iCG);

            for (std::size_t j = 0; j != pdeMembers.size(); ++j)
                pdeMembers[j]->AddMemberToJG(phyDH[0], DummyMemory, DummyMemory,
                        M->mDiag + iCJG, DummyMemory, DummyMemory, DummyMemory,
                        M->leftBdryExtraBck, DummyMemory, dV + iCG);
        }
        else
            for (std::size_t i = (indx[tID].iStart - 1); i <= indx[tID].iStart;
                    ++i) {
                iPG = (i - 1) * M->sqrBcksDim;
                iCG = (i) *M->sqrBcksDim;
                iNG = (i + 1) * M->sqrBcksDim;

                iPJG = (i - 1) * M->sqrBcksSize;
                iCJG = (i) *M->sqrBcksSize;
                iNJG = (i + 1) * M->sqrBcksSize;

                iPRJG = (i - 1) * M->rectBcksSize;
                iCRJG = (i) *M->rectBcksSize;

                /*
                 * The function jet calculates necessary terms to set up the
                 ***physics
                 * desired by the user
                 */
                phy.jet(phyDH[tID], V + iCG, Vn + iCG, p[i], u[i], x[i], tCurr,
                        i);

                for (std::size_t j = 0; j != pdeMembers.size(); ++j)
                    pdeMembers[j]->AddMemberToLHS(phyDH[tID], LHS + iPG,
                            LHS + iCG, LHS + iNG, dV + iCG, dV + iNG);

                for (std::size_t j = 0; j != pdeMembers.size(); ++j)
                    pdeMembers[j]->AddMemberToJG(phyDH[tID], M->mDiag + iPJG,
                            M->mDiag + iCJG, M->mDiag + iNJG, M->mUpper + iPRJG,
                            M->mUpper + iCRJG, M->mLower + iPRJG,
                            M->mLower + iCRJG, dV + iCG, dV + iNG);
            }
    }

    SetBC_JacAndLHS(tPrev, tCurr);
    /*
       std::cout << "D2 = [ " ;
       for(std::size_t i=0 ; i!= M->sqrArraySize ; ++i)
        std::cout << M->mDiag[i] << " " ;
       std::cout << "];" << std::endl;
       std::cout << "L2 = [ " ;
       for(std::size_t i=0 ; i!= M->rectArraySize ; ++i)
        std::cout << M->mLower[i] << " " ;
       std::cout << "];" << std::endl;
       std::cout << "U2 = [ " ;
       for(std::size_t i=0 ; i!= M->rectArraySize ; ++i)
        std::cout << M->mUpper[i] << " " ;
       std::cout << "];" << std::endl;
       std::cout << "LHS2 = [ " ;
       for(std::size_t i=0 ; i!= M->RHSArraySize ; ++i)
        std::cout << LHS[i] << " " ;
       std::cout << "];" << std::endl;
       std::cout << "RHS2 = [ " ;
       for(std::size_t i=0 ; i!= M->RHSArraySize ; ++i)
        std::cout << RHS[i] << " " ;
       std::cout << "];" << std::endl;
       std::cout << "dV2 = [ " ;
       for(std::size_t i=0 ; i!= M->RHSArraySize ; ++i)
        std::cout << dV[i] << " " ;
       std::cout << "];\n" << std::endl;
       exit(1);
     */

    // double aux;
    // for(std::size_t i=0 ; i<5 ; ++i)
    //{
    // aux = M->mDiag[10+i];
    // M->mDiag[10+i] = M->mDiag[20+i];
    // M->mDiag[20+i] = aux;

    // aux = M->mUpper[i];
    // M->mUpper[i] = M->mUpper[10+i];
    // M->mUpper[10+i] = aux;
    //}

    // aux = LHS[2] ;
    // LHS[2] = LHS[4];
    // LHS[4] = aux;

    // aux = RHS[2] ;
    // RHS[2] = RHS[4];
    // RHS[4] = aux;

    // for(std::size_t i=0 ; i<5 ; ++i)
    //{
    // aux = M->mDiag[M->sqrArraySize - M->sqrBcksSize+ 10+i];
    // M->mDiag[M->sqrArraySize - M->sqrBcksSize+ 10+i] =
    // M->mDiag[M->sqrArraySize - M->sqrBcksSize+ 20+i];
    // M->mDiag[M->sqrArraySize - M->sqrBcksSize+ 20+i] = aux;

    // aux = M->mUpper[M->rectArraySize - M->rectBcksSize+i];
    // M->mUpper[M->rectArraySize - M->rectBcksSize+i] =
    // M->mUpper[M->rectArraySize - M->rectBcksSize+10+i];
    // M->mUpper[M->rectArraySize - M->rectBcksSize+10+i] = aux;
    //}

    // aux = LHS[M->RHSArraySize-M->sqrBcksDim+2] ;
    // LHS[M->RHSArraySize-M->sqrBcksDim+2] =
    // LHS[M->RHSArraySize-M->sqrBcksDim+4];
    // LHS[M->RHSArraySize-M->sqrBcksDim+4] = aux;

    // aux = RHS[M->RHSArraySize-M->sqrBcksDim+2] ;
    // RHS[M->RHSArraySize-M->sqrBcksDim+2] =
    // RHS[M->RHSArraySize-M->sqrBcksDim+4];
    // RHS[M->RHSArraySize-M->sqrBcksDim+4] = aux;
}
