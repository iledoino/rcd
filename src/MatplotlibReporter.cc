#include "MatplotlibReporter.h"

#ifdef ENABLE_MATPLOTLIB

void
MatplotlibReporter::make(const std::vector<std::vector<double>>& solution,
        const std::vector<double>& spcMesh, double time, bool /* makeanyway */)
{
    auto nGroups = reportGroups_.size();
    if (nGroups == 0)
        return;

    for (std::size_t i = 0; i < nGroups; ++i) {
        plt::figure(i + 1);
        plt::clf();
        plt::title(spaceNames_[i] + " at t=" + std::to_string(time));
        if (not spaceRangeAutomatic_) {
            auto xmin = xl_;
            auto xmax = xr_;
            plt::xlim(xl_, xr_);
        }
        for (std::size_t j = 0; j < reportGroups_[i].size(); ++j) {
            auto curveIndex = reportGroups_[i][j];
            auto curveName = std::string("curve ") + std::to_string(curveIndex);
            plt::named_plot(curveName, spcMesh, solution[curveIndex]);
        }
        plt::legend();
        plt::pause(1.0e-5);
    }
}

void
MatplotlibReporter::finish()
{
    auto nGroups = reportGroups_.size();
    if (nGroups == 0)
        return;

    for (std::size_t i = 0; i < nGroups; ++i) {
        plt::figure(i + 1);
        plt::save(spaceNames_[i] + ".pdf");
    }
}

#endif /* ENABLE_MATPLOTLIB */
