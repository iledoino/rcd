#ifndef SRC_INTERFACE_GNUPLOTREPORTER_H
#define SRC_INTERFACE_GNUPLOTREPORTER_H

#ifdef ENABLE_GNUPLOT

#include "PlotReporter.h"
#include "gnuplot_i.h"

class GnuplotReporter : public PlotReporter
{
public:
    GnuplotReporter(std::vector<std::vector<std::size_t>> reportGroups_p,
            std::string xAxisName_p, std::vector<std::string> yAxesNames_p)
        : PlotReporter(reportGroups_p, xAxisName_p, yAxesNames_p)
    {
    }
    virtual ~GnuplotReporter() noexcept = default;

    void start() override;
    void make(const std::vector<std::vector<double>>& solution,
            const std::vector<double>& spcMesh, double time,
            bool makeanyway = false) override;
    void finish() override;

private:
    std::vector<gnuplot_ctrl*> gnuplots_{};
};

#endif /* ENABLE_GNUPLOT */

#endif /* SRC_INTERFACE_GNUPLOTREPORTER_H */
