#include "PlotReporter.h"
#include <cmath>
#include <limits>

#ifdef ENABLE_OPENMP
#include <omp.h>

PlotReporter::PlotReporter() : prevRunTime(omp_get_wtime()) {}
#else
PlotReporter::PlotReporter() : prevRunTime(std::clock()) {}
#endif /* ENABLE_OPENMP */

void PlotReporter::spaceRange(/* automatic */)
{
    spaceRangeAutomatic_ = true;
}

void
PlotReporter::spaceRange(double xl_p, double xr_p)
{
    spaceRangeAutomatic_ = false;
    xl_ = xl_p;
    xr_ = xr_p;
}

void
PlotReporter::start()
{
    auto nGroups = reportGroups_.size();
    if (nGroups == 0)
        return;

    spaceNames_.resize(nGroups);
    for (std::size_t i = 0; i < nGroups; ++i) {
        auto yAxisName = (i < yAxesNames_.size() ? yAxesNames_[i] :
                                                   std::string("unknown"));
        auto spaceName = "(" + xAxisName_ + ", " + yAxisName + ")-space";
        spaceNames_[i] = spaceName;
    }

#ifdef ENABLE_OPENMP
    prevRunTime = omp_get_wtime();
#else
    prevRunTime
            = std::clock() - static_cast<std::clock_t>(0.6 * CLOCKS_PER_SEC);
#endif /* ENABLE_OPENMP */
}

bool
PlotReporter::timetomake()
{
#ifdef ENABLE_OPENMP
    auto elapsedTime = omp_get_wtime() - prevRunTime;
    bool timetomake_p = static_cast<double>(elapsedTime) > frameSecUpdate_;
    if (timetomake_p)
        prevRunTime = omp_get_wtime();
#else
    auto elapsedTime = std::clock() - prevRunTime;
    bool timetomake_p = static_cast<double>(elapsedTime) / CLOCKS_PER_SEC
            > frameSecUpdate_;
    if (timetomake_p)
        prevRunTime = std::clock();
#endif /* ENABLE_OPENMP */
    return timetomake_p;
}
