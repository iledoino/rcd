#include "Eigenproblem.h"

Eigenproblem::Eigenproblem() {}

Eigenproblem::~Eigenproblem() {}

int
Eigenproblem::solve(const std::vector<double>& A, const std::vector<double>& B,
        std::vector<double>& vl, std::vector<double>& vr,
        std::vector<double>& ev_r, std::vector<double>& ev_i,
        std::vector<bool>& isReal)
{
    int dimSqrd(static_cast<int>(A.size()));
    int dim(static_cast<int>(std::sqrt(static_cast<double>(A.size()))));

    std::vector<double> AA(dimSqrd);
    std::vector<double> BB(dimSqrd);
    for (int i = 0; i < dim; ++i)
        for (int j = 0; j < dim; ++j) {
            AA[i * dim + j] = A[j * dim + i];
            BB[i * dim + j] = B[j * dim + i];
        }

    int lda = std::max(1, dim);
    int ldb = std::max(1, dim);

    // Eigenvalues of the form:
    //
    // (alphar[j] + i*alphai[j])/beta[j] for j = 0,..., (n - 1).
    //
    std::vector<double> alphar(dim), alphai(dim), beta(dim);

    // The leading dimension of the matrix vl.
    // ldlvl >= 1, and if jovl = "V", ldvl >= n.
    //
    int ldvl = dim;
    vl.resize(ldvl * dim);

    // The leading dimension of the matrix vr.
    // ldvr >= 1, and if jobvr = "V", ldvr >= n.
    int ldvr = dim;
    vr.resize(ldvr * dim);

    // Working array
    int lwork = std::max(1, 8 * dim);
    std::vector<double> work(lwork);

    // Info
    int info = 0;

    // Invoke LAPACK
    dggev_("V", "V", &dim, AA.data(), &lda, BB.data(), &ldb, alphar.data(),
            alphai.data(), beta.data(), vl.data(), &ldvl, vr.data(), &ldvr,
            work.data(), &lwork, &info);

    std::vector<double> vlT(ldvl * dim), vrT(ldvr * dim);
    for (int i = 0; i < dim; ++i)
        for (int j = 0; j < dim; ++j) {
            vlT[i * dim + j] = vl[j * dim + i];
            vrT[i * dim + j] = vr[j * dim + i];
        }
    vlT.swap(vl);
    vrT.swap(vr);

    ev_r.resize(dim);
    ev_i.resize(dim);
    isReal.resize(dim);
    double epsilon(1e-10);
    if (info == 0) {
        // Abort if some beta is smaller than sum_i(abs(alphar(i)) +
        // abs(alphai(i))).
        double sum = 0;
        for (int i = 0; i < dim; i++)
            sum += fabs(alphar[i]) + fabs(alphai[i]);
        sum *= epsilon;

        int max = 0;
        for (int i = 0; i < dim; i++)
            if (fabs(beta[i]) > sum)
                max++;
        if (max == 0)
            return EIGENPROBLEM_BETA_NEAR_ZERO;

        int pos = 0;
        for (int i = 0; i < dim; i++) {
            if (fabs(beta[i]) > sum) {
                if (fabs(alphai[i]) < epsilon) {
                    ev_r[pos] = alphar[i] / beta[i];
                    ev_i[pos] = alphai[i] / beta[i];

                    // Normalize
                    // to do: use the transpose!!!
                    double norm_l(0.0), norm_r(0.0);
                    for (int j = 0; j < dim; j++) {
                        norm_l += vl[i + j * dim] * vl[i + j * dim];
                        norm_r += vr[i + j * dim] * vr[i + j * dim];
                    }
                    norm_l = std::sqrt(norm_l);
                    norm_r = std::sqrt(norm_r);

                    for (int j = 0; j < dim; j++) {
                        vl[i + j * dim] /= norm_l;
                        vr[i + j * dim] /= norm_r;
                    }

                    isReal[pos] = true;
                }
                else {
                    /*
                     * Eigenvalue is complex.
                     * In this case the i-th column of v contains the real part
                     * of the eigenvector and the (i + 1)-th column contains the
                     * imaginary part of the eigenvector.
                     *
                     * If the eigenvalues are complex they are returned by
                     * Lapack in conjugated pairs,
                     * the first of the pair having positive imaginary part.
                     */
                    ev_r[pos] = alphar[i] / beta[i];
                    ev_i[pos] = alphai[i] / beta[i];
                    ev_r[pos + 1] = ev_r[pos];
                    ev_i[pos + 1] = -ev_i[pos];

                    // Normalize
                    double norm_l(0.0), norm_r(0.0);
                    for (int j = 0; j < dim; j++) {
                        norm_l += vl[i + j * dim] * vl[i + j * dim]
                                + vl[i + 1 + j * dim] * vl[i + 1 + j * dim];
                        norm_r += vr[i + j * dim] * vr[i + j * dim]
                                + vr[i + 1 + j * dim] * vr[i + 1 + j * dim];
                    }
                    norm_l = std::sqrt(norm_l);
                    norm_r = std::sqrt(norm_r);

                    for (int j = 0; j < dim; j++) {
                        vl[i + j * dim] /= norm_l;
                        vl[i + 1 + j * dim] /= norm_l;

                        vr[i + j * dim] /= norm_r;
                        vr[i + 1 + j * dim] /= norm_r;
                    }

                    isReal[pos] = true;
                    isReal[pos + 1] = true;

                    // Skip one step!
                    i++;
                }
                pos++;
            }
            else {
                // printf("Eigenvalue discarded: %d\n", pos);
            }
        }
    }

    return EIGENPROBLEM_SUCCESS;
}
