#ifndef SRC_DCT_PDEMEMBER_H
#define SRC_DCT_PDEMEMBER_H


#include "physicsRCD.h"
#include <cmath>
#include <iostream>
#include <string>
#include <vector>

class Discretization;

/*
 * DESCRIPTION: This class provides a way to set up a discretizatio of
 * a PDE Member (that is, an accumulation, flux, diffusion, reaction or
 * constraint function). Since each PDE member has its own type of
 * discretization, we call this class PDE Member instead of Discretization
 * of a PDE Member.
 *
 * WHAT IT DOES: This class is to be used to set up the matrix and RHS
 * necessary to solve the Newton Method. The Newton Method solves the equation
 *
 * F(V^{n+1})=Y(V^{n}),
 *
 * where V^{n+1} is the variable to be found. In order to numerically solve
 * this, it is necessary to solve a system of form
 *
 * JG( G(V^{n+1}) ) _{l+1} deltaV = G(V^{n+1}) _{l}
 *
 * where G = F-Y, and JG is the jacobian operator.
 * Below, we call the array which holds F(V^{n+1}) as
 * LHS, and the array which holds Y(V^{n}) as RHS.
 */
class PDEMember
{
    /*
     * Discretization class has full access to this class
     */
    friend class Discretization;

public:
    /*
     * This constant regulates the time average of the PDE member.
     * Let's say it is a flux member. Then, its time average will be
     *
     * (F_x)^{n+\alpha} = \alpha (F_x)^{n+1} + (1-\alpha) (F_x)^{n}
     *
     * By choosing \alpha=1 to this constant, the user will be using
     * a fully implicit finite difference, and by choosing \alpha=0,
     * the user will be using an explicit finite difference.
     */
    double lambda_, alpha_, ctL_, ctR_;
    const std::string memberName_;

    /*
     * Those funtions are the ones who really set up the LHS and RHS
     * array. A deeper knowlege of this class and how it is supposed
     * to be used is necessary in order to instantiate these functions
     * in a child class
     */
    virtual void
    AddMemberToRHS(PhysicsDataHolder& /* PDH */, double* /* RHS_iMinus1 */,
            double* /* RHS_i */, double* /* RHS_iPlus1 */, double* /* dV_i */,
            double* /* dV_iPlus1 */)
    {
    }
    virtual void
    AddMemberToLHS(PhysicsDataHolder& /* PDH */, double* /* LHS_iMinus1 */,
            double* /* LHS_i */, double* /* LHS_iPlus1 */, double* /* dV_i */,
            double* /* dV_iPlus1 */)
    {
    }
    virtual void
    AddMemberToJG(PhysicsDataHolder& /* PDH */, double* /* JGDiag_iMinus1 */,
            double* /* JGDiag_i */, double* /* JGDiag_iPlus1 */,
            double* /* JGUpper_iMinus1 */, double* /* JGUpper_i */,
            double* /* JGLower_i */, double* /* JGLower_iPlus1 */,
            double* /* dV_i */, double* /* dV_iPlus1 */)
    {
    }

    /*
     * This function is to be used exclusively by the PDE Member related
     * to the accumulation term. For any other instance, this function
     * should do nothing
     */
    virtual void SetLambda(PhysicsDataHolder& /* PDH */, const double& /* dt */)
    {
    }
    /*
     * This function is to modify the PhysicsDataHolder class instance
     * in order to set up constants related to the alpha constant
     */
    virtual void
    SetAlphaAndBeta(PhysicsDataHolder& /* PDH */, const double& /* dx */)
    {
    }

    /*
     * Construction
     */
    PDEMember(double lambda_p, double alpha_p, std::string memberName_p)
        : lambda_(lambda_p)
        , alpha_(alpha_p)
        , ctL_()
        , ctR_()
        , memberName_(memberName_p)
    {
    }
    virtual ~PDEMember();
};

/*
 * class to set up PDEMembers inside discretization
 */
class PDEMemberConfig
{
public:
    PDEMemberConfig() = default;
    ~PDEMemberConfig() noexcept = default;

    /* getters */
    bool hasFlux() const { return hasFlux_; }
    bool fluxIsUpwind() const { return fluxIsUpwind_; }
    bool hasDiffusion() const { return hasDiffusion_; }
    bool diffIsConstant() const { return diffIsConstant_; }
    bool hasReaction() const { return hasReaction_; }
    bool constraintIsRelaxed() const { return constraintIsRelaxed_; }

    double impParamFlux() const { return impParamFlux_; }
    double impParamDiffusion() const { return impParamDiffusion_; }
    double impParamReaction() const { return impParamReaction_; }
    double impParamConstraint() const { return impParamConstraint_; }
    double constraintRelaxation() const { return constraintRelaxation_; }

    /* setters */
    bool& hasFlux() { return hasFlux_; }
    bool& fluxIsUpwind() { return fluxIsUpwind_; }
    bool& hasDiffusion() { return hasDiffusion_; }
    bool& diffIsConstant() { return diffIsConstant_; }
    bool& hasReaction() { return hasReaction_; }
    bool& constraintIsRelaxed() { return constraintIsRelaxed_; }

    double& impParamFlux() { return impParamFlux_; }
    double& impParamDiffusion() { return impParamDiffusion_; }
    double& impParamReaction() { return impParamReaction_; }
    double& impParamConstraint() { return impParamConstraint_; }
    double& constraintRelaxation() { return constraintRelaxation_; }

private:
    bool hasFlux_ = true, fluxIsUpwind_ = true, hasDiffusion_ = true;
    bool diffIsConstant_ = true, hasReaction_ = true;
    bool constraintIsRelaxed_ = false;
    double impParamFlux_ = 0.5, impParamDiffusion_ = 0.5;
    double impParamReaction_ = 0.5, impParamConstraint_ = 1.0;
    double constraintRelaxation_ = 0.0;
};

/*
 * CLASSES THAT ARE CHILD OF PDEMember class
 *
 * These classes are to set up discretizations for each one of the functions
 * G (accumulation), F (flux), B (diffusion), R (reaction) and H (constraint),
 * where
 *
 * ∂/∂t G(V) + ∂/∂x F(V) = ∂/∂x( B(V) ∂V/∂x ) + R(V)
 *                  H(V) = 0
 *
 *
 * For each one of the accepted points (x_m,t_n), we are interested on
 * solving the discretized equation that is approximated on time by
 *
 * (G^{n+1}-G^{n})/dt + (alpha ∂/∂x F(V)^{n+1} + beta ∂/∂x F(V)^{n}) = (alpha
 ***∂/∂x( B(V) ∂V/∂x )^{n+1} + beta ∂/∂x( B(V) ∂V/∂x )^{n}) + (alpha R^{n+1} +
 ***beta R^{n})
 *                              (alpha H^{n+1} + beta H^{n}) = 0
 *
 * where beta = 1 - alpha. For details of what is alpha, go to its
 * description in PDEMember class
 */

/*
 * This function sets up whats necessary for the following discretization
 *
 * ∂/∂t G(V)^{n+alpha} = (G^{n+1}-G^{n})/dt
 */
class Accumulation : public PDEMember
{
public:
    void
    AddMemberToRHS(PhysicsDataHolder& PDH, double* RHS_iMinus1, double* RHS_i,
            double* RHS_iPlus1, double* dV_i, double* dV_iPlus1) override;
    void
    AddMemberToLHS(PhysicsDataHolder& PDH, double* LHS_iMinus1, double* LHS_i,
            double* LHS_iPlus1, double* dV_i, double* dV_iPlus1) override;
    void AddMemberToJG(PhysicsDataHolder& PDH, double* JGDiag_iMinus1,
            double* JGDiag_i, double* JGDiag_iPlus1, double* JGUpper_iMinus1,
            double* JGUpper_i, double* JGLower_i, double* JGLower_iPlus1,
            double* dV_i, double* dV_iPlus1) override;

    void SetLambda(PhysicsDataHolder& /* PDH */, const double& dt) override
    {
        lambda_ = 1.0 / dt;
    }

    Accumulation() : PDEMember(0.0, 0.0, "accumulation") {}
    ~Accumulation() = default;
};

/*
 * This function sets up whats necessary for the following discretization
 *
 * ∂/∂x F(V)^{n+alpha}_m = alpha (F^{n+1}_{m}-F^{n+1}_{m-1})/dx + beta
 ***(F^{n}_{m}-F^{n}_{m-1})/dx
 */
class Flux_UpWind : public PDEMember
{
public:
    void
    AddMemberToRHS(PhysicsDataHolder& PDH, double* RHS_iMinus1, double* RHS_i,
            double* RHS_iPlus1, double* dV_i, double* dV_iPlus1) override;
    void
    AddMemberToLHS(PhysicsDataHolder& PDH, double* LHS_iMinus1, double* LHS_i,
            double* LHS_iPlus1, double* dV_i, double* dV_iPlus1) override;
    void AddMemberToJG(PhysicsDataHolder& PDH, double* JGDiag_iMinus1,
            double* JGDiag_i, double* JGDiag_iPlus1, double* JGUpper_iMinus1,
            double* JGUpper_i, double* JGLower_i, double* JGLower_iPlus1,
            double* dV_i, double* dV_iPlus1) override;

    void
    SetAlphaAndBeta(PhysicsDataHolder& /* PDH */, const double& dx) override
    {
        ctL_ = alpha_ / dx;
        ctR_ = (1.0 - alpha_) / dx;
    }

    Flux_UpWind(const double& alpha_p) : PDEMember(0.0, alpha_p, "flux") {}
    ~Flux_UpWind() = default;
};

/*
 * This function sets up whats necessary for the following discretization
 *
 * ∂/∂x F(V)^{n+alpha}_m = alpha (F^{n+1}_{m+1}-F^{n+1}_{m-1})/(2dx) + beta
 ***(F^{n}_{m+1}-F^{n}_{m-1})/(2dx)
 */
class Flux_CentralDifference : public PDEMember
{
public:
    void
    AddMemberToRHS(PhysicsDataHolder& PDH, double* RHS_iMinus1, double* RHS_i,
            double* RHS_iPlus1, double* dV_i, double* dV_iPlus1) override;
    void
    AddMemberToLHS(PhysicsDataHolder& PDH, double* LHS_iMinus1, double* LHS_i,
            double* LHS_iPlus1, double* dV_i, double* dV_iPlus1) override;
    void AddMemberToJG(PhysicsDataHolder& PDH, double* JGDiag_iMinus1,
            double* JGDiag_i, double* JGDiag_iPlus1, double* JGUpper_iMinus1,
            double* JGUpper_i, double* JGLower_i, double* JGLower_iPlus1,
            double* dV_i, double* dV_iPlus1) override;

    void
    SetAlphaAndBeta(PhysicsDataHolder& /* PDH */, const double& dx) override
    {
        ctL_ = alpha_ * 0.5 / dx;
        ctR_ = (1.0 - alpha_) * 0.5 / dx;
    }

    Flux_CentralDifference(const double& alpha_p)
        : PDEMember(0.0, alpha_p, "flux")
    {
    }
    ~Flux_CentralDifference() = default;
};

/*
 * This function sets up whats necessary for the following discretization
 *
 * ∂/∂x( B(V) ∂V/∂x )^{n+alpha}_m = alpha ∂/∂x( B(V) ∂V/∂x )^{n+1}_m + beta
 ***∂/∂x( B(V) ∂V/∂x )^{n}_m
 *
 * where B is a constant in space and in the unknown V, and
 *
 * ∂/∂x( B ∂V/∂x )_m = (BV_{i+1} - 2BV_{i} + BV_{i-1})/(dx^2)
 */
class Diffusion_Constant : public PDEMember
{
public:
    void
    AddMemberToRHS(PhysicsDataHolder& PDH, double* RHS_iMinus1, double* RHS_i,
            double* RHS_iPlus1, double* dV_i, double* dV_iPlus1) override;
    void
    AddMemberToLHS(PhysicsDataHolder& PDH, double* LHS_iMinus1, double* LHS_i,
            double* LHS_iPlus1, double* dV_i, double* dV_iPlus1) override;
    virtual void AddMemberToJG(PhysicsDataHolder& PDH, double* JGDiag_iMinus1,
            double* JGDiag_i, double* JGDiag_iPlus1, double* JGUpper_iMinus1,
            double* JGUpper_i, double* JGLower_i, double* JGLower_iPlus1,
            double* dV_i, double* dV_iPlus1) override;

    void
    SetAlphaAndBeta(PhysicsDataHolder& /* PDH */, const double& dx) override
    {
        ctL_ = alpha_ / (2.0 * dx * dx);
        ctR_ = (1.0 - alpha_) / (2.0 * dx * dx);
    }

    Diffusion_Constant(const double& alpha_p)
        : PDEMember(0.0, alpha_p, "diffusion")
    {
    }
    virtual ~Diffusion_Constant();
};

/*
 * This function sets up whats necessary for the following discretization
 *
 * ∂/∂x( B(V) ∂V/∂x )^{n+alpha}_m = alpha ∂/∂x( B(V) ∂V/∂x )^{n+1}_m + beta
 ***∂/∂x( B(V) ∂V/∂x )^{n}_m
 *
 * where B is general, and
 *
 * ∂/∂x( B ∂V/∂x )_m = ( (B_{m+1}+B_{m})(V_{m+1}-V_{m}) -
 ***(B_{m}+B_{m-1})(V_{m}-V_{m-1}) )/(2dx^2)
 */
class Diffusion : public Diffusion_Constant
{
public:
    void AddMemberToJG(PhysicsDataHolder& PDH, double* JGDiag_iMinus1,
            double* JGDiag_i, double* JGDiag_iPlus1, double* JGUpper_iMinus1,
            double* JGUpper_i, double* JGLower_i, double* JGLower_iPlus1,
            double* dV_i, double* dV_iPlus1) override;

    Diffusion(const double& alpha_p) : Diffusion_Constant(alpha_p) {}
    ~Diffusion() = default;
};

/*
 * This function sets up whats necessary for the following discretization
 *
 * R^{n+alpha} = alpha R^{n+1} + beta R^{n}
 */
class Reaction : public PDEMember
{
public:
    void
    AddMemberToRHS(PhysicsDataHolder& PDH, double* RHS_iMinus1, double* RHS_i,
            double* RHS_iPlus1, double* dV_i, double* dV_iPlus1) override;
    void
    AddMemberToLHS(PhysicsDataHolder& PDH, double* LHS_iMinus1, double* LHS_i,
            double* LHS_iPlus1, double* dV_i, double* dV_iPlus1) override;
    void AddMemberToJG(PhysicsDataHolder& PDH, double* JGDiag_iMinus1,
            double* JGDiag_i, double* JGDiag_iPlus1, double* JGUpper_iMinus1,
            double* JGUpper_i, double* JGLower_i, double* JGLower_iPlus1,
            double* dV_i, double* dV_iPlus1) override;

    void SetAlphaAndBeta(
            PhysicsDataHolder& /* PDH */, const double& /* dx */) override
    {
        ctL_ = alpha_;
        ctR_ = (1.0 - alpha_);
    }

    Reaction(const double& alpha_p) : PDEMember(0.0, alpha_p, "reaction") {}
    ~Reaction() = default;
};

/*
 * This function sets up whats necessary for the following discretization
 *
 * H^{n+alpha} = alpha H^{n+1} + beta H^{n}
 */
class Constraint : public PDEMember
{
public:
    void
    AddMemberToRHS(PhysicsDataHolder& PDH, double* RHS_iMinus1, double* RHS_i,
            double* RHS_iPlus1, double* dV_i, double* dV_iPlus1) override;
    void
    AddMemberToLHS(PhysicsDataHolder& PDH, double* LHS_iMinus1, double* LHS_i,
            double* LHS_iPlus1, double* dV_i, double* dV_iPlus1) override;
    void AddMemberToJG(PhysicsDataHolder& PDH, double* JGDiag_iMinus1,
            double* JGDiag_i, double* JGDiag_iPlus1, double* JGUpper_iMinus1,
            double* JGUpper_i, double* JGLower_i, double* JGLower_iPlus1,
            double* dV_i, double* dV_iPlus1) override;

    void SetAlphaAndBeta(
            PhysicsDataHolder& /* PDH */, const double& /* dx */) override
    {
        ctL_ = alpha_;
        ctR_ = (1.0 - alpha_);
    }

    Constraint(const double& alpha_p) : PDEMember(0.0, alpha_p, "constraint") {}
    ~Constraint() = default;
};

/*
 * This function sets up whats necessary for the following discretization
 *
 * τ * ∂/∂t H(V)^{n+alpha} + H(V)^{n+alpha} = τ(H^{n+1}-H^{n})/dt + (alpha
 * H^{n+1} + beta H^{n})
 *
 * PS: Here the relaxed time is given by τ
 */
class RelaxedConstraint : public PDEMember
{
public:
    void
    AddMemberToRHS(PhysicsDataHolder& PDH, double* RHS_iMinus1, double* RHS_i,
            double* RHS_iPlus1, double* dV_i, double* dV_iPlus1) override;
    void
    AddMemberToLHS(PhysicsDataHolder& PDH, double* LHS_iMinus1, double* LHS_i,
            double* LHS_iPlus1, double* dV_i, double* dV_iPlus1) override;
    void AddMemberToJG(PhysicsDataHolder& PDH, double* JGDiag_iMinus1,
            double* JGDiag_i, double* JGDiag_iPlus1, double* JGUpper_iMinus1,
            double* JGUpper_i, double* JGLower_i, double* JGLower_iPlus1,
            double* dV_i, double* dV_iPlus1) override;

    void SetAlphaAndBeta(
            PhysicsDataHolder& /* PDH */, const double& /* dx */) override
    {
        ctL_ = alpha_;
        ctR_ = (1.0 - alpha_);
    }

    void SetLambda(PhysicsDataHolder& /* PDH */, const double& dt) override
    {
        lambda_ = relaxationParameter / dt;
    }

    RelaxedConstraint(const double& alpha_p, const double relaxationParameter_p)
        : PDEMember(0.0, alpha_p, "constraint")
        , relaxationParameter(relaxationParameter_p)
    {
    }
    ~RelaxedConstraint() = default;

private:
    const double relaxationParameter;
};

#endif /* SRC_DCT_PDEMEMBER_H */
