#include "keymatch.h"
#include <sstream>

KeyMatch::KeyMatch(std::string defaultInput_, std::string defaultOutput_)
    : map_()
    , defaultInput(defaultInput_)
    , defaultOutput(defaultOutput_)
    , in(defaultInput_)
    , out(defaultOutput_)
    , end_of_file()
    , fp_()
    , end_()
    , line_()
    , pch_()
    , len_()
    , read_()
{
}

template <>
void
KeyMatch::register_entry<double>(
        double* data_, std::size_t size_, std::string key_)
{
    Registry reg_;
    reg_.data_double_ = data_;
    reg_.size_ = size_;
    reg_.type_ = RegistryType::DOUBLE;
    map_.emplace(key_, reg_);
}

template <>
void
KeyMatch::register_entry<int>(int* data_, std::size_t size_, std::string key_)
{
    Registry reg_;
    reg_.data_int_ = data_;
    reg_.size_ = size_;
    reg_.type_ = RegistryType::INTEGER;
    map_.emplace(key_, reg_);
}

template <>
void
KeyMatch::register_entry<unsigned>(
        unsigned* data_, std::size_t size_, std::string key_)
{
    Registry reg_;
    reg_.data_unsigned_ = data_;
    reg_.size_ = size_;
    reg_.type_ = RegistryType::UNSIGNED;
    map_.emplace(key_, reg_);
}

template <>
void
KeyMatch::register_entry<unsigned long>(
        unsigned long* data_, std::size_t size_, std::string key_)
{
    Registry reg_;
    reg_.data_unsigned_long_ = data_;
    reg_.size_ = size_;
    reg_.type_ = RegistryType::UNSIGNEDLONG;
    map_.emplace(key_, reg_);
}

template <>
void
KeyMatch::register_entry<std::string>(
        std::string* data_, std::size_t size_, std::string key_)
{
    Registry reg_;
    reg_.data_stdstring_ = data_;
    reg_.size_ = size_;
    reg_.type_ = RegistryType::STRING;
    map_.emplace(key_, reg_);
}

void
KeyMatch::match(char* pch)
{
    std::string pch_string(pch);
    if (pch_string.back() == '\n')
        pch_string.pop_back();

    auto iter_ = map_.find(pch_string);
    if (iter_ == map_.end())
        return;

    auto& registry_ = iter_->second;
    if (registry_.type_ == RegistryType::DOUBLE) {
        for (std::size_t i = 0; i < registry_.size_; ++i)
            if (readDouble(registry_.data_double_ + i) == 2)
                std::cerr << "WARNING: Please verify the syntax of key " << pch
                          << ".\n";
    }
    else if (registry_.type_ == RegistryType::INTEGER) {
        for (std::size_t i = 0; i < registry_.size_; ++i)
            if (readInt(registry_.data_int_ + i) == 2)
                std::cerr << "WARNING: Please verify the syntax of key " << pch
                          << ".\n";
    }
    else if (registry_.type_ == RegistryType::UNSIGNED) {
        for (std::size_t i = 0; i < registry_.size_; ++i)
            if (readUns(registry_.data_unsigned_ + i) == 2)
                std::cerr << "WARNING: Please verify the syntax of key " << pch
                          << ".\n";
    }
    else if (registry_.type_ == RegistryType::UNSIGNEDLONG) {
        for (std::size_t i = 0; i < registry_.size_; ++i)
            if (readUnsLong(registry_.data_unsigned_long_ + i) == 2)
                std::cerr << "WARNING: Please verify the syntax of key " << pch
                          << ".\n";
    }
    else if (registry_.type_ == RegistryType::STRING) {
        for (std::size_t i = 0; i < registry_.size_; ++i)
            if (readString(registry_.data_stdstring_ + i) == 2)
                std::cerr << "WARNING: Please verify the syntax of key " << pch
                          << ".\n";
    }
    else {
        std::cerr << "Warning: not able to read from key " << pch << '\n';
    }
}

void
KeyMatch::make()
{
    line_ = NULL;
    len_ = 0;
    fp_ = std::fopen(in.c_str(), "r");

    if (fp_ == NULL) {
        std::cerr << "Err: Input file has no input.\n";
        return;
    }
    else
        end_of_file = 0;

    while (((read_ = getline(&line_, &len_, fp_)) != -1)
            && (end_of_file == 0)) {
        pch_ = std::strtok(line_, " ");
        while (pch_ != NULL) {
            match(pch_);
            pch_ = std::strtok(NULL, " ");
        }
    }

    free(line_);
    std::fclose(fp_);
}

int
KeyMatch::readDouble(double* output)
{
    return read_double(fp_, &line_, &len_, &read_, pch_, &end_, output);
}

int
KeyMatch::readInt(int* output)
{
    return read_int(fp_, &line_, &len_, &read_, pch_, output);
}

int
KeyMatch::readUns(unsigned* output)
{
    return read_uns(fp_, &line_, &len_, &read_, pch_, output);
}

int
KeyMatch::readUnsLong(unsigned long* output)
{
    return read_unsLong(fp_, &line_, &len_, &read_, pch_, output);
}

int
KeyMatch::readString(std::string* output)
{
    return read_string(fp_, &line_, &len_, &read_, pch_, output);
}

void
KeyMatch::define_in_out(int argc, char** argv)
{
    switch (argc) {
    case 1: {
        std::cout << "Warning: The input and output files were defined by "
                     "default.\nSyntax: -i in_file_name.extension -o "
                     "out_file_name.extension \n";
        in = defaultInput;
        out = defaultOutput;
    } break;
    case 2: {
        std::cout << "Warning: The input and output files were defined by "
                     "default.\nSyntax: -i in_file_name.extension -o "
                     "out_file_name.extension \n";
        in = defaultInput;
        out = defaultOutput;
    } break;
    case 3: {
        if (std::strcmp(argv[1], "-i") == 0) {
            std::cout << "Warning: The output file was defined by default.\n";
            in = std::string(argv[2]);
            out = defaultOutput;
        }
        else if (std::strcmp(argv[1], "-o") == 0) {
            std::cout << "Warning: The input file was defined by default.\n";
            out = std::string(argv[2]);
            in = defaultInput;
        }
        else {
            std::cout << "Warning: The input and output files were defined by "
                         "default.\nSyntax: -i in_file_name.extension -o "
                         "out_file_name.extension \n";
            in = defaultInput;
            out = defaultOutput;
        }
    } break;
    case 4: {
        if (std::strcmp(argv[1], "-i") == 0) {
            std::cout << "Warning: The output file was defined by default.\n";
            in = std::string(argv[2]);
            out = defaultOutput;
        }
        else if (std::strcmp(argv[1], "-o") == 0) {
            std::cout << "Warning: The input file was defined by default.\n";
            out = std::string(argv[2]);
            in = defaultInput;
        }
        else {
            std::cout << "Warning: The input and output files were defined by "
                         "default.\nSyntax: -i in_file_name.extension -o "
                         "out_file_name.extension \n";
            in = defaultInput;
            out = defaultOutput;
        }
    } break;
    case 5: {
        if ((std::strcmp(argv[1], "-i") == 0)
                && (std::strcmp(argv[3], "-o") == 0)) {
            in = std::string(argv[2]);
            out = std::string(argv[4]);
        }
        else if ((std::strcmp(argv[3], "-i") == 0)
                && (std::strcmp(argv[1], "-o") == 0)) {
            in = std::string(argv[4]);
            out = std::string(argv[2]);
        }
        else {
            std::cout << "Warning: The input and output files were defined by "
                         "default.\nSyntax: -i in_file_name.extension -o "
                         "out_file_name.extension \n";
            in = defaultInput;
            out = defaultOutput;
        }
    } break;
    default: {
        std::cout << "Warning: Too many arguments.\nThe input and output files "
                     "were defined by default.\nSyntax: -i "
                     "in_file_name.extension -o out_file_name.extension \n";
        in = defaultInput;
        out = defaultOutput;
    }
    }
}

int
KeyMatch::read_double(FILE* fp, char** line, size_t* len, ssize_t* read,
        char* pch, char** end, double* output)
{
    int i;

    /*
     * Once one has already identified the correct key, one can read what's
     * after it in the file. The std::strtok function searches for the next
     * string which is preceded by a space character
     */
    pch = std::strtok(NULL, " ");

    /*
     * If pch is different than NULL, then it is assumed that the user has
     * entered a double after the key in the same line. The code inside
     * the next if reads this double
     */
    if (pch != NULL) {
        /*
         * If pch has a double, then it is copied to output. In this case,
         * end points to a different address than pch
         */
        *output = std::strtod(pch, end);

        /*
         * If end is equal to pch, when then know that nothing was read.
         * In this case, 2 is returned as a sign that it wasn't possible
         * to read the double. Otherwise, the double was read, and 0 is
         * returned as a sign of success
         */
        if (*(end) == pch)
            return 2;
        else
            return 0;
    }
    else {
        /*
         * If pch is equal to NULL, then it is assumed that the user has
         * entered the double in the next line. The following piece of
         * code reads this double if it exists
         */
        i = std::fscanf(fp, "\n%lf", output);

        /*
         * if i is different than end of file, then the double was read
         */
        if (i != EOF)
            return 0;
        else
            return 2;

        /** CORRECT THIS COMMENT
         * Uma vez que o double foi lido (ou não), deve-se então procurar
         * pelas próximas keys na mesma linha. Observe que nesse estágio,
         * pch = NULL, então a busca ocorre do início de line, no entanto,
         * esta line possivelmente não é toda a linha atual (caso o double
         * tenha sido lido), mas sim a linha a partir do double lido.
         */
        if ((*(read) = getline(line, len, fp)) != -1)
            pch = std::strtok(*(line), " ");
        else
            return 1;
    }

    return 0;
}

int
KeyMatch::read_uns(FILE* fp, char** line, size_t* len, ssize_t* read, char* pch,
        unsigned* output)
{
    int output_internal;
    auto returned_value = read_int(fp, line, len, read, pch, &output_internal);
    if (output_internal >= 0) {
        *output = static_cast<unsigned>(output_internal);
        return returned_value;
    }
    return 1;
}

int
KeyMatch::read_unsLong(FILE* fp, char** line, size_t* len, ssize_t* read,
        char* pch, unsigned long* output)
{
    int output_internal;
    auto returned_value = read_int(fp, line, len, read, pch, &output_internal);
    if (output_internal >= 0) {
        *output = static_cast<unsigned long>(output_internal);
        return returned_value;
    }
    return 1;
}

int
KeyMatch::read_int(FILE* fp, char** line, size_t* len, ssize_t* read, char* pch,
        int* output)
{
    int i;

    /*
     * Once one has already identified the correct key, one can read what's
     * after it in the file. The std::strtok function searches for the next
     * string which is preceded by a space character
     */
    pch = std::strtok(NULL, " ");

    /*
     * If pch is different than NULL, then it is assumed that the user has
     * entered a double after the key in the same line. The code inside
     * the next if reads this double
     */
    if (pch != NULL) {
        /*
         * If pch has an int, then it is copied to output. In this case,
         * end points to a different address than pch
         */
        i = std::sscanf(pch, "%i", output);

        /*
         * if i is different than end of file, then the int was read
         */
        if (i == EOF)
            return 2;
        else
            return 0;
    }
    else {
        /*
         * If pch is equal to NULL, then it is assumed that the user has
         * entered the int in the next line. The following piece of
         * code reads this double if it exists
         */
        i = std::fscanf(fp, "\n%i", output);

        /*
         * if i is different than end of file, then the int was read
         */
        if (i != EOF)
            return 0;
        else
            return 2;

        /** CORRECT THIS COMMENT
         * Uma vez que o double foi lido (ou não), deve-se então procurar
         * pelas próximas keys na mesma linha. Observe que nesse estágio,
         * pch = NULL, então a busca ocorre do início de line, no entanto,
         * esta line possivelmente não é toda a linha atual (caso o double
         * tenha sido lido), mas sim a linha a partir do double lido.
         */
        if ((*(read) = getline(line, len, fp)) != -1)
            pch = std::strtok(*(line), " ");
        else
            return 1;
    }

    return 0;
}

int
KeyMatch::read_string(FILE* fp, char** line, size_t* len, ssize_t* read,
        char* pch, std::string* output)
{
    /*
     * Once one has already identified the correct key, one can read what's
     * after it in the file. The std::strtok function searches for the next
     * string which is preceded by a space character
     */
    pch = std::strtok(NULL, " ");

    /*
     * If pch is different than NULL, then it is assumed that pch has the
     * desired string. This string is then copied to output
     */
    if (pch != NULL) {
        std::string pch_str(pch);
        std::stringstream ss(pch_str);
        ss >> (*output);
        return 0;
    }
    else {
        /*
         * If pch is equal to NULL, then it was typed on the next line
         */
        if ((*(read) = getline(line, len, fp)) != -1) {
            pch = std::strtok(*(line), " ");

            /*
             * if there is a string on the next line, it is then stored.
             * Otherwise, something has gone wrong, and 2 is reported as
             * a sign of unsuccess
             */
            if (pch != NULL) {
                std::string pch_str(pch);
                std::stringstream ss(pch_str);
                ss >> (*output);
                return 0;
            }
            else
                return 2;

            /** CORRECT THIS COMMENT
             * Uma vez que a string foi lida (ou não), deve-se procurar
             * pelas próximas keys.
             */
            if ((*(read) = getline(line, len, fp)) != -1)
                pch = std::strtok(*(line), " ");
            else
                return 1;
        }
        else
            return 3;
    }

    return 0;
}
