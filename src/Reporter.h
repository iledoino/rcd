#ifndef SRC_INTERFACE_REPORTER_H
#define SRC_INTERFACE_REPORTER_H


#include <list>
#include <vector>

class Reporter
{
public:
    Reporter() = default;
    virtual ~Reporter() noexcept = default;

    virtual void start() = 0;
    virtual bool timetomake() = 0;
    virtual void make(const std::vector<std::vector<double>>& solution,
            const std::vector<double>& spcMesh, double time,
            bool makeanyway = false)
            = 0;
    virtual void finish() = 0;

    virtual void spaceRange(/* automatic */) {}
    virtual void spaceRange(double /* xl */, double /* xr */) {}
};

class Reporters : public Reporter
{
public:
    Reporters() : reportersList_(), timetomakeList_(){};
    virtual ~Reporters() = default;

    void start() override
    {
        for (auto& rpt : reportersList_)
            rpt->start();
    }
    bool timetomake() override
    {
        bool timetomake_p = false;
        auto iter = timetomakeList_.begin();
        for (auto& rpt : reportersList_) {
            (*iter) = rpt->timetomake();
            timetomake_p = timetomake_p || (*iter);
            iter++;
        }
        return timetomake_p;
    }
    void make(const std::vector<std::vector<double>>& solution,
            const std::vector<double>& spcMeshspcMesh, double time,
            bool makeanyway = false) override
    {
        auto iter = timetomakeList_.begin();
        for (auto& rpt : reportersList_) {
            if ((*iter) || makeanyway)
                rpt->make(solution, spcMeshspcMesh, time);
            iter++;
        }
    }
    void finish() override
    {
        for (auto& rpt : reportersList_)
            rpt->finish();
    }

    void spaceRange(/* automatic */) override
    {
        for (auto& rpt : reportersList_)
            rpt->spaceRange();
    }
    void spaceRange(double xl, double xr) override
    {
        for (auto& rpt : reportersList_)
            rpt->spaceRange(xl, xr);
    }

    void addReporter(Reporter& rpt)
    {
        reportersList_.push_back(&rpt);
        timetomakeList_.push_back(true);
    }

private:
    std::list<Reporter*> reportersList_;
    std::list<int> timetomakeList_;
};

#endif /* SRC_INTERFACE_REPORTER_H */
