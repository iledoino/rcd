#include "PDEMember.h"
#include <fstream>
#include <iostream>

/*
 * Destructor is default for base PDEMember
 */
PDEMember::~PDEMember() = default;

/*
 * The following classes were created to implement different discretizations
 * for each of the members available on class PhysicsRCD. Here are the most
 * commun. If the needs, it can create its own, by inheriting from base
 * PDEMember.
 */

void
Accumulation::AddMemberToRHS(PhysicsDataHolder& PDH, double* /* RHS_iMinus1 */,
        double* RHS_i, double* /* RHS_iPlus1 */, double* /* dV_i */,
        double* /* dV_iPlus1 */)
{
    for (std::size_t i = PDH.zeroBcksDim, j = 0; i != PDH.sqrBcksDim; ++i, ++j)
        RHS_i[i] += lambda_ * PDH.G[j];
}

void
Accumulation::AddMemberToLHS(PhysicsDataHolder& PDH, double* /* LHS_iMinus1 */,
        double* LHS_i, double* /* LHS_iPlus1 */, double* /* dV_i */,
        double* /* dV_iPlus1 */)
{
    for (std::size_t i = PDH.zeroBcksDim, j = 0; i != PDH.sqrBcksDim; ++i, ++j)
        LHS_i[i] += lambda_ * PDH.G[j];
}

void
Accumulation::AddMemberToJG(PhysicsDataHolder& PDH,
        double* /* JGDiag_iMinus1 */, double* JGDiag_i,
        double* /* JGDiag_iPlus1 */, double* /* JGUpper_iMinus1 */,
        double* /* JGUpper_i */, double* /* JGLower_i */,
        double* /* JGLower_iPlus1 */, double* /* dV_i */,
        double* /* dV_iPlus1 */)
{
    for (std::size_t i = PDH.zeroBcksSize, j = 0; i != PDH.sqrBcksSize;
            ++i, ++j)
        JGDiag_i[i] += lambda_ * PDH.DG[j];
}

void
Flux_UpWind::AddMemberToRHS(PhysicsDataHolder& PDH, double* /* RHS_iMinus1 */,
        double* RHS_i, double* RHS_iPlus1, double* /* dV_i */,
        double* /* dV_iPlus1 */)
{
    for (std::size_t i = PDH.zeroBcksDim, j = 0; i != PDH.sqrBcksDim;
            ++i, ++j) {
        RHS_i[i] -= ctR_ * PDH.F[j];

        RHS_iPlus1[i] += ctR_ * PDH.F[j];
    }
}

void
Flux_UpWind::AddMemberToLHS(PhysicsDataHolder& PDH, double* /* LHS_iMinus1 */,
        double* LHS_i, double* LHS_iPlus1, double* /* dV_i */,
        double* /* dV_iPlus1 */)
{
    for (std::size_t i = PDH.zeroBcksDim, j = 0; i != PDH.sqrBcksDim;
            ++i, ++j) {
        LHS_i[i] += ctL_ * PDH.F[j];

        LHS_iPlus1[i] -= ctL_ * PDH.F[j];
    }
}

void
Flux_UpWind::AddMemberToJG(PhysicsDataHolder& PDH, double* /* JGDiag_iMinus1 */,
        double* JGDiag_i, double* /* JGDiag_iPlus1 */,
        double* /* JGUpper_iMinus1 */, double* /* JGUpper_i */,
        double* /* JGLower_i */, double* JGLower_iPlus1, double* /* dV_i */,
        double* /* dV_iPlus1 */)
{
    for (std::size_t i = PDH.zeroBcksSize, j = 0; i != PDH.sqrBcksSize;
            ++i, ++j) {
        JGDiag_i[i] += ctL_ * PDH.DF[j];

        JGLower_iPlus1[j] -= ctL_ * PDH.DF[j];
    }
}

void
Flux_CentralDifference::AddMemberToRHS(PhysicsDataHolder& PDH,
        double* RHS_iMinus1, double* /* RHS_i */, double* RHS_iPlus1,
        double* /* dV_i */, double* /* dV_iPlus1 */)
{
    for (std::size_t i = PDH.zeroBcksDim, j = 0; i != PDH.sqrBcksDim;
            ++i, ++j) {
        RHS_iMinus1[i] -= ctR_ * PDH.F[j];

        RHS_iPlus1[i] += ctR_ * PDH.F[j];
    }
}

void
Flux_CentralDifference::AddMemberToLHS(PhysicsDataHolder& PDH,
        double* LHS_iMinus1, double* /* LHS_i */, double* LHS_iPlus1,
        double* /* dV_i */, double* /* dV_iPlus1 */)
{
    for (std::size_t i = PDH.zeroBcksDim, j = 0; i != PDH.sqrBcksDim;
            ++i, ++j) {
        LHS_iMinus1[i] += ctL_ * PDH.F[j];

        LHS_iPlus1[i] -= ctL_ * PDH.F[j];
    }
}

void
Flux_CentralDifference::AddMemberToJG(PhysicsDataHolder& PDH,
        double* /* JGDiag_iMinus1 */, double* /* JGDiag_i */,
        double* /* JGDiag_iPlus1 */, double* JGUpper_iMinus1,
        double* /* JGUpper_i */, double* /* JGLower_i */,
        double* JGLower_iPlus1, double* /* dV_i */, double* /* dV_iPlus1 */)
{
    for (std::size_t j = 0; j != PDH.rectBcksSize; ++j) {
        JGUpper_iMinus1[j] += ctL_ * PDH.DF[j];

        JGLower_iPlus1[j] -= ctL_ * PDH.DF[j];
    }
}

Diffusion_Constant::~Diffusion_Constant() = default;

void
Diffusion_Constant::AddMemberToRHS(PhysicsDataHolder& PDH, double* RHS_iMinus1,
        double* RHS_i, double* RHS_iPlus1, double* dV_i, double* dV_iPlus1)
{
    PDH.MatrixProduct(dV_i, dV_iPlus1, ctR_);
    for (std::size_t i = PDH.zeroBcksDim, j = 0; i != PDH.sqrBcksDim;
            ++i, ++j) {
        RHS_iMinus1[i] += PDH.BtdV[j];

        RHS_i[i] += (PDH.BtdVNext[j] - PDH.BtdV[j]);

        RHS_iPlus1[i] -= PDH.BtdVNext[j];
    }
}

void
Diffusion_Constant::AddMemberToLHS(PhysicsDataHolder& PDH, double* LHS_iMinus1,
        double* LHS_i, double* LHS_iPlus1, double* dV_i, double* dV_iPlus1)
{
    PDH.MatrixProduct(dV_i, dV_iPlus1, ctL_);

    for (std::size_t i = PDH.zeroBcksDim, j = 0; i != PDH.sqrBcksDim;
            ++i, ++j) {
        LHS_iMinus1[i] -= PDH.BtdV[j];

        LHS_i[i] -= (PDH.BtdVNext[j] - PDH.BtdV[j]);

        LHS_iPlus1[i] += PDH.BtdVNext[j];
    }
}

void
Diffusion_Constant::AddMemberToJG(PhysicsDataHolder& PDH,
        double* JGDiag_iMinus1, double* JGDiag_i, double* JGDiag_iPlus1,
        double* JGUpper_iMinus1, double* JGUpper_i, double* JGLower_i,
        double* JGLower_iPlus1, double* /* dV_i */, double* /* dV_iPlus1 */)
{
    double tmp;

    for (std::size_t i = PDH.zeroBcksSize, j = 0; i != PDH.sqrBcksSize;
            ++i, ++j) {
        tmp = ctL_ * PDH.B[j];

        JGDiag_iMinus1[i] += tmp;
        JGDiag_i[i] += (tmp + tmp);
        JGDiag_iPlus1[i] += tmp;
    }

    for (std::size_t j = 0; j != PDH.rectBcksSize; ++j) {
        tmp = -ctL_ * PDH.B[j];

        JGUpper_iMinus1[j] += tmp;
        JGUpper_i[j] += tmp;

        JGLower_i[j] += tmp;
        JGLower_iPlus1[j] += tmp;
    }
}

void
Diffusion::AddMemberToJG(PhysicsDataHolder& PDH, double* JGDiag_iMinus1,
        double* JGDiag_i, double* JGDiag_iPlus1, double* JGUpper_iMinus1,
        double* JGUpper_i, double* JGLower_i, double* JGLower_iPlus1,
        double* dV_i, double* dV_iPlus1)
{
    Diffusion_Constant::AddMemberToJG(PDH, JGDiag_iMinus1, JGDiag_i,
            JGDiag_iPlus1, JGUpper_iMinus1, JGUpper_i, JGLower_i,
            JGLower_iPlus1, dV_i, dV_iPlus1);

    double tmp1, tmp2;

    PDH.TensorProduct(dV_i, dV_iPlus1);

    for (std::size_t i = PDH.zeroBcksSize, j = 0; i != PDH.sqrBcksSize;
            ++i, ++j) {
        tmp1 = ctL_ * PDH.DBtdV[j];
        tmp2 = ctL_ * PDH.DBtdVNext[j];

        JGDiag_i[i] -= (tmp2 - tmp1);

        JGUpper_iMinus1[j] -= tmp1;

        JGLower_iPlus1[j] += tmp2;
    }
}

void
Reaction::AddMemberToRHS(PhysicsDataHolder& PDH, double* /* RHS_iMinus1 */,
        double* RHS_i, double* /* RHS_iPlus1 */, double* /* dV_i */,
        double* /* dV_iPlus1 */)
{
    for (std::size_t i = PDH.zeroBcksDim, j = 0; i != PDH.sqrBcksDim; ++i, ++j)
        RHS_i[i] += ctR_ * PDH.R[j];
}

void
Reaction::AddMemberToLHS(PhysicsDataHolder& PDH, double* /* LHS_iMinus1 */,
        double* LHS_i, double* /* LHS_iPlus1 */, double* /* dV_i */,
        double* /* dV_iPlus1 */)
{
    for (std::size_t i = PDH.zeroBcksDim, j = 0; i != PDH.sqrBcksDim; ++i, ++j)
        LHS_i[i] -= ctL_ * PDH.R[j];
}

void
Reaction::AddMemberToJG(PhysicsDataHolder& PDH, double* /* JGDiag_iMinus1 */,
        double* JGDiag_i, double* /* JGDiag_iPlus1 */,
        double* /* JGUpper_iMinus1 */, double* /* JGUpper_i */,
        double* /* JGLower_i */, double* /* JGLower_iPlus1 */,
        double* /* dV_i */, double* /* dV_iPlus1 */)
{
    for (std::size_t i = PDH.zeroBcksSize, j = 0; i != PDH.sqrBcksSize;
            ++i, ++j)
        JGDiag_i[i] -= ctL_ * PDH.DR[j];
}

void
Constraint::AddMemberToRHS(PhysicsDataHolder& PDH, double* /* RHS_iMinus1 */,
        double* RHS_i, double* /* RHS_iPlus1 */, double* /* dV_i */,
        double* /* dV_iPlus1 */)
{
    for (std::size_t j = 0; j != PDH.zeroBcksDim; ++j)
        RHS_i[j] -= ctR_ * PDH.H[j];
}

void
Constraint::AddMemberToLHS(PhysicsDataHolder& PDH, double* /* LHS_iMinus1 */,
        double* LHS_i, double* /* LHS_iPlus1 */, double* /* dV_i */,
        double* /* dV_iPlus1 */)
{
    for (std::size_t j = 0; j != PDH.zeroBcksDim; ++j)
        LHS_i[j] += ctL_ * PDH.H[j];
}

void
Constraint::AddMemberToJG(PhysicsDataHolder& PDH, double* /* JGDiag_iMinus1 */,
        double* JGDiag_i, double* /* JGDiag_iPlus1 */,
        double* /* JGUpper_iMinus1 */, double* /* JGUpper_i */,
        double* /* JGLower_i */, double* /* JGLower_iPlus1 */,
        double* /* dV_i */, double* /* dV_iPlus1 */)
{
    for (std::size_t j = 0; j != PDH.zeroBcksSize; ++j)
        JGDiag_i[j] += ctL_ * PDH.DH[j];
}

void
RelaxedConstraint::AddMemberToRHS(PhysicsDataHolder& PDH,
        double* /* RHS_iMinus1 */, double* RHS_i, double* /* RHS_iPlus1 */,
        double* /* dV_i */, double* /* dV_iPlus1 */)
{
    for (std::size_t j = 0; j != PDH.zeroBcksDim; ++j)
        RHS_i[j] += lambda_ * PDH.H[j] - ctR_ * PDH.H[j];
}

void
RelaxedConstraint::AddMemberToLHS(PhysicsDataHolder& PDH,
        double* /* LHS_iMinus1 */, double* LHS_i, double* /* LHS_iPlus1 */,
        double* /* dV_i */, double* /* dV_iPlus1 */)
{
    for (std::size_t j = 0; j != PDH.zeroBcksDim; ++j)
        LHS_i[j] += lambda_ * PDH.H[j] + ctL_ * PDH.H[j];
}

void
RelaxedConstraint::AddMemberToJG(PhysicsDataHolder& PDH,
        double* /* JGDiag_iMinus1 */, double* JGDiag_i,
        double* /* JGDiag_iPlus1 */, double* /* JGUpper_iMinus1 */,
        double* /* JGUpper_i */, double* /* JGLower_i */,
        double* /* JGLower_iPlus1 */, double* /* dV_i */,
        double* /* dV_iPlus1 */)
{
    for (std::size_t j = 0; j != PDH.zeroBcksSize; ++j)
        JGDiag_i[j] += lambda_ * PDH.DH[j] + ctL_ * PDH.DH[j];
}
