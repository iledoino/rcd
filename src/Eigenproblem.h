#ifndef SRC_EIGEN_EIGENPROBLEM_H
#define SRC_EIGEN_EIGENPROBLEM_H


#include <algorithm>
#include <cmath>
#include <iostream>
#include <vector>

#define EIGENPROBLEM_SUCCESS 1
#define EIGENPROBLEM_BETA_NEAR_ZERO -123

// Generalized eigenproblem, A*v = lambda*B*v
extern "C" void dggev_(const char*, const char*, // JOBVL, JOBVR
        int*,                                    // N
        double*, int*,                           // A, LDA
        double*, int*,                           // B, LDB
        double*,                                 // ALPHAR
        double*,                                 // ALPHAI
        double*,                                 // BETA
        double*, int*,                           // VL, LDVL,
        double*, int*,                           // VR, LDVR,
        double*, int*,                           // WORK, LWORK
        int*                                     // INFO
);

class Eigenproblem
{
public:
    Eigenproblem();
    virtual ~Eigenproblem();

    int solve(const std::vector<double>& A, const std::vector<double>& B,
            std::vector<double>& vl, std::vector<double>& vr,
            std::vector<double>& ev_r, std::vector<double>& ev_i,
            std::vector<bool>& isReal);
};

#endif /* SRC_EIGEN_EIGENPROBLEM_H */
