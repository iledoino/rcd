#include "MultidReporter.h"

#ifdef ENABLE_MULTID

#include <GUI.h>
#include <Polyline.h>
#include <cmath>
#include <limits>
#include <plotutil.h>

MultidReporter::MultidReporter(int* /* pargc */, char** /* argv */,
        std::string spaceName_p, std::size_t nOfVariables_p)
    : space_(spaceName_p, static_cast<int>(nOfVariables_p + 1))
    , sourceScene_(space_)
{
    // TODO (Brad):  removed this call because ELI calls it
    // multid::GUI::init(pargc, argv);
}

void
MultidReporter::start()
{
    PlotReporter::start();

    auto nGroups = reportGroups_.size();
    if (nGroups == 0)
        return;

    auto xIndex = nGroups;
    spaces_.resize(nGroups);
    proj2dMaps_.resize(nGroups);
    plot2ds_.resize(nGroups);
    for (std::size_t i = 0; i < nGroups; ++i) {
        spaces_[i] = std::make_unique<multid::Space>(spaceNames_[i], 2);
        proj2dMaps_[i] = std::make_unique<multid::Proj2dMap>(
                sourceScene_.space(), *spaces_[i], xIndex, i);
        plot2ds_[i] = std::make_unique<multid::Plot2d>(sourceScene_,
                *proj2dMaps_[i], xAxisName_, yAxesNames_[i], true,
                multid::GUI::default_max_n_chars, multid::Frame::root(),
                multid::GUI::default_x + i * (multid::GUI::default_width + 10),
                multid::GUI::default_y, multid::GUI::default_width);
        plot2ds_[i]->show();
    }
}

void
MultidReporter::make(const std::vector<std::vector<double>>& solution,
        const std::vector<double>& spcMesh, double /* time */,
        bool /* makeanyway */)
{
    auto nGroups = reportGroups_.size();
    if (nGroups == 0)
        return;
    auto xIndex = nGroups;
    auto nComps = solution.size();

    auto maxGroupComp = reportGroups_[0].size();
    for (std::size_t i = 1; i < nGroups; ++i)
        if (reportGroups_[i].size() > maxGroupComp)
            maxGroupComp = reportGroups_[i].size();

    auto meshSize = spcMesh.size();
    for (std::size_t i = 0; i < nComps; ++i) {
        if (meshSize > solution[i].size())
            meshSize = solution[i].size();
    }

    std::vector<double> max_value(nGroups, -std::numeric_limits<double>::max());
    std::vector<double> min_value(nGroups, std::numeric_limits<double>::max());
    multid::Scene scene(sourceScene_.space());
    multid::Polyline polyline(sourceScene_.space(), meshSize);
    for (std::size_t j = 0; j < meshSize; ++j)
        polyline[j][xIndex] = spcMesh[j];
    for (std::size_t mgc = 0; mgc < maxGroupComp; mgc++) {
        for (std::size_t k = 0; k < nGroups; ++k)
            for (std::size_t i = 0; i < nComps; ++i)
                if (reportGroups_[k].size() > mgc && i == reportGroups_[k][mgc])
                    for (std::size_t j = 0; j < meshSize; ++j) {
                        auto value = polyline[j][k] = solution[i][j];
                        if (max_value[k] < value)
                            max_value[k] = value;
                        if (min_value[k] > value)
                            min_value[k] = value;
                    }
        scene.add(polyline);
    }
    for (std::size_t i = 0; i < nGroups; ++i) {
        if (not plot2ds_[i])
            continue;
        multid::Bounds2d bounds2d;
        bounds2d.xmin = (spaceRangeAutomatic_ ? spcMesh.front() : xl_);
        bounds2d.ymin = min_value[i] - .15 * (max_value[i] - min_value[i]);
        bounds2d.xmax = (spaceRangeAutomatic_ ? spcMesh.back() : xr_);
        bounds2d.ymax = max_value[i] + .15 * (max_value[i] - min_value[i]);

        // round the max value upward and the min value downward
        if (spaceRangeAutomatic_ && bounds2d.xmin < bounds2d.xmax)
            multid::nice_range(bounds2d.xmin, bounds2d.xmax);
        if (bounds2d.ymin < bounds2d.ymax)
            multid::nice_range(bounds2d.ymin, bounds2d.ymax);

        plot2ds_[i]->set_range(bounds2d);
    }
    sourceScene_.clear();
    sourceScene_.add(scene);
    sourceScene_.render();
}

#endif /* ENABLE_MULTID */
