#include "physicsRCD.h"

void
PhysicsDataHolder::allocate(
        const std::size_t& PDEDimension, const std::size_t& ConstraintDimension)
{
    if (allocated)
        std::cerr << "Sorry, your data holder has already been allocated with "
                     "its memory. Please create another instance."
                  << std::endl;
    else {
        sqrBcksDim = PDEDimension + ConstraintDimension;
        rectBcksDim = PDEDimension;
        zeroBcksDim = ConstraintDimension;

        sqrBcksSize = sqrBcksDim * sqrBcksDim;
        rectBcksSize = rectBcksDim * sqrBcksDim;
        zeroBcksSize = zeroBcksDim * sqrBcksDim;

        G = new double[rectBcksDim];
        F = new double[rectBcksDim];
        R = new double[rectBcksDim];
        B = new double[rectBcksDim * sqrBcksDim];
        H = new double[(sqrBcksDim - rectBcksDim)];

        DG = new double[rectBcksDim * sqrBcksDim];
        DF = new double[rectBcksDim * sqrBcksDim];
        DR = new double[rectBcksDim * sqrBcksDim];
        DB = new double[rectBcksDim * sqrBcksDim * sqrBcksDim];
        DH = new double[(sqrBcksDim - rectBcksDim) * sqrBcksDim];

        BtdV = new double[sqrBcksDim];
        BtdVNext = new double[sqrBcksDim];
        DBtdV = new double[sqrBcksDim * rectBcksDim];
        DBtdVNext = new double[sqrBcksDim * rectBcksDim];

        for (std::size_t i = 0; i < rectBcksDim; ++i)
            G[i] = F[i] = R[i] = 0.0;
        for (std::size_t i = 0; i < rectBcksDim * sqrBcksDim; ++i)
            B[i] = DG[i] = DF[i] = DR[i] = 0.0;
        for (std::size_t i = 0; i < rectBcksDim * sqrBcksDim * sqrBcksDim; ++i)
            DB[i] = 0.0;
        for (std::size_t i = 0; i < (sqrBcksDim - rectBcksDim); ++i)
            H[i] = 0.0;
        for (std::size_t i = 0; i < (sqrBcksDim - rectBcksDim) * sqrBcksDim;
                ++i)
            DH[i] = 0.0;

        k = q = G1n1 = G1n = G2n1 = G2n = F1n1 = F2n1 = 0.0;
    }
}

void
PhysicsDataHolder::MatrixProduct(double* dV, double* dVNext, const double& ct)
{
    double tmp;

    for (std::size_t iB = 0, iBtdV = 0; iB != rectBcksSize;
            iB += sqrBcksDim, ++iBtdV) {
        tmp = ct * B[iB];
        BtdV[iBtdV] = tmp * dV[0];
        BtdVNext[iBtdV] = tmp * dVNext[0];

        for (std::size_t i = 1; i != sqrBcksDim; i++) {
            tmp = ct * B[iB + i];
            BtdV[iBtdV] += tmp * dV[i];
            BtdVNext[iBtdV] += tmp * dVNext[i];
        }
    }
}

void
PhysicsDataHolder::TensorProduct(double* dV, double* dVNext)
{
    double tmp1, tmp2;
    std::size_t delimiter;

    /*
     * index i goes through each line of gradients in tensor, and
     * each line of matrix
     */
    for (std::size_t iMatrix = 0, iTensor = 0; iMatrix != rectBcksSize;
            iMatrix += sqrBcksDim, iTensor = iMatrix * sqrBcksDim) {
        delimiter = (iMatrix + sqrBcksDim);

        tmp1 = dV[0];
        tmp2 = dVNext[0];
        /*
         * index l goes through each column of gradient 0
         * OBS: This first for just set matrix partially
         */
        for (std::size_t lMatrix = iMatrix, lTensor = iTensor;
                lMatrix != delimiter; lMatrix++, lTensor++) {
            DBtdV[lMatrix] = DB[lTensor] * tmp1;
            DBtdVNext[lMatrix] = DB[lTensor] * tmp2;
        }

        /*
         * index j goes through each gradient of line i, and
         * each line of vector
         */
        for (std::size_t j = 1, jTensor = iTensor + sqrBcksDim; j != sqrBcksDim;
                j++, jTensor += sqrBcksDim) {
            tmp1 = dV[j];
            tmp2 = dVNext[j];
            /*
             * index l goes through each column of gradient j
             * OBS: This for adds the components missing from last for
             */
            for (std::size_t lMatrix = iMatrix, lTensor = jTensor;
                    lMatrix != delimiter; lMatrix++, lTensor++) {
                DBtdV[lMatrix] += DB[lTensor] * tmp1;
                DBtdVNext[lMatrix] += DB[lTensor] * tmp2;
            }
        }
    }
}

PhysicsDataHolder::~PhysicsDataHolder()
{
    if (allocated) {
        delete[] G;
        delete[] F;
        delete[] R;
        delete[] B;
        delete[] H;

        delete[] DG;
        delete[] DF;
        delete[] DR;
        delete[] DB;
        delete[] DH;

        delete[] BtdV;
        delete[] BtdVNext;
        delete[] DBtdV;
        delete[] DBtdVNext;
    }
}
