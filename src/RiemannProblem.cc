#include "RiemannProblem.h"
#include "bdry.h"
#include <cmath>
#include <limits>

#define RP_TOL 1.0e-6

double
linear_interp(const double& fL, const double& fR, const double& currX,
        const double& xL = 0., const double& xR = 1.)
{
    return (fR - fL) * (currX - xL) / (xR - xL) + fL;
}

RiemannProblemConfig::RiemannProblemConfig()
    : automaticSetUp_(true)
    , leftState_({ 0.33, 0.5 })
    , rightState_({ 0.01, 0.7 })
    , flowDirection_()
    , leftSpcDelim_()
    , rightSpcDelim_()
    , jumSpcPt_()
    , spcMeshStpSize_()
    , nSpcMeshPts_()
    , useNSMP_()
    , infTimeDelim_()
    , timeMeshStpSize_()
    , cflCondPercent_()
    , nTimeMeshPts_()
    , useNTMP_()
    , speedPercentage_(0.2)
    , timeStepConvCheck_(5.0)
    , parallel_()
    , convergencePlot_()
{
}

RiemannProblem::RiemannProblem(PhysicsRCD& phys_p, Reporter& rptr_p,
        std::size_t PDEDim_p, std::size_t CSTDim_p, bool hasPress_p,
        bool hasVel_p)
    : phys_(phys_p)
    , rptr_(rptr_p)
    , PDEDim_(PDEDim_p)
    , CSTDim_(CSTDim_p)
    , SYSDim_(PDEDim_ + CSTDim_)
    , Dim_(SYSDim_ + (hasPress_p ? 1 : 0) + (hasVel_p ? 1 : 0))
    , hasPress_(hasPress_p)
    , hasVel_(hasVel_p)
    , bdryCondsProvided_(false)
    , bdry_internal_(5, nullptr)
    , bdry_(bdry_internal_)
    , dct_()
    , nwt_()
    , rpc_()
    , pdemc_()
    , prevCheckedSolutions_()
    , checkForConvergence_(true)
    , checkForChangesAtBoundary_(true)
{
    rpc_.leftState().resize(Dim_);
    rpc_.rightState().resize(Dim_);
    setUpBdryConditions();
}

RiemannProblem::RiemannProblem(const std::vector<Bdry*>& bdry_p,
        PhysicsRCD& phys_p, Reporter& rptr_p, std::size_t PDEDim_p,
        std::size_t CSTDim_p, bool hasPress_p, bool hasVel_p)
    : phys_(phys_p)
    , rptr_(rptr_p)
    , PDEDim_(PDEDim_p)
    , CSTDim_(CSTDim_p)
    , SYSDim_(PDEDim_ + CSTDim_)
    , Dim_(SYSDim_ + (hasPress_p ? 1 : 0) + (hasVel_p ? 1 : 0))
    , hasPress_(hasPress_p)
    , hasVel_(hasVel_p)
    , bdryCondsProvided_(true)
    , bdry_internal_(5, nullptr)
    , bdry_(bdry_p)
    , dct_()
    , nwt_()
    , rpc_()
    , pdemc_()
    , prevCheckedSolutions_()
    , checkForConvergence_(true)
    , checkForChangesAtBoundary_(true)
{
    rpc_.leftState().resize(Dim_);
    rpc_.rightState().resize(Dim_);
    setUpBdryConditions();
}

RiemannProblem::~RiemannProblem() noexcept
{
    if (not bdryCondsProvided_)
        for (std::size_t i = 0; i < bdry_.size(); ++i)
            if (bdry_internal_[i] != nullptr)
                delete bdry_internal_[i];
}

double
RiemannProblem::shockSpeed(const std::vector<double>& gL,
        const std::vector<double>& gR, const std::vector<double>& fL,
        const std::vector<double>& fR)
{
    double numer = 0., denom = 0.;

    for (std::size_t i = 0; i < PDEDim_; ++i) {
        double delta_g = gR[i] - gL[i];
        numer += (fR[i] - fL[i]) * delta_g;
        denom += delta_g * delta_g;
    }
#ifdef DEBUG_RIEMANN_PROBLEM
    for (std::size_t i = 0; i < PDEDim_; ++i) {
        std::cerr << "\tgR[" << i << "] --> " << gR[i] << ", \tgL[" << i
                  << "] --> " << gL[i] << ", \tfR[" << i << "] --> " << fR[i]
                  << ", \tfL[" << i << "] --> " << fL[i] << std::endl;
    }
    std::cerr << "numer --> " << numer << ", denom --> " << denom << std::endl;
#endif

    return numer / denom;
}

void
RiemannProblem::setUpBdryConditions()
{
    if (not bdryCondsProvided_) {
        for (std::size_t i = 0; i < bdry_internal_.size(); ++i)
            if (bdry_internal_[i] != nullptr) {
                delete bdry_internal_[i];
                bdry_internal_[i] = nullptr;
            }

        std::vector<double> a(SYSDim_, 1.), b(SYSDim_, 0), c(SYSDim_, 0);
        for (std::size_t i = 0; i < std::min(SYSDim_, rpc_.leftState().size());
                i++)
            c[i] = -rpc_.leftState()[i];
        bdry_internal_[0] = new PDE_RobinBC_FO_L(
                PDEDim_, CSTDim_, rpc_.spcMeshStpSize(), a, b, c);
        if (pdemc_.hasFlux() && pdemc_.fluxIsUpwind()
                && not pdemc_.hasDiffusion())
            bdry_internal_[1] = new NoBoundaryCondition();
        else {
            for (std::size_t i = 0;
                    i < std::min(SYSDim_, rpc_.leftState().size()); i++)
                c[i] = -rpc_.rightState()[i];
            bdry_internal_[1] = new PDE_RobinBC_FO_R(
                    PDEDim_, CSTDim_, rpc_.spcMeshStpSize(), a, b, c);
        }

        if (hasPress_) {
            bdry_internal_[2] = new Pressure_RobinBC_FO_L(
                    rpc_.spcMeshStpSize(), 1., 0., rpc_.leftState()[SYSDim_]);
            bdry_internal_[3] = new Pressure_RobinBC_FO_R(
                    rpc_.spcMeshStpSize(), 1., 0., rpc_.rightState()[SYSDim_]);
        }
        else if (hasVel_)
            bdry_internal_[2]
                    = new Velocity_WithoutPressure_L(rpc_.leftState()[SYSDim_]);
    }
}

void
RiemannProblem::getEigenvalues(PhysicsDataHolder& pdh_, Eigenproblem& ep,
        std::vector<double>& state_, double p_p, double u_p,
        std::vector<double>& vl, std::vector<double>& vr,
        std::vector<double>& ev_r, std::vector<double>& ev_i,
        std::vector<bool>& isReal)
{
    std::vector<double> dg(PDEDim_ * PDEDim_), df(PDEDim_ * PDEDim_);

    phys_.jet(pdh_, state_.data(), state_.data(), p_p, u_p, 0., 1., 0);
#ifdef DEBUG_RIEMANN_PROBLEM
    std::cerr << "DF=[";
    for (std::size_t i = 0; i < PDEDim_; ++i) {
        for (std::size_t j = 0; j < PDEDim_; ++j)
            std::cerr << pdh_.DF[i * PDEDim_ + j] << " ";
        std::cerr << std::endl;
    }
    std::cerr << "];" << std::endl;
    std::cerr << "DG=[";
    for (std::size_t i = 0; i < PDEDim_; ++i) {
        for (std::size_t j = 0; j < PDEDim_; ++j)
            std::cerr << pdh_.DG[i * PDEDim_ + j] << " ";
        std::cerr << std::endl;
    }
    std::cerr << "];" << std::endl;
#endif
    for (std::size_t i = 0; i < PDEDim_ * PDEDim_; ++i) {
        dg[i] = pdh_.DG[i];
        df[i] = pdh_.DF[i];
    }
    if (PDEDim_ > 1)
        ep.solve(df, dg, vl, vr, ev_r, ev_i, isReal);
    else
        ev_r[0] = df[0] / dg[0];
}

void
RiemannProblem::setUpSpeedLimits(const RiemannProblemConfig& rpc_p)
{
    PhysicsDataHolder pdh_;
    pdh_.allocate(PDEDim_, CSTDim_);
    phys_.setConstantValues(pdh_);

    double p_p = 0., u_p = 0.;

    Eigenproblem ep;
    std::vector<bool> isReal;
    std::vector<double> vl, vr, ev_r(PDEDim_), ev_i;
    std::vector<double> gL(PDEDim_), gR(PDEDim_), fL(PDEDim_), fR(PDEDim_);
    std::vector<double> state_;

    std::size_t jj = SYSDim_;
    std::size_t jjj = hasPress_ ? SYSDim_ + 1 : SYSDim_;

    if (hasPress_)
        p_p = rpc_p.leftState()[jj];
    if (hasVel_)
        u_p = rpc_p.leftState()[jjj];
    state_ = rpc_p.leftState();
    getEigenvalues(pdh_, ep, state_, p_p, u_p, vl, vr, ev_r, ev_i, isReal);
    for (std::size_t i = 0; i < PDEDim_; ++i) {
        gL[i] = pdh_.G[i];
        fL[i] = pdh_.F[i];
    }
    xi_min = ev_r.size() > 0 ? ev_r[0] : 0.;
    xi_mod_max = ev_r.size() > 0 ? std::abs(ev_r[0]) : 0.;
    for (std::size_t i = 1; i < ev_r.size(); ++i) {
        xi_min = std::min(xi_min, ev_r[i]);
        xi_mod_max = std::max(xi_mod_max, std::abs(ev_r[i]));
    }

#ifdef DEBUG_RIEMANN_PROBLEM
    for (std::size_t i = 0; i < ev_r.size(); ++i)
        std::cerr << "\tev_r[" << i << "] --> " << ev_r[i] << std::endl;
    std::cerr << std::endl;
#endif

    if (hasPress_)
        p_p = rpc_p.rightState()[jj];
    if (hasVel_)
        u_p = rpc_p.rightState()[jjj];
    state_ = rpc_p.rightState();
    getEigenvalues(pdh_, ep, state_, p_p, u_p, vl, vr, ev_r, ev_i, isReal);
    for (std::size_t i = 0; i < PDEDim_; ++i) {
        gR[i] = pdh_.G[i];
        fR[i] = pdh_.F[i];
    }
    xi_max = ev_r.size() > 0 ? ev_r[0] : 0.;
    xi_mod_max = std::max(xi_mod_max, std::abs(ev_r[0]));
    for (std::size_t i = 1; i < ev_r.size(); ++i) {
        xi_max = std::max(xi_max, ev_r[i]);
        xi_mod_max = std::max(xi_mod_max, std::abs(ev_r[i]));
    }

#ifdef DEBUG_RIEMANN_PROBLEM
    for (std::size_t i = 0; i < ev_r.size(); ++i)
        std::cerr << "\tev_r[" << i << "] --> " << ev_r[i] << std::endl;
    std::cerr << std::endl;
#endif

    if (xi_max < xi_min) {
        double aux = xi_max;
        xi_max = xi_min;
        xi_min = aux;
    }

    double shockSpeed_ = shockSpeed(gL, gR, fL, fR);
    double pct = std::abs(rpc_p.speedPercentage());
    xi_min = (1. - pct) * std::min(shockSpeed_, xi_min);
    xi_max = (1. + pct) * std::max(shockSpeed_, xi_max);

    if (std::abs(xi_mod_max) < 1.0e-15) {
        std::cerr << "Warning: Getting maximum eigenvalue close to zero. "
                     "Roundint it to 1."
                  << std::endl;
        xi_mod_max = 1.0;
    }
    else if (std::abs(xi_mod_max) > 1.0e10) {
        std::cerr << "Warning: Getting maximum eigenvalue close to infinity. "
                     "Roundint it to 1."
                  << std::endl;
        xi_mod_max = 1.0;
    }

#ifdef DEBUG_RIEMANN_PROBLEM
    std::cerr << "xi_min --> " << xi_min << " xi_max --> " << xi_max
              << " xi_mod_max --> " << xi_mod_max << " shock speed --> "
              << shockSpeed_ << std::endl;
#endif
}

void
RiemannProblem::updateCFLCondition(double currentTime_p, double finalTime)
{
    PhysicsDataHolder pdh_;
    pdh_.allocate(PDEDim_, CSTDim_);
    phys_.setConstantValues(pdh_);

    double p_p = 0., u_p = 0.;

    Eigenproblem ep;
    std::vector<bool> isReal;
    std::vector<double> vl, vr, ev_r(PDEDim_), ev_i;
    std::vector<double> gL(PDEDim_), gR(PDEDim_), fL(PDEDim_), fR(PDEDim_);

    std::size_t jj = SYSDim_;
    std::size_t jjj = hasPress_ ? SYSDim_ + 1 : SYSDim_;

    std::vector<double> stateAvrg_(SYSDim_, 1.0);
    for (std::size_t j = 0; j < SYSDim_; ++j) {
        stateAvrg_[j] = (rpc_.rightState()[j] + rpc_.leftState()[j]) * 0.5;
        if (std::abs(stateAvrg_[j]) < 1.0e-16)
            stateAvrg_[j] = 1.0;
    }

    if (dct_) {
        double ev_mod_max = -std::numeric_limits<double>::max();
        std::vector<double> statePrev_(
                SYSDim_, std::numeric_limits<double>::max());
        for (std::size_t i = 0, j = 0;
                i < static_cast<std::size_t>(dct_->nSpcMeshPts());
                i++, j += SYSDim_) {
            std::vector<double> state_(SYSDim_, 0.);
            for (std::size_t k = 0; k < SYSDim_; ++k)
                state_[k] = dct_->V[j + k];

            double norm_p = 0.;
            for (std::size_t l = 0; l < SYSDim_; ++l)
                norm_p += sqr_((statePrev_[l] - state_[l]) / stateAvrg_[l]);
            norm_p /= static_cast<double>(SYSDim_);

            if (norm_p > RP_TOL) {
                if (hasPress_)
                    p_p = rpc_.rightState()[jj];
                if (hasVel_)
                    u_p = rpc_.rightState()[jjj];
                getEigenvalues(
                        pdh_, ep, state_, p_p, u_p, vl, vr, ev_r, ev_i, isReal);
                for (std::size_t l = 0; l < ev_r.size(); ++l)
                    ev_mod_max = std::max(ev_mod_max, std::abs(ev_r[l]));
            }

            state_.swap(statePrev_);
        }

        if (std::abs(ev_mod_max) < 1.0e-15) {
            std::cerr << "Warning: Getting maximum eigenvalue close to zero. "
                         "Roundint it to 1."
                      << std::endl;
            ev_mod_max = 1.0;
        }
        else if (std::abs(ev_mod_max) > 1.0e10) {
            std::cerr
                    << "Warning: Getting maximum eigenvalue close to infinity. "
                       "Roundint it to 1."
                    << std::endl;
            ev_mod_max = 1.0;
        }

        // dt < e * dx / u (CFL condition)
#ifdef DEBUG_RIEMANN_PROBLEM
        std::cerr << "Current time: " << currentTime_p << std::endl;
        std::cerr << "Adapting CFL: prev --> " << rpc_.timeMeshStpSize_;
#endif
        if (ev_mod_max > 0) {
            rpc_.timeMeshStpSize_ = rpc_.cflCondPercent()
                    * static_cast<double>(rpc_.spcMeshStpSize()) / ev_mod_max;
            if ((currentTime_p + rpc_.timeMeshStpSize_) > finalTime)
                rpc_.timeMeshStpSize_ = finalTime - currentTime_p;
            dct_->DT(rpc_.timeMeshStpSize_);
        }
#ifdef DEBUG_RIEMANN_PROBLEM
        std::cerr << ", new --> " << rpc_.timeMeshStpSize_ << "\n";
#endif
    }
}

void
RiemannProblem::adaptTimeMeshStpSize(
        std::size_t timeCounter, double currentTime_p, double finalTime)
{
    if (rpc_.automaticSetUp() && rpc_.adaptiveCFL()) {
        if (timeCounter % rpc_.adapCFLFrequency() == 0) {
            updateCFLCondition(currentTime_p, finalTime);
        }
    }
}

RiemannProblemConfig
RiemannProblem::getRPC() const
{
    return rpc_;
}

void
RiemannProblem::setRPC(RiemannProblemConfig rpc_p)
{
    if (rpc_p.automaticSetUp()) {
        setUpSpeedLimits(rpc_p);
        rpc_p.leftSpcDelim() = std::min(xi_min, 0.) * rpc_p.infTimeDelim();
        rpc_p.rightSpcDelim() = std::max(xi_max, 0.) * rpc_p.infTimeDelim();
        if (rpc_p.flowDirection() == FlowDirection::BIDIRECTIONAL) {
            rpc_p.leftSpcDelim() = -std::max(std::abs(rpc_p.leftSpcDelim()),
                    std::abs(rpc_p.rightSpcDelim()));
            rpc_p.rightSpcDelim() = std::abs(rpc_p.leftSpcDelim());
        }
        rpc_p.jumSpcPt() = 0.;
    }
    else
        rpc_p.flowDirection() = FlowDirection::BIDIRECTIONAL;

    if (rpc_p.useNSMP()) {
        rpc_p.spcMeshStpSize() = (rpc_p.rightSpcDelim() - rpc_p.leftSpcDelim())
                / static_cast<double>(rpc_p.nSpcMeshPts() - 1);
    }
    else {
        rpc_p.nSpcMeshPts()
                = static_cast<std::size_t>(
                          (rpc_p.rightSpcDelim() - rpc_p.leftSpcDelim())
                          / rpc_p.spcMeshStpSize())
                + 1;
    }
    bool reallocate_dct(rpc_.nSpcMeshPts() != rpc_p.nSpcMeshPts());

    if (rpc_p.automaticSetUp()) {
        // dt < e * dx / u (CFL condition)
        rpc_p.timeMeshStpSize() = rpc_p.cflCondPercent()
                * static_cast<double>(rpc_p.spcMeshStpSize()) / xi_mod_max;
        rpc_p.nTimeMeshPts() = static_cast<std::size_t>(rpc_p.infTimeDelim()
                                       / rpc_p.timeMeshStpSize())
                + 1;
        rpc_p.useNTMP() = false;
    }
    else {
        if (rpc_p.useNTMP()) {
            rpc_p.timeMeshStpSize() = rpc_p.infTimeDelim()
                    / static_cast<double>(rpc_p.nTimeMeshPts() - 1);
        }
        else {
            rpc_p.nTimeMeshPts() = static_cast<std::size_t>(rpc_p.infTimeDelim()
                                           / rpc_p.timeMeshStpSize())
                    + 1;
        }
    }
#ifdef DEBUG_RIEMANN_PROBLEM
    std::cerr << "cflCondPercent() --> " << rpc_p.cflCondPercent()
              << " useNSMP() --> " << rpc_p.useNSMP()
              << " spcMeshStpSize() --> " << rpc_p.spcMeshStpSize()
              << " nSpcMeshPts() --> " << rpc_p.nSpcMeshPts()
              << " useNTMP() --> " << rpc_p.useNTMP()
              << " timeMeshStpSize() --> " << rpc_p.timeMeshStpSize()
              << " nTimeMeshPts() --> " << rpc_p.nTimeMeshPts()
              << " xi_mod_max --> " << xi_mod_max << std::endl;
#endif

    if (rpc_p.leftState().size() < Dim_)
        rpc_p.leftState().resize(Dim_);
    if (rpc_p.rightState().size() < Dim_)
        rpc_p.rightState().resize(Dim_);
    bool updtInitSol(false);
    if (!reallocate_dct) {
        for (std::size_t i = 0; i < Dim_; ++i) {
            if (rpc_p.leftState()[i] != rpc_.leftState()[i]
                    || rpc_p.rightState()[i] != rpc_.rightState()[i])
                updtInitSol = true;
        }
    }

    if ((reallocate_dct || (rpc_p.parallel() != rpc_.parallel())) && !dct_)
        dct_.release();
    else {
        rpc_ = rpc_p;
        if (updtInitSol) {
            setInitialSolution();
        }
        else if (dct_) {
            for (std::size_t i = 0; i != rpc_.nSpcMeshPts(); i++)
                dct_->x[i] = rpc_.leftSpcDelim()
                        + static_cast<double>(i) * (dct_->spcMeshStpSize());
        }
        setUpBdryConditions();
        return;
    }

    rpc_ = rpc_p;
    setUpBdryConditions();
}

PDEMemberConfig
RiemannProblem::getPDEMC() const
{
    return pdemc_;
}

void
RiemannProblem::setPDEMC(PDEMemberConfig pdemc_p)
{
    pdemc_ = pdemc_p;
    if (dct_)
        dct_->setUpPDEMembers(pdemc_);
    setUpBdryConditions();
}

void
RiemannProblem::getSolution(std::vector<std::vector<double>>& sol_, double& tt_)
{
    tt_ = currentTime;
    if (dct_) {
        sol_.resize(Dim_);
        for (std::size_t i = 0; i < Dim_; ++i)
            sol_[i].resize(dct_->nSpcMeshPts());
        std::size_t jj = SYSDim_;
        std::size_t jjj = hasPress_ ? SYSDim_ + 1 : SYSDim_;
        for (std::size_t i = 0, ii = 0;
                i < static_cast<std::size_t>(dct_->nSpcMeshPts());
                ++i, ii += SYSDim_) {
            for (std::size_t j = 0; j < SYSDim_; ++j)
                sol_[j][i] = dct_->V[j + ii];
            if (hasPress_)
                sol_[jj][i] = dct_->V[jj + ii];
            if (hasVel_)
                sol_[jjj][i] = dct_->V[jjj + ii];
        }
    }
}

void
RiemannProblem::getSpcMesh(std::vector<double>& x_)
{
    if (dct_) {
        x_.resize(dct_->nSpcMeshPts());
        for (std::size_t j = 0;
                j < static_cast<std::size_t>(dct_->nSpcMeshPts()); ++j)
            x_[j] = dct_->x[j];
    }
}

double
RiemannProblem::norm(const std::vector<double>& sol_)
{
    double norm_(0.);
    std::size_t size_ = sol_.size();

    if (size_ < 2)
        return 0.;

    norm_ = 0.5 * sqr_(sol_[0]);
    for (std::size_t i = 1; i < size_ - 1; ++i)
        norm_ += sqr_(sol_[i]);
    norm_ += 0.5 * sqr_(sol_[size_ - 1]);

    if (dct_)
        return norm_ * (dct_->spcMeshStpSize());

    return norm_;
}

double
RiemannProblem::norm(
        const std::vector<double>& sol1, const std::vector<double>& sol2)
{
    double norm_(0.);
    std::size_t size_ = std::min(sol1.size(), sol2.size());

    if (size_ < 2)
        return 0.;

    norm_ = 0.5 * sqr_(sol1[0] - sol2[0]);
    for (std::size_t i = 1; i < size_ - 1; ++i)
        norm_ += sqr_(sol1[i] - sol2[i]);
    norm_ += 0.5 * sqr_(sol1[size_ - 1] - sol2[size_ - 1]);

    if (dct_)
        return norm_ * (dct_->spcMeshStpSize());

    return norm_;
}

double
RiemannProblem::norm(const std::vector<std::vector<double>>& sol_)
{
    double maxNorm(0.), currNorm;
    std::size_t size_ = sol_.size();

    for (std::size_t i = 0; i < size_; ++i) {
        currNorm = norm(sol_[i]);
        if (currNorm > maxNorm)
            maxNorm = currNorm;
    }

    return maxNorm;
}

double
RiemannProblem::norm(const std::vector<std::vector<double>>& sol1,
        const std::vector<std::vector<double>>& sol2)
{
    double maxNorm(0.), currNorm;
    std::size_t size_ = std::min(sol1.size(), sol2.size());

    for (std::size_t i = 0; i < size_; ++i) {
        currNorm = norm(sol1[i], sol2[i]);
        if (currNorm > maxNorm)
            maxNorm = currNorm;
    }

    return maxNorm;
}

void
RiemannProblem::shrinkSolution()
{
    if (dct_) {
        std::size_t shrinkSize_(
                static_cast<std::size_t>(dct_->nSpcMeshPts() / 2));
        std::vector<double> sol(SYSDim_ * shrinkSize_);
        for (std::size_t i = 0; i < shrinkSize_; ++i)
            for (std::size_t j = 0; j < SYSDim_; ++j)
                sol[i * SYSDim_ + j] = dct_->V[2 * i * SYSDim_ + j];

        std::size_t k1, k2, k3;
        if (rpc_.flowDirection() == FlowDirection::BIDIRECTIONAL) {
            k1 = SYSDim_
                    * static_cast<std::size_t>(dct_->nSpcMeshPts() / 4 + 1);
            k2 = SYSDim_
                    * static_cast<std::size_t>(3 * dct_->nSpcMeshPts() / 4 - 1);
            k3 = SYSDim_ * static_cast<std::size_t>(dct_->nSpcMeshPts() / 4);
        }
        else if (rpc_.flowDirection() == FlowDirection::POSITIVE) {
            k1 = 0;
            k2 = SYSDim_
                    * static_cast<std::size_t>(dct_->nSpcMeshPts() / 2 - 1);
            k3 = 0;
        }
        else {
            k1 = SYSDim_
                    * static_cast<std::size_t>(dct_->nSpcMeshPts() / 2 + 1);
            k2 = 0;
            k3 = SYSDim_
                    * static_cast<std::size_t>(dct_->nSpcMeshPts() / 2 - 1);
        }
        for (std::size_t i = 0; i < k1; i += SYSDim_)
            for (std::size_t j = 0; j < SYSDim_; ++j)
                dct_->V[i + j] = rpc_.leftState()[j];
        std::size_t l = SYSDim_ * static_cast<std::size_t>(dct_->nSpcMeshPts());
        for (std::size_t i = k2; i < l; i += SYSDim_)
            for (std::size_t j = 0; j < SYSDim_; ++j)
                dct_->V[i + j] = rpc_.rightState()[j];
        for (std::size_t i = 0; i < sol.size(); ++i, ++k3)
            dct_->V[k3] = sol[i];

        if (hasPress_) {
            for (std::size_t i = 0; i < shrinkSize_; ++i)
                sol[i] = dct_->p[2 * i];

            if (rpc_.flowDirection() == FlowDirection::BIDIRECTIONAL) {
                k1 = static_cast<std::size_t>(dct_->nSpcMeshPts() / 4 + 1);
                k2 = static_cast<std::size_t>(3 * dct_->nSpcMeshPts() / 4 - 1);
                k3 = static_cast<std::size_t>(dct_->nSpcMeshPts() / 4);
            }
            else if (rpc_.flowDirection() == FlowDirection::POSITIVE) {
                k1 = 0;
                k2 = static_cast<std::size_t>(dct_->nSpcMeshPts() / 2 - 1);
                k3 = 0;
            }
            else {
                k1 = SYSDim_
                        * static_cast<std::size_t>(dct_->nSpcMeshPts() / 2 + 1);
                k2 = 0;
                k3 = SYSDim_
                        * static_cast<std::size_t>(dct_->nSpcMeshPts() / 2 - 1);
            }

            for (std::size_t i = 0; i < k1; ++i)
                dct_->p[i] = rpc_.leftState()[SYSDim_];
            l = static_cast<std::size_t>(dct_->nSpcMeshPts());
            for (std::size_t i = k2; i < l; ++i)
                dct_->p[i] = rpc_.rightState()[SYSDim_];
            for (std::size_t i = 0; i < shrinkSize_; ++i, ++k3)
                dct_->p[k3] = sol[i];
        }

        if (hasVel_) {
            for (std::size_t i = 0; i < shrinkSize_; ++i)
                sol[i] = dct_->u[2 * i];

            if (rpc_.flowDirection() == FlowDirection::BIDIRECTIONAL) {
                k1 = static_cast<std::size_t>(dct_->nSpcMeshPts() / 4 + 1);
                k2 = static_cast<std::size_t>(3 * dct_->nSpcMeshPts() / 4 - 1);
                k3 = static_cast<std::size_t>(dct_->nSpcMeshPts() / 4);
            }
            else if (rpc_.flowDirection() == FlowDirection::POSITIVE) {
                k1 = 0;
                k2 = static_cast<std::size_t>(dct_->nSpcMeshPts() / 2 - 1);
                k3 = 0;
            }
            else {
                k1 = SYSDim_
                        * static_cast<std::size_t>(dct_->nSpcMeshPts() / 2 + 1);
                k2 = 0;
                k3 = SYSDim_
                        * static_cast<std::size_t>(dct_->nSpcMeshPts() / 2 - 1);
            }

            std::size_t index_(hasPress_ ? SYSDim_ + 1 : SYSDim_);
            for (std::size_t i = 0; i < k1; ++i)
                dct_->u[i] = rpc_.leftState()[index_];
            l = static_cast<std::size_t>(dct_->nSpcMeshPts());
            for (std::size_t i = k2; i < l; ++i)
                dct_->u[i] = rpc_.rightState()[index_];
            for (std::size_t i = 0; i < shrinkSize_; ++i, ++k3)
                dct_->u[k3] = sol[i];
        }
    }
}

void
RiemannProblem::setInitialSolution()
{
    if (dct_) {
        int ii = xi_min < 0. ?
                0 :
                0; // there was a -5; for the else part, which makes no sense
                   // without a comment explaining it
        for (std::size_t i = 0;
                i < static_cast<std::size_t>(dct_->nSpcMeshPts()); i++, ii++)
            dct_->x[i] = rpc_.leftSpcDelim()
                    + static_cast<double>(ii) * (dct_->spcMeshStpSize());
        double XJUMPSIZE((rpc_.rightSpcDelim() - rpc_.leftSpcDelim()) * RP_TOL);
        for (std::size_t i = 0, j = 0;
                i < static_cast<std::size_t>(dct_->nSpcMeshPts());
                i++, j += SYSDim_) {
            double cte(
                    (1. + std::tanh((dct_->x[i] - rpc_.jumSpcPt()) / XJUMPSIZE))
                    * .5);
            for (std::size_t k = 0; k < SYSDim_; ++k) {
                if (rpc_.rightState()[k] != rpc_.leftState()[k])
                    dct_->V[j + k] = (1. - cte) * rpc_.leftState()[k]
                            + cte * rpc_.rightState()[k];
                else
                    dct_->V[j + k] = rpc_.leftState()[k];
            }
        }
        for (std::size_t i = 0, j = 0; i < 3; i++, j += SYSDim_) {
            for (std::size_t k = 0; k < SYSDim_; ++k)
                dct_->V[j + k] = rpc_.leftState()[k];
        }
        for (std::size_t i = static_cast<std::size_t>(dct_->nSpcMeshPts()) - 3,
                         j = i * SYSDim_;
                i < static_cast<std::size_t>(dct_->nSpcMeshPts());
                i++, j += SYSDim_) {
            for (std::size_t k = 0; k < SYSDim_; ++k)
                dct_->V[j + k] = rpc_.rightState()[k];
        }
    }
}

void
RiemannProblem::load_dct()
{
    if (!dct_
            || static_cast<std::size_t>(dct_->nSpcMeshPts())
                    != rpc_.nSpcMeshPts()) {
#ifdef ENABLE_OPENMP
        if (rpc_.parallel()) {
            dct_ = std::make_unique<DiscretizationParallelized>(phys_, bdry_,
                    PDEDim_, CSTDim_, rpc_.nSpcMeshPts(), rpc_.spcMeshStpSize(),
                    hasPress_, hasVel_);
        }
        else {
#endif /* ENABLE_OPENMP */
            dct_ = std::make_unique<Discretization>(phys_, bdry_, PDEDim_,
                    CSTDim_, rpc_.nSpcMeshPts(), rpc_.spcMeshStpSize(),
                    hasPress_, hasVel_);
#ifdef ENABLE_OPENMP
        }
#endif
        nwt_ = std::make_unique<Newton>(*dct_, 500);
        setInitialSolution();
        dct_->setUpPDEMembers(pdemc_);
        dct_->SetInitialPressureAndVelocity();
    }
}

void
RiemannProblem::makeReport(bool makeanyway)
{
    if (rptr_.timetomake() || makeanyway) {
        if (rpc_.automaticSetUp() && rpc_.convergencePlot())
            rptr_.spaceRange(xi_min * currentTime, xi_max * currentTime);
        else
            rptr_.spaceRange();
        std::vector<std::vector<double>> solution_;
        std::vector<double> spcMesh_;
        double tt_;
        getSolution(solution_, tt_);
        getSpcMesh(spcMesh_);
        rptr_.make(solution_, spcMesh_, tt_, makeanyway);
    }
}

bool
RiemannProblem::checkConvergence()
{
    if (!dct_ || not checkForConvergence_)
        return false;

    prevCheckedSolutions_[0].resize(Dim_);
    for (auto& sol : prevCheckedSolutions_[0])
        sol.resize(rpc_.nSpcMeshPts());
    prevCheckedSolutions_[1].resize(Dim_);
    for (auto& sol : prevCheckedSolutions_[1])
        sol.resize(rpc_.nSpcMeshPts());

    auto& sol_ = prevCheckedSolutions_[0];
    double x_min(xi_min * currentTime), x_max(xi_max * currentTime);
    double deltax(
            (x_max - x_min) / static_cast<double>(rpc_.nSpcMeshPts() - 1));

#ifdef DEBUG_RIEMANN_PROBLEM
    std::cerr << "x_min --> " << x_min << " x_max --> " << x_max
              << " deltax --> " << deltax << std::endl;
#endif

    for (std::size_t i = 0; i < rpc_.nSpcMeshPts(); ++i) {
        double currX(x_min + static_cast<double>(i) * deltax);
        if (currX < rpc_.leftSpcDelim()) {
            std::size_t j(0);
            for (; j < PDEDim_; ++j)
                sol_[j][i] = dct_->V[j];
            if (hasPress_)
                sol_[j++][i] = dct_->p[0];
            if (hasVel_)
                sol_[j][i] = dct_->u[0];
        }
        else if (currX > rpc_.rightSpcDelim()) {
            std::size_t j(0), jj((rpc_.nSpcMeshPts() - 1) * SYSDim_);
            for (; j < PDEDim_; ++j)
                sol_[j][i] = dct_->V[j + jj];
            if (hasPress_)
                sol_[j++][i] = dct_->p[(rpc_.nSpcMeshPts() - 1)];
            if (hasVel_)
                sol_[j][i] = dct_->u[(rpc_.nSpcMeshPts() - 1)];
        }
        else {
            std::size_t index = rpc_.nSpcMeshPts()
                    - static_cast<std::size_t>((rpc_.rightSpcDelim() - currX)
                            / rpc_.spcMeshStpSize());
            std::size_t j(0), jj(index * SYSDim_), jjj((index + 1) * SYSDim_);

#ifdef DEBUG_RIEMANN_PROBLEM
            std::cerr << "index --> " << index << " ";
#endif
            double xL(dct_->x[index]), xR(dct_->x[index + 1]);
            for (; j < PDEDim_; ++j) {
                double fL(dct_->V[j + jj]), fR(dct_->V[j + jjj]);
                sol_[j][i] = linear_interp(fL, fR, currX, xL, xR);
            }
            if (hasPress_) {
                double fL(dct_->p[index]), fR(dct_->p[index + 1]);
                sol_[j++][i] = linear_interp(fL, fR, currX, xL, xR);
            }
            if (hasVel_) {
                double fL(dct_->u[index]), fR(dct_->u[index + 1]);
                sol_[j][i] = linear_interp(fL, fR, currX, xL, xR);
            }
        }

#ifdef DEBUG_RIEMANN_PROBLEM
        std::cerr << "currX --> " << currX << " (u, v) --> (" << sol_[0][i]
                  << ", " << sol_[1][i] << ")" << std::endl;
#endif
    }

    prevCheckedSolutions_[0].swap(prevCheckedSolutions_[1]);

    if (convCheckTimes <= 1) {
        refNorm_ = norm(prevCheckedSolutions_[1]);

#ifdef DEBUG_RIEMANN_PROBLEM
        std::cerr << "refNorm_ --> " << refNorm_ << std::endl;
#endif

        return false;
    }

#ifdef DEBUG_RIEMANN_PROBLEM
    std::cerr << "norm --> "
              << norm(prevCheckedSolutions_[0], prevCheckedSolutions_[1])
              << std::endl;
#endif

    return norm(prevCheckedSolutions_[0], prevCheckedSolutions_[1]) / refNorm_
            < RP_TOL; // rpc_.spcMeshStpSize()*rpc_.spcMeshStpSize();
}

bool
RiemannProblem::reachedBoundary(double tol)
{
    if (not checkForChangesAtBoundary_)
        return false;

    std::size_t k = (3 <= dct_->nSpcMeshPts() ? 3 : dct_->nSpcMeshPts());
    for (std::size_t i = 0; i < k; ++i) {
        for (std::size_t j = 0; j < SYSDim_; ++j)
            if (std::abs(dct_->V[i * SYSDim_ + j] - rpc_.leftState()[j]) > tol)
                return true;

        if (hasPress_
                && (std::abs(dct_->p[i] - rpc_.leftState()[SYSDim_]) > tol))
            return true;

        if (hasPress_ && hasVel_
                && (std::abs(dct_->u[i] - rpc_.leftState()[SYSDim_ + 1]) > tol))
            return true;
        else if (!hasPress_ && hasVel_
                && (std::abs(dct_->u[i] - rpc_.leftState()[SYSDim_]) > tol))
            return true;
    }

    k = (dct_->nSpcMeshPts() >= 3 ? dct_->nSpcMeshPts() - 3 :
                                    dct_->nSpcMeshPts());
    for (std::size_t i = k; i < static_cast<std::size_t>(dct_->nSpcMeshPts());
            ++i) {
        for (std::size_t j = 0; j < SYSDim_; ++j)
            if (std::abs(dct_->V[i * SYSDim_ + j] - rpc_.rightState()[j]) > tol)
                return true;

        if (hasPress_
                && (std::abs(dct_->p[i] - rpc_.rightState()[SYSDim_]) > tol))
            return true;

        if (hasPress_ && hasVel_
                && (std::abs(dct_->u[i] - rpc_.rightState()[SYSDim_ + 1])
                        > tol))
            return true;
        else if (!hasPress_ && hasVel_
                && (std::abs(dct_->u[i] - rpc_.rightState()[SYSDim_]) > tol))
            return true;
    }

    return false;
}

void
RiemannProblem::startCalc(std::string /* physicsName */, double time_p,
        double ZEROTOL, double DIFFTOL, bool boundaryCheck)
{
    /* set up calculation settings */
    if (time_p == 0.) {
        convCheckTimes = 1;
        hasConverged = false;
    }
    load_dct();
    if (rpc_.automaticSetUp())
        setUpSpeedLimits(rpc_);

    /* initialize counter, collect current time and report first time */
    std::size_t timeCounter = 0;
    currentTime = time_p;
    rptr_.start();
    makeReport();

    /* set up for the time loop */
    bool isFinished_ = false;

    /* time loop */
    while (not isFinished_) {

        /* make sure to time step correctly */
        if (not(rpc_.automaticSetUp() && rpc_.useNTMP())
                && ((currentTime + rpc_.timeMeshStpSize_)
                        > rpc_.infTimeDelim())) {
            rpc_.timeMeshStpSize_ = rpc_.infTimeDelim() - currentTime;
            dct_->DT(rpc_.timeMeshStpSize_);
        }

        /* time step using newton's method */
        switch (nwt_->TimeStep(
                rpc_.timeMeshStpSize(), currentTime, ZEROTOL, DIFFTOL)) {
        case 0:
            isFinished_ = true;
            std::cerr << "Looping is finished because solving "
                      << "the linear system failed.\n";
            break;

        case -1:
            isFinished_ = true;
            std::cerr << "Looping is finished because the maximum number "
                      << "of iterations was reached.\n";
            break;
        default:
            break;
        }

        /* advancing time */
        timeCounter++;
        currentTime += rpc_.timeMeshStpSize();
        isFinished_ = isFinished_
                || std::abs(currentTime - rpc_.infTimeDelim()) <= 1.0e-18;
        adaptTimeMeshStpSize(timeCounter, currentTime, rpc_.infTimeDelim());

        /* checking for changes at the boundary */
        if ((not isFinished_) && boundaryCheck
                && currentTime > .05 * rpc_.infTimeDelim())
            isFinished_ = reachedBoundary(rpc_.spcMeshStpSize() * 1.0e-3);

        /* checking convergence */
        if ((not isFinished_)
                && currentTime > static_cast<double>(convCheckTimes)
                                * rpc_.timeStepConvCheck()) {
            isFinished_ = hasConverged = checkConvergence();
            convCheckTimes++;
        }
        isFinished_ = not((not isFinished_)
                && (rpc_.automaticSetUp()
                        || timeCounter < (rpc_.nTimeMeshPts() - 1)));

        /* reporting the solution */
        makeReport(isFinished_);
    }
    rptr_.finish();
    isFinished_ = true;
}

void
RiemannProblem::continueCalc(
        std::string physicsName, double ZEROTOL, double DIFFTOL)
{
    shrinkSolution();
    convCheckTimes /= 2;
    startCalc(physicsName, currentTime * 0.5, ZEROTOL, DIFFTOL);
}

void
RiemannProblem::solve(std::string physicsName, double ZEROTOL, double DIFFTOL)
{
    hasConverged = false;
    startCalc(physicsName, 0., ZEROTOL, DIFFTOL, false);
    while (!hasConverged)
        continueCalc(physicsName, ZEROTOL, DIFFTOL);
}
