#include "GnuplotReporter.h"

#ifdef ENABLE_GNUPLOT

void
GnuplotReporter::start()
{
    PlotReporter::start();

    auto nGroups = reportGroups_.size();
    if (nGroups == 0)
        return;

    gnuplots_.resize(nGroups);
    for (std::size_t i = 0; i < nGroups; ++i) {
        gnuplots_[i] = gnuplot_init();
        char tmp[6] = "lines";
        gnuplot_setstyle(gnuplots_[i], tmp);
    }
}

void
GnuplotReporter::make(const std::vector<std::vector<double>>& solution,
        const std::vector<double>& spcMesh, double time, bool /* makeanyway */)
{
    auto nGroups = reportGroups_.size();
    if (nGroups == 0)
        return;

    for (std::size_t i = 0; i < nGroups; ++i) {
        auto title = (spaceNames_[i] + " at t=" + std::to_string(time));
        auto cmd = "set title '" + title + "'";
        gnuplot_cmd(gnuplots_[i], cmd.c_str());
        if (not spaceRangeAutomatic_) {
            cmd = "set xrange [" + std::to_string(xl_) + ":"
                    + std::to_string(xr_) + "]";
            gnuplot_cmd(gnuplots_[i], cmd.c_str());
        }
        gnuplot_resetplot(gnuplots_[i]);
        for (std::size_t j = 0; j < reportGroups_[i].size(); ++j) {
            auto curveIndex = reportGroups_[i][j];
            auto curveName = std::string("curve ") + std::to_string(curveIndex);
            gnuplot_plot_xy(gnuplots_[i], spcMesh.data(),
                    solution[curveIndex].data(),
                    static_cast<int>(std::min(
                            spcMesh.size(), solution[curveIndex].size())),
                    curveName.c_str());
        }
    }
}

void
GnuplotReporter::finish()
{
    auto nGroups = reportGroups_.size();
    if (nGroups == 0)
        return;

    for (std::size_t i = 0; i < nGroups; ++i) {
        auto cmd = "set term pdf; set output '" + spaceNames_[i]
                + ".pdf'; replot";
        gnuplot_cmd(gnuplots_[i], cmd.c_str());
        gnuplot_close(gnuplots_[i]);
    }
}

#endif /* ENABLE_GNUPLOT */
