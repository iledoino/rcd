#ifndef SRC_KEYMATCH_KEYMATCH_H
#define SRC_KEYMATCH_KEYMATCH_H


#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <iostream>
#include <map>
#include <string>
#include <vector>

/**
 * KeyMatch Object is an aid to reading parameters from files
 */

class KeyMatch
{
public:
    KeyMatch(std::string defaultInput_ = "input.in",
            std::string defaultOutput_ = "output.in");
    virtual ~KeyMatch() noexcept = default;

    template <typename T>
    void register_entry(T* data, std::size_t size, std::string key);
    template <typename T>
    void register_entry(std::vector<T>& data, std::string key)
    {
        register_entry(data.data(), data.size(), key);
    }
    template <typename T>
    void register_entry(T& data, std::string key)
    {
        register_entry(&data, 1, key);
    }

    void make();
    void define_in_out(int argc, char** argv);

    const std::string input() const { return in; }
    const std::string output() const { return out; }

protected:
    int readDouble(double* output);
    int readInt(int* output);
    int readUns(unsigned* output);
    int readUnsLong(unsigned long* output);
    int readString(std::string* output);
    virtual void match(char* pch);

private:
    KeyMatch(const KeyMatch&) = delete;
    KeyMatch& operator=(const KeyMatch&) = delete;

    enum class RegistryType {
        DOUBLE,
        INTEGER,
        UNSIGNED,
        UNSIGNEDLONG,
        STRING,
        NON_LISTED
    };
    struct Registry
    {
        double* data_double_ = nullptr;
        int* data_int_ = nullptr;
        unsigned* data_unsigned_ = nullptr;
        unsigned long* data_unsigned_long_ = nullptr;
        std::string* data_stdstring_ = nullptr;
        std::size_t size_ = 0;
        RegistryType type_ = RegistryType::NON_LISTED;
    };
    std::map<std::string, Registry> map_;

    std::string defaultInput, defaultOutput;
    std::string in, out;
    int end_of_file;
    FILE* fp_;
    char *end_, *line_, *pch_;
    size_t len_;
    ssize_t read_;

    static int read_double(FILE* fp, char** line, size_t* len, ssize_t* read,
            char* pch, char** end, double* output);
    static int read_int(FILE* fp, char** line, size_t* len, ssize_t* read,
            char* pch, int* output);
    static int read_uns(FILE* fp, char** line, size_t* len, ssize_t* read,
            char* pch, unsigned* output);
    static int read_unsLong(FILE* fp, char** line, size_t* len, ssize_t* read,
            char* pch, unsigned long* output);
    static int read_string(FILE* fp, char** line, size_t* len, ssize_t* read,
            char* pch, std::string* output);
};

#endif /* SRC_KEYMATCH_KEYMATCH_H */
