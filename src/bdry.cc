#include "bdry.h"

Bdry::~Bdry() = default;

/*
 * This version of setPointers adjust the pointers for the case of simple
 * boundary conditions, which depend just on 2 discrete solution points
 */
void
Bdry::setPointers(double* V_i_, double* V_2i_, double* Vn_i_, double* Vn_2i_,
        double* f_i_, double* Df_iDV_i_, double* Df_iDV_2i_)
{
    V_i = V_i_;
    V_2i = V_2i_;
    Vn_i = Vn_i_;
    Vn_2i = Vn_2i_;
    f_i = f_i_;
    Df_iDV_i = Df_iDV_i_;
    Df_iDV_2i = Df_iDV_2i_;

    V_3i = nullptr;
    Vn_3i = nullptr;
    Df_iDV_3i = nullptr;
}

/*
 * This version adjust the pointers for the case which boundary conditions
 * are set by expressions that depend on 3 discrete solution points
 */
void
Bdry::setPointers(double* V_i_, double* V_2i_, double* V_3i_, double* Vn_i_,
        double* Vn_2i_, double* Vn_3i_, double* f_i_, double* Df_iDV_i_,
        double* Df_iDV_2i_, double* Df_iDV_3i_)
{
    V_i = V_i_;
    V_2i = V_2i_;
    V_3i = V_3i_;
    Vn_i = Vn_i_;
    Vn_2i = Vn_2i_;
    Vn_3i = Vn_3i_;
    f_i = f_i_;
    Df_iDV_i = Df_iDV_i_;
    Df_iDV_2i = Df_iDV_2i_;
    Df_iDV_3i = Df_iDV_3i_;
}

PrescribedFlux_L::PrescribedFlux_L(const std::size_t& PDEDim_,
        const std::size_t& CSTDim_, PhysicsRCD& physicsRCD_p,
        std::vector<double> Feval_p, double dx_p, double pDirichlet_p,
        double uDirichlet_p)
    : Bdry(1)
    , PDEDim(PDEDim_)
    , CSTDim(CSTDim_)
    , SYSDim(PDEDim_ + CSTDim_)
    , physicsRCD_(physicsRCD_p)
    , Feval_(Feval_p)
    , dx_(dx_p)
    , pDirichlet_(pDirichlet_p)
    , uDirichlet_(uDirichlet_p)
{
}

void
PrescribedFlux_L::SetBoundaryCondition(
        double x, double tPrev, double tCurr, std::size_t idx)
{
    PhysicsDataHolder phyDH;
    phyDH.allocate(PDEDim, CSTDim);
    physicsRCD_.setConstantValues(phyDH);
    std::vector<double> auxData((PDEDim + CSTDim) * (PDEDim + CSTDim));

    for (std::size_t i = 0; i != PDEDim; ++i)
        f_i[i] = 0.0;

    // assembly with respect to current index
    physicsRCD_.setPDEAndCstFunctions(
            phyDH, Vn_i, pDirichlet_, uDirichlet_, x, tPrev, idx);
    for (std::size_t i = 0; i != pdeMembers_.size(); ++i) {
        if (pdeMembers_[i]->memberName_ == "diffusion")
            continue;
        pdeMembers_[i]->AddMemberToRHS(phyDH, auxData.data(),
                f_i - phyDH.zeroBcksDim, auxData.data(), auxData.data(),
                auxData.data());
    }

    // assembly with respect to previous index
    physicsRCD_.setPDEAndCstFunctions(
            phyDH, Vn_i, pDirichlet_, uDirichlet_, x - dx_, tPrev, idx);
    for (std::size_t i = 0; i != PDEDim; ++i)
        phyDH.F[i] = Feval_[i];
    for (std::size_t i = 0; i != pdeMembers_.size(); ++i) {
        if (pdeMembers_[i]->memberName_ == "diffusion")
            continue;
        pdeMembers_[i]->AddMemberToRHS(phyDH, auxData.data(), auxData.data(),
                f_i - phyDH.zeroBcksDim, auxData.data(), auxData.data());
    }

    // assembly with respect to next index
    physicsRCD_.setPDEAndCstFunctions(
            phyDH, Vn_2i, pDirichlet_, uDirichlet_, x + dx_, tPrev, idx);
    for (std::size_t i = 0; i != pdeMembers_.size(); ++i) {
        if (pdeMembers_[i]->memberName_ == "diffusion")
            continue;
        pdeMembers_[i]->AddMemberToRHS(phyDH, f_i - phyDH.zeroBcksDim,
                auxData.data(), auxData.data(), auxData.data(), auxData.data());
    }

    // bringing right hand side to left hand side
    for (std::size_t i = 0; i != PDEDim; ++i)
        f_i[i] *= -1.0;

    // assembly with respect to current index
    physicsRCD_.jet(phyDH, V_i, Vn_i, pDirichlet_, uDirichlet_, x, tCurr, idx);
    for (std::size_t j = 0; j != pdeMembers_.size(); ++j) {
        if (pdeMembers_[j]->memberName_ == "diffusion")
            continue;
        pdeMembers_[j]->AddMemberToLHS(phyDH, auxData.data(),
                f_i - phyDH.zeroBcksDim, auxData.data(), auxData.data(),
                auxData.data());

        pdeMembers_[j]->AddMemberToJG(phyDH, auxData.data(),
                Df_iDV_i - phyDH.zeroBcksSize, auxData.data(), auxData.data(),
                Df_iDV_2i, auxData.data(), auxData.data(), auxData.data(),
                auxData.data());
    }

    // assembly with respect to previous index
    physicsRCD_.jet(
            phyDH, V_i, Vn_i, pDirichlet_, uDirichlet_, x - dx_, tCurr, idx);
    for (std::size_t i = 0; i != PDEDim; ++i)
        phyDH.F[i] = Feval_[i];
    for (std::size_t i = 0; i != (PDEDim + CSTDim) * PDEDim; ++i)
        phyDH.DF[i] = 0.0;
    for (std::size_t j = 0; j != pdeMembers_.size(); ++j) {
        if (pdeMembers_[j]->memberName_ == "diffusion")
            continue;
        pdeMembers_[j]->AddMemberToLHS(phyDH, auxData.data(), auxData.data(),
                f_i - phyDH.zeroBcksDim, auxData.data(), auxData.data());

        pdeMembers_[j]->AddMemberToJG(phyDH, auxData.data(), auxData.data(),
                Df_iDV_i - phyDH.zeroBcksSize, auxData.data(), auxData.data(),
                auxData.data(), auxData.data(), auxData.data(), auxData.data());
    }

    // assembly with respect to next index
    physicsRCD_.jet(
            phyDH, V_2i, Vn_2i, pDirichlet_, uDirichlet_, x + dx_, tCurr, idx);
    for (std::size_t j = 0; j != pdeMembers_.size(); ++j) {
        if (pdeMembers_[j]->memberName_ == "diffusion")
            continue;
        pdeMembers_[j]->AddMemberToLHS(phyDH, f_i - phyDH.zeroBcksDim,
                auxData.data(), auxData.data(), auxData.data(), auxData.data());

        pdeMembers_[j]->AddMemberToJG(phyDH, Df_iDV_i - phyDH.zeroBcksSize,
                auxData.data(), auxData.data(), Df_iDV_2i, auxData.data(),
                auxData.data(), auxData.data(), auxData.data(), auxData.data());
    }
}

PrescribedFlux_R::PrescribedFlux_R(const std::size_t& PDEDim_,
        const std::size_t& CSTDim_, PhysicsRCD& physicsRCD_p,
        std::vector<double> Feval_p, double dx_p, double pDirichlet_p,
        double uDirichlet_p)
    : Bdry(1)
    , PDEDim(PDEDim_)
    , CSTDim(CSTDim_)
    , SYSDim(PDEDim_ + CSTDim_)
    , physicsRCD_(physicsRCD_p)
    , Feval_(Feval_p)
    , dx_(dx_p)
    , pDirichlet_(pDirichlet_p)
    , uDirichlet_(uDirichlet_p)
{
}

void
PrescribedFlux_R::SetBoundaryCondition(
        double x, double tPrev, double tCurr, std::size_t idx)
{
    PhysicsDataHolder phyDH;
    phyDH.allocate(PDEDim, CSTDim);
    physicsRCD_.setConstantValues(phyDH);
    std::vector<double> auxData((PDEDim + CSTDim) * (PDEDim + CSTDim));

    for (std::size_t i = 0; i != PDEDim; ++i)
        f_i[i] = 0.0;

    // assembly with respect to current index
    physicsRCD_.setPDEAndCstFunctions(
            phyDH, Vn_i, pDirichlet_, uDirichlet_, x, tPrev, idx);
    for (std::size_t i = 0; i != pdeMembers_.size(); ++i) {
        if (pdeMembers_[i]->memberName_ == "diffusion")
            continue;
        pdeMembers_[i]->AddMemberToRHS(phyDH, auxData.data(),
                f_i - phyDH.zeroBcksDim, auxData.data(), auxData.data(),
                auxData.data());
    }

    // assembly with respect to previous index
    physicsRCD_.setPDEAndCstFunctions(
            phyDH, Vn_i, pDirichlet_, uDirichlet_, x - dx_, tPrev, idx);
    for (std::size_t i = 0; i != pdeMembers_.size(); ++i) {
        if (pdeMembers_[i]->memberName_ == "diffusion")
            continue;
        pdeMembers_[i]->AddMemberToRHS(phyDH, auxData.data(), auxData.data(),
                f_i - phyDH.zeroBcksDim, auxData.data(), auxData.data());
    }

    // assembly with respect to next index
    physicsRCD_.setPDEAndCstFunctions(
            phyDH, Vn_2i, pDirichlet_, uDirichlet_, x + dx_, tPrev, idx);
    for (std::size_t i = 0; i != PDEDim; ++i)
        phyDH.F[i] = Feval_[i];
    for (std::size_t i = 0; i != pdeMembers_.size(); ++i) {
        if (pdeMembers_[i]->memberName_ == "diffusion")
            continue;
        pdeMembers_[i]->AddMemberToRHS(phyDH, f_i - phyDH.zeroBcksDim,
                auxData.data(), auxData.data(), auxData.data(), auxData.data());
    }

    // bringing right hand side to left hand side
    for (std::size_t i = 0; i != PDEDim; ++i)
        f_i[i] *= -1.0;

    // assembly with respect to current index
    physicsRCD_.jet(phyDH, V_i, Vn_i, pDirichlet_, uDirichlet_, x, tCurr, idx);
    for (std::size_t j = 0; j != pdeMembers_.size(); ++j) {
        if (pdeMembers_[j]->memberName_ == "diffusion")
            continue;
        pdeMembers_[j]->AddMemberToLHS(phyDH, auxData.data(),
                f_i - phyDH.zeroBcksDim, auxData.data(), auxData.data(),
                auxData.data());

        pdeMembers_[j]->AddMemberToJG(phyDH, auxData.data(),
                Df_iDV_i - phyDH.zeroBcksSize, auxData.data(), auxData.data(),
                auxData.data(), Df_iDV_2i, auxData.data(), auxData.data(),
                auxData.data());
    }

    // assembly with respect to previous index
    physicsRCD_.jet(
            phyDH, V_i, Vn_i, pDirichlet_, uDirichlet_, x - dx_, tCurr, idx);
    for (std::size_t j = 0; j != pdeMembers_.size(); ++j) {
        if (pdeMembers_[j]->memberName_ == "diffusion")
            continue;
        pdeMembers_[j]->AddMemberToLHS(phyDH, auxData.data(), auxData.data(),
                f_i - phyDH.zeroBcksDim, auxData.data(), auxData.data());

        pdeMembers_[j]->AddMemberToJG(phyDH, auxData.data(), auxData.data(),
                Df_iDV_i - phyDH.zeroBcksSize, auxData.data(), auxData.data(),
                auxData.data(), Df_iDV_2i, auxData.data(), auxData.data());
    }

    // assembly with respect to next index
    physicsRCD_.jet(
            phyDH, V_2i, Vn_2i, pDirichlet_, uDirichlet_, x + dx_, tCurr, idx);
    for (std::size_t i = 0; i != PDEDim; ++i)
        phyDH.F[i] = Feval_[i];
    for (std::size_t i = 0; i != (PDEDim + CSTDim) * PDEDim; ++i)
        phyDH.DF[i] = 0.0;
    for (std::size_t j = 0; j != pdeMembers_.size(); ++j) {
        if (pdeMembers_[j]->memberName_ == "diffusion")
            continue;
        pdeMembers_[j]->AddMemberToLHS(phyDH, f_i - phyDH.zeroBcksDim,
                auxData.data(), auxData.data(), auxData.data(), auxData.data());

        pdeMembers_[j]->AddMemberToJG(phyDH, Df_iDV_i - phyDH.zeroBcksSize,
                auxData.data(), auxData.data(), auxData.data(), auxData.data(),
                auxData.data(), auxData.data(), auxData.data(), auxData.data());
    }
}

/*
 * Constructors for Robin BCs
 */
PDE_RobinBC_FO_L::PDE_RobinBC_FO_L(const std::size_t& PDEDim_,
        const std::size_t& CSTDim_, const double& dx_, const double alpha_)
    : Bdry(1)
    , dx(dx_)
    , PDEDim(PDEDim_)
    , CSTDim(CSTDim_)
    , SYSDim(PDEDim_ + CSTDim_)
    , alpha(alpha_)
    , a(PDEDim * SYSDim)
    , b(PDEDim * SYSDim)
    , c(PDEDim * SYSDim)
{
}

PDE_RobinBC_FO_L::PDE_RobinBC_FO_L(const std::size_t& PDEDim_p,
        const std::size_t& CSTDim_p, const double& dx_p,
        const std::vector<double>& a_p, const std::vector<double>& b_p,
        const std::vector<double>& c_p, const double alpha_)
    : PDE_RobinBC_FO_L(PDEDim_p, CSTDim_p, dx_p, alpha_)
{
    if (a_p.size() >= PDEDim * SYSDim) {
        for (std::size_t i = 0; i < PDEDim * SYSDim; ++i) {
            a[i] = a_p[i];
            b[i] = b_p[i];
            c[i] = c_p[i];
        }
    }
    else {
        for (std::size_t i = 0; i < PDEDim; ++i) {
            a[i * SYSDim + i] = a_p[i];
            b[i * SYSDim + i] = b_p[i];
            c[i * SYSDim + i] = c_p[i];
        }
    }
}

void
PDE_RobinBC_FO_L::SetBoundaryCondition(
        double x, double tPrev, double tCurr, std::size_t /* idx */)
{
    SetBoundaryValues(x, tCurr);
    for (std::size_t i = 0; i != PDEDim; ++i) {
        f_i[i] = alpha * c[i];
        for (std::size_t j = 0; j != (PDEDim + CSTDim); ++j) {
            std::size_t k = i * (PDEDim + CSTDim) + j;
            f_i[i] += alpha * (a[k] * V_i[j] + b[k] * (V_2i[j] - V_i[j])) / dx;
        }
    }
    for (std::size_t i = 0; i != (PDEDim + CSTDim) * PDEDim; ++i) {
        Df_iDV_i[i] = alpha * (a[i] - b[i]) / dx;
        Df_iDV_2i[i] = alpha * b[i] / dx;
    }

    SetBoundaryValues(x, tPrev);
    for (std::size_t i = 0; i != PDEDim; ++i) {
        f_i[i] = (1. - alpha) * c[i];
        for (std::size_t j = 0; j != (PDEDim + CSTDim); ++j) {
            std::size_t k = i * (PDEDim + CSTDim) + j;
            f_i[i] += (1. - alpha)
                    * (a[k] * Vn_i[j] + b[k] * (Vn_2i[j] - Vn_i[j])) / dx;
        }
    }
}

PDE_RobinBC_FO_R::PDE_RobinBC_FO_R(const std::size_t& PDEDim_,
        const std::size_t& CSTDim_, const double& dx_, const double alpha_)
    : Bdry(1)
    , dx(dx_)
    , PDEDim(PDEDim_)
    , CSTDim(CSTDim_)
    , SYSDim(PDEDim_ + CSTDim_)
    , alpha(alpha_)
    , a(PDEDim * SYSDim)
    , b(PDEDim * SYSDim)
    , c(PDEDim * SYSDim)
{
}

PDE_RobinBC_FO_R::PDE_RobinBC_FO_R(const std::size_t& PDEDim_p,
        const std::size_t& CSTDim_p, const double& dx_p,
        const std::vector<double>& a_p, const std::vector<double>& b_p,
        const std::vector<double>& c_p, const double alpha_)
    : PDE_RobinBC_FO_R(PDEDim_p, CSTDim_p, dx_p, alpha_)
{
    if (a_p.size() >= PDEDim * SYSDim) {
        for (std::size_t i = 0; i < PDEDim * SYSDim; ++i) {
            a[i] = a_p[i];
            b[i] = b_p[i];
            c[i] = c_p[i];
        }
    }
    else {
        for (std::size_t i = 0; i < PDEDim; ++i) {
            a[i * SYSDim + i] = a_p[i];
            b[i * SYSDim + i] = b_p[i];
            c[i * SYSDim + i] = c_p[i];
        }
    }
}

void
PDE_RobinBC_FO_R::SetBoundaryCondition(
        double x, double tPrev, double tCurr, std::size_t /* idx */)
{
    SetBoundaryValues(x, tCurr);
    for (std::size_t i = 0; i != PDEDim; ++i) {
        f_i[i] = alpha * c[i];
        for (std::size_t j = 0; j != (PDEDim + CSTDim); ++j) {
            std::size_t k = i * (PDEDim + CSTDim) + j;
            f_i[i] += alpha * (a[k] * V_i[j] + b[k] * (V_i[j] - V_2i[j])) / dx;
        }
    }
    for (std::size_t i = 0; i != (PDEDim + CSTDim) * PDEDim; ++i) {
        Df_iDV_i[i] = alpha * (a[i] + b[i]) / dx;
        Df_iDV_2i[i] = -alpha * b[i] / dx;
    }

    SetBoundaryValues(x, tPrev);
    for (std::size_t i = 0; i != PDEDim; ++i) {
        f_i[i] = (1. - alpha) * c[i];
        for (std::size_t j = 0; j != (PDEDim + CSTDim); ++j) {
            std::size_t k = i * (PDEDim + CSTDim) + j;
            f_i[i] += (1. - alpha)
                    * (a[k] * Vn_i[j] + b[k] * (Vn_i[j] - Vn_2i[j])) / dx;
        }
    }
}

PDE_RobinBC_SO_L::PDE_RobinBC_SO_L(const std::size_t& PDEDim_,
        const std::size_t& CSTDim_, const double& dx_, const double alpha_)
    : Bdry(2)
    , dx(dx_)
    , PDEDim(PDEDim_)
    , CSTDim(CSTDim_)
    , SYSDim(PDEDim_ + CSTDim_)
    , alpha(alpha_)
    , a(PDEDim * SYSDim)
    , b(PDEDim * SYSDim)
    , c(PDEDim * SYSDim)
{
}

PDE_RobinBC_SO_L::PDE_RobinBC_SO_L(const std::size_t& PDEDim_p,
        const std::size_t& CSTDim_p, const double& dx_p,
        const std::vector<double>& a_p, const std::vector<double>& b_p,
        const std::vector<double>& c_p, const double alpha_)
    : PDE_RobinBC_SO_L(PDEDim_p, CSTDim_p, dx_p, alpha_)
{
    if (a_p.size() >= PDEDim * SYSDim) {
        for (std::size_t i = 0; i < PDEDim * SYSDim; ++i) {
            a[i] = a_p[i];
            b[i] = b_p[i];
            c[i] = c_p[i];
        }
    }
    else {
        for (std::size_t i = 0; i < PDEDim; ++i) {
            a[i * SYSDim + i] = a_p[i];
            b[i * SYSDim + i] = b_p[i];
            c[i * SYSDim + i] = c_p[i];
        }
    }
}

void
PDE_RobinBC_SO_L::SetBoundaryCondition(
        double x, double tPrev, double tCurr, std::size_t /* idx */)
{
    SetBoundaryValues(x, tCurr);
    for (std::size_t i = 0; i != PDEDim; ++i) {
        f_i[i] = alpha * c[i];
        for (std::size_t j = 0; j != (PDEDim + CSTDim); ++j) {
            std::size_t k = i * (PDEDim + CSTDim) + j;
            f_i[i] += alpha
                    * (a[k] * V_i[j]
                            + b[k] * (-V_3i[j] + 4.0 * V_2i[j] - 3.0 * V_i[j]))
                    / (2.0 * dx);
        }
    }
    for (std::size_t i = 0; i != (PDEDim + CSTDim) * PDEDim; ++i) {
        Df_iDV_i[i] = alpha * (a[i] - 3.0 * b[i]) / (2.0 * dx);
        Df_iDV_2i[i] = 4.0 * alpha * b[i] / (2.0 * dx);
        Df_iDV_3i[i] = -alpha * b[i] / (2.0 * dx);
    }

    SetBoundaryValues(x, tPrev);
    for (std::size_t i = 0; i != PDEDim; ++i) {
        f_i[i] = (1. - alpha) * c[i];
        for (std::size_t j = 0; j != (PDEDim + CSTDim); ++j) {
            std::size_t k = i * (PDEDim + CSTDim) + j;
            f_i[i] += (1. - alpha)
                    * (a[k] * Vn_i[j]
                            + b[k]
                                    * (-Vn_3i[j] + 4.0 * Vn_2i[j]
                                            - 3.0 * Vn_i[j]))
                    / (2.0 * dx);
        }
    }
}

PDE_RobinBC_SO_R::PDE_RobinBC_SO_R(const std::size_t& PDEDim_,
        const std::size_t& CSTDim_, const double& dx_, const double alpha_)
    : Bdry(2)
    , dx(dx_)
    , PDEDim(PDEDim_)
    , CSTDim(CSTDim_)
    , SYSDim(PDEDim_ + CSTDim_)
    , alpha(alpha_)
    , a(PDEDim * SYSDim)
    , b(PDEDim * SYSDim)
    , c(PDEDim * SYSDim)
{
}

PDE_RobinBC_SO_R::PDE_RobinBC_SO_R(const std::size_t& PDEDim_p,
        const std::size_t& CSTDim_p, const double& dx_p,
        const std::vector<double>& a_p, const std::vector<double>& b_p,
        const std::vector<double>& c_p, const double alpha_)
    : PDE_RobinBC_SO_R(PDEDim_p, CSTDim_p, dx_p, alpha_)
{
    if (a_p.size() >= PDEDim * SYSDim) {
        for (std::size_t i = 0; i < PDEDim * SYSDim; ++i) {
            a[i] = a_p[i];
            b[i] = b_p[i];
            c[i] = c_p[i];
        }
    }
    else {
        for (std::size_t i = 0; i < PDEDim; ++i) {
            a[i * SYSDim + i] = a_p[i];
            b[i * SYSDim + i] = b_p[i];
            c[i * SYSDim + i] = c_p[i];
        }
    }
}

void
PDE_RobinBC_SO_R::SetBoundaryCondition(
        double x, double tPrev, double tCurr, std::size_t /* idx */)
{
    SetBoundaryValues(x, tCurr);
    for (std::size_t i = 0; i != PDEDim; ++i) {
        f_i[i] = alpha * c[i];
        for (std::size_t j = 0; j != (PDEDim + CSTDim); ++j) {
            std::size_t k = i * (PDEDim + CSTDim) + j;
            f_i[i] += alpha
                    * (a[k] * V_i[j]
                            + b[k] * (V_3i[j] - 4.0 * V_2i[j] + 3.0 * V_i[j]))
                    / (2.0 * dx);
        }
    }
    for (std::size_t i = 0; i != (PDEDim + CSTDim) * PDEDim; ++i) {
        Df_iDV_i[i] = alpha * (a[i] + 3.0 * b[i]) / (2.0 * dx);
        Df_iDV_2i[i] = -4.0 * alpha * b[i] / (2.0 * dx);
        Df_iDV_3i[i] = alpha * b[i] / (2.0 * dx);
    }

    SetBoundaryValues(x, tPrev);
    for (std::size_t i = 0; i != PDEDim; ++i) {
        f_i[i] = (1. - alpha) * c[i];
        for (std::size_t j = 0; j != (PDEDim + CSTDim); ++j) {
            std::size_t k = i * (PDEDim + CSTDim) + j;
            f_i[i] += (1. - alpha)
                    * (a[k] * V_i[j]
                            + b[k] * (V_3i[j] - 4.0 * V_2i[j] + 3.0 * V_i[j]))
                    / (2.0 * dx);
        }
    }
}

/*
 * Setting up for Pressure Robin BCs
 */
void
Pressure_RobinBC_FO_L::SetBoundaryCondition(
        double x, double /* tPrev */, double tCurr, std::size_t /* idx */)
{
    SetBoundaryValues(V_i, V_2i, x, tCurr);

    Df_iDV_i[0] = a - b / dx;
    Df_iDV_2i[0] = b / dx;
    f_i[0] = c;
}

void
Pressure_RobinBC_FO_R::SetBoundaryCondition(
        double x, double /* tPrev */, double tCurr, std::size_t /* idx */)
{
    SetBoundaryValues(V_i, V_2i, x, tCurr);

    Df_iDV_i[0] = a + b / dx;
    Df_iDV_2i[0] = -b / dx;
    f_i[0] = c;
}

void
Pressure_RobinBC_SO_L::SetBoundaryCondition(
        double x, double /* tPrev */, double tCurr, std::size_t /* idx */)
{
    SetBoundaryValues(V_i, V_2i, V_3i, x, tCurr);

    Df_iDV_i[0] = a - 1.5 * b / dx;
    Df_iDV_2i[0] = 2.0 * b / dx;
    Df_iDV_3i[0] = -0.5 * b / dx;
    f_i[0] = c;
}

void
Pressure_RobinBC_SO_R::SetBoundaryCondition(
        double x, double /* tPrev */, double tCurr, std::size_t /* idx */)
{
    SetBoundaryValues(V_i, V_2i, V_3i, x, tCurr);

    Df_iDV_i[0] = a + 1.5 * b / dx;
    Df_iDV_2i[0] = -2.0 * b / dx;
    Df_iDV_3i[0] = 0.5 * b / dx;
    f_i[0] = c;
}

/*
 * Setting up for Velocity BC when there is no pressure
 */
void
Velocity_WithoutPressure_L::SetBoundaryCondition(
        double x, double /* tPrev */, double tCurr, std::size_t /* idx */)
{
    SetBoundaryValues(V_i, V_2i, x, tCurr);

    f_i[0] = u_minus1;
}
