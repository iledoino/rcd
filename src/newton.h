#ifndef SRC_NEWTON_NEWTON_H
#define SRC_NEWTON_NEWTON_H


#include "dct.h"
#include <cmath>
#include <iostream>
#include <omp.h>

class Newton
{
public:
    /*
     * Variable that holds everything related to the PDE discretization
     * This variable MUST be defined outside this class
     */
    Discretization& dct;

    /*
     * Variables used and defined inside this class
     */
    const std::size_t maxNOIter, RHSArraySize;

    /*
     * Variables defined outside, but used inside this class
     */
    double *G, *Y, *V, *Vn;

    /*
     * Function that executes the time-stepping
     */
    int
    TimeStep(double const& dt, double const& t, double limitForGNorm = 1.0e-16,
            double limitForRelativeVNorm = 1.0e-16);

    /*
     * Function that calculates the infinity norm of an array
     */
    static double InfiniteNorm(double* array, std::size_t arraySize);

    /*
     * Constructor
     */
    Newton(Discretization& dct_, const std::size_t maxNOIter_,
            const std::vector<double>& step_percent_ = step_percent_default_)
        : dct(dct_)
        , maxNOIter(maxNOIter_)
        , RHSArraySize(dct.M->RHSArraySize)
        , G(dct.LHS)
        , Y(dct.RHS)
        , V(dct.V)
        , Vn(dct.Vn)
        , step_percent(step_percent_)
    {
    }

    /*
     * Function for identification
     */
    static std::string About()
    {
        return "***************************************************************"
               "*****************\n                      Newton Class for nxn "
               "RCD equations                       "
               "\n*************************************************************"
               "*******************\n\nCopyright: Ismael de Souza Ledoino "
               "<ismael.sledoino@gmail.com>\n**********************************"
               "**********************************************\n\n";
    }

private:
    static std::vector<double> step_percent_default_;

protected:
    const std::vector<double>& step_percent;
};

#endif /* SRC_NEWTON_NEWTON_H */
