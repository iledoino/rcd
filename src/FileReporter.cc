#include "FileReporter.h"
#include <fstream>
#include <iomanip>
#include <iostream>

FileReporter::FileReporter(
        std::string solution_file_name_p, std::size_t solution_frequency_p)
    : Reporter()
    , solution_file_name_(solution_file_name_p)
    , solution_frequency_(solution_frequency_p)
    , nvar(0)
    , nprint(0)
    , nsize(0)
    , solcounter(0)
{
}

void
FileReporter::start()
{
    solcounter = 0;
    std::ofstream reader("plotsol.m");

    reader << "%% name of the file that contains the solution            \n"
           << "arq_out = '" << solution_file_name_ << "';                \n"
           << "cfg_out = 'cfgsol.out';                                   \n"
           << "                                                          \n"
           << "%% reading of consfig data                                \n"
           << "fid = fopen(cfg_out, 'r');                                \n"
           << "nOfSolutions = fscanf(fid, '%e', 1);                      \n"
           << "nOfTimes     = fscanf(fid, '%e', 1);                      \n"
           << "NOPoints     = fscanf(fid, '%i', 1);                      \n"
           << "fclose(fid);                                              \n"
           << "                                                          \n"
           << "%% reading of data                                        \n"
           << "fid = fopen(arq_out, 'r');                                \n"
           << "rresults  = zeros(nOfTimes, nOfSolutions+1, NOPoints);    \n"
           << "times    = zeros(nOfTimes, 1);                            \n"
           << "for i=1:nOfTimes                                          \n"
           << "    times(i)           = fscanf(fid, '%e', 1);            \n"
           << "    A = fscanf(fid, '%e', [(nOfSolutions+1) NOPoints]);   \n"
           << "    results(i, : , : ) = A(: , :);                        \n"
           << "end;                                                      \n"
           << "                                                          \n"
           << "x = zeros(NOPoints, 1);                                   \n"
           << "u = zeros(NOPoints, 1);                                   \n"
           << "x(:) = results(1,1,:);                                    \n"
           << "for j=2:nOfSolutions+1                                    \n"
           << "    for i=1:nOfTimes                                      \n"
           << "        figure(j-1);                                      \n"
           << "        u(:) = results(i,j,:);                            \n"
           << "        plot(x, u);                                       \n"
           << "        pause(0.01);                                      \n"
           << "    end;                                                  \n"
           << "end;                                                      \n"
           << "fclose(fid);                                              \n"
           << std::endl;
    reader.close();

    std::ofstream data(solution_file_name_);
    data << " ";
    data.close();
    nprint = 0;
}

bool
FileReporter::timetomake()
{
    bool timetomake_p = (solcounter++) % solution_frequency_ == 0;
    if (timetomake_p)
        solcounter = 1;
    return timetomake_p;
}

void
FileReporter::make(const std::vector<std::vector<double>>& solution,
        const std::vector<double>& spcMesh, double time, bool /* makeanyway */)
{
    std::cout << "Printing solution to file at (x, t) = (:," << time << ")\n";
    nvar = solution.size();
    nsize = spcMesh.size();

    std::ofstream data(solution_file_name_, std::fstream::app);
    data << std::scientific << std::setprecision(16) << time << std::endl;
    for (std::size_t i = 0; i < spcMesh.size(); ++i) {
        data << spcMesh[i] << " ";
        for (std::size_t j = 0; j < solution.size(); ++j) {
            data << solution[j].at(i) << " ";
        }
        data << std::endl;
    }
    data.close();

    nprint++;
}

void
FileReporter::finish()
{
    std::ofstream cfg("cfgsol.out");
    cfg << nvar << std::endl;
    cfg << nprint << std::endl;
    cfg << nsize << std::endl;
    cfg.close();
}
