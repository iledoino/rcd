#ifndef SRC_INTERFACE_MULTIDREPORTER_H
#define SRC_INTERFACE_MULTIDREPORTER_H


#ifdef ENABLE_MULTID

#include "PlotReporter.h"
#include <Plot2d.h>
#include <Proj2dMap.h>
#include <SourceScene.h>

class MultidReporter : public PlotReporter
{
public:
    MultidReporter(int* pargc, char** argv, std::string spaceName,
            std::size_t nOfVariables);
    MultidReporter(int* pargc, char** argv, std::string spaceName,
            std::size_t nOfVariables,
            std::vector<std::vector<std::size_t>> reportGroups_p,
            std::string xAxisName_p, std::vector<std::string> yAxesNames_p)
        : MultidReporter(pargc, argv, spaceName, nOfVariables)
    {
        reportGroups_ = reportGroups_p;
        xAxisName_ = xAxisName_p;
        yAxesNames_ = yAxesNames_p;
    }
    virtual ~MultidReporter() noexcept = default;

    void start() override;
    void make(const std::vector<std::vector<double>>& solution,
            const std::vector<double>& spcMesh, double time,
            bool makeanyway = false) override;
    void finish() override{};

    const std::vector<std::unique_ptr<multid::Plot2d>>& plot2ds() const
    {
        return plot2ds_;
    }
    std::vector<std::unique_ptr<multid::Plot2d>>& plot2ds() { return plot2ds_; }

private:
    multid::Space space_;
    multid::SourceScene sourceScene_;

    std::vector<std::unique_ptr<multid::Space>> spaces_{};
    std::vector<std::unique_ptr<multid::Proj2dMap>> proj2dMaps_{};
    std::vector<std::unique_ptr<multid::Plot2d>> plot2ds_{};
};

#endif /* ENABLE_MULTID */

#endif /* SRC_INTERFACE_MULTIDREPORTER_H */
