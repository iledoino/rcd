#include "newton.h"

std::vector<double> Newton::step_percent_default_ = std::vector<double>(1, 1.);

/*
 * Newton does the advance in time
 * OBS: Newton assumes that V0 is stored in V, and that V0 = V^{j}
 */
int
Newton::TimeStep(double const& dt, double const& t, double limitForGNorm,
        double limitForRelativeVNorm)
{
    std::size_t totalNOVariables = dct.PDEDim() + dct.cstDim();
    if (&step_percent == &step_percent_default_
            && step_percent_default_.size() != totalNOVariables) {
        step_percent_default_.resize(totalNOVariables);
        for (auto& percent : step_percent_default_)
            percent = 1.;
    }

    /*
     * =================================================================
     *  Scope for solving the Newton Method associated to the PDE
     *
     * ∂/∂t G(V) + ∂/∂x F(V) = ∂/∂x( B(V) ∂V/∂x ) + R(V)
     *                  H(V) = 0
     *
     * =================================================================
     */
    double infNormG0 = 1.;
    std::size_t nwtIter = 0;
    bool VFound = false;
    int PDELSResult;

    /*
     * Setting up the new previousV
     */
    for (std::size_t i = 0; i != RHSArraySize; ++i)
        Vn[i] = V[i];
    /*
     * Letting Discretization class know what is the current dt value
     */
    dct.DT(dt);
    /*
     * The vector Y is calculated. This vector won't change until the
     * next call of Newton
     */
    dct.SetRHS(t);
    /*
     * The following scope is executed while the aproximation to the PDE
     * solution has not be found, or the linear linear system has been
     * solved succesfully
     */
    while (!VFound) {
        /*
         * The vector G(V^{j+1}) = F(V^{j+1}) - Y is partially calculated.
         * (G(V^{j+1}) = F(V^{j+1}) for V^{j+1}=V_0)
         * The jacobian matrix of G is calulated.
         *
         * OBS: from now on, V represents either the solution from
         * the previous iteration or the actual solution, it depends
         * on its place in this scope (while scope)
         */
        dct.SetJacAndLHS(t, t + dt);
        /*
         * The vector G(V) = RHS - LHS is calculated.
         */
        for (std::size_t i = 0; i != RHSArraySize; ++i)
            G[i] -= Y[i];
        /*
         * Checks the norm toleration for G
         */
        if (nwtIter != 0)
            VFound = (Newton::InfiniteNorm(G, RHSArraySize) / infNormG0)
                            > limitForGNorm ?
                    false :
                    true;
        else {
            /*
             * If || G(V_0) || _{inf} > epsilon then it means that V_0 is not
             * a good aproximation for V^{j+1}, therefore the newton method must
             * be applied
             */
            infNormG0 = Newton::InfiniteNorm(G, RHSArraySize);
            VFound = infNormG0 > limitForGNorm ? false : true;
        }
        /*
         * Solves the linear system
         */
        if (!VFound) {
            /*
             * Solves the linear system
             * \frac{\partial G}{\partial V} * \lambda V = G
             * OBS: The solution \lambda V is stored in G
             */
            PDELSResult = dct.Solve_PDE();
            if (PDELSResult != 1) {
                std::cerr << "\tNewton Method has not converged\n";
                return 0;
            }
            /*
             * Check the convergence by the condition
             * || V^{i+1}-V^{i} ||_{inf} / || V^{i} ||_{inf} < epsilon
             */
            VFound = (Newton::InfiniteNorm(G, RHSArraySize)
                             / Newton::InfiniteNorm(V, RHSArraySize))
                            > limitForRelativeVNorm ?
                    false :
                    true;
            /*
             * The solution for the actual iteration is calculated
             */
            for (std::size_t i = 0; i != RHSArraySize; i += totalNOVariables)
                for (std::size_t jj = i, j = 0; j != totalNOVariables;
                        ++j, ++jj)
                    V[jj] = V[jj] - G[jj] * step_percent[j];
        }
        /*
         * Checks if the maximum number of iterations was reached
         */
        nwtIter++;
        if (!VFound)
            if (nwtIter > maxNOIter) {
#if 0
                // NOTE: this variable is never used
                VFound = true;
#endif
                return -1;
            }
    }

    /*
     * =================================================================
     *  Scope for solving the linear system associated to the pressure
     * equation, given by the expression
     *
     *                  ∇ . ( - k(V) ∇p ) = q(p)
     *
     * At this point, V is known, so that the k function above is simply
     * a parameter to the system. This means that there is no need to run
     * the Newton Method
     * =================================================================
     */
    if (dct.hasPressure) {
        int PressureLSResult;

        /*
         * Fills up the matrix and its RHS
         */
        dct.SetPressureMatrix(t, t + dt);

        /*
         * Solves the Linear System
         */
        PressureLSResult = dct.Solve_Pressure();

        if (PressureLSResult != 1) {
            std::cerr << "\tPressure System has not been solved\n";
            return 0;
        }
    }

    /*
     * =================================================================
     *  Scope for calculating the velocity, given by the expression
     *
     *                      u = - k(V) ∇p
     *
     * At this point, all the variables, except u are known. Therefore,
     * the above expression is explicit in u
     * =================================================================
     */
    if (dct.hasVelocity)
        (dct.*dct.Calculate_Velocity)(t, t + dt);
    return 1;
}

/*
 * Calculates the infinite norm of an array
 */
double
Newton::InfiniteNorm(double* array, std::size_t arraySize)
{
    auto infNorm = fabs(array[0]);
    double tmp;

    for (std::size_t i = 1; i != arraySize; i++) {
        tmp = fabs(array[i]);
        if (tmp > infNorm)
            infNorm = tmp;
    }

    return infNorm;
}
