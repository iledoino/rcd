#ifndef SRC_DCT_DCT_H
#define SRC_DCT_DCT_H


#include "PDEMember.h"
#include "bdry.h"
#include <blots.h>
#include <cmath>
#include <iostream>
#include <string>
#include <vector>

#ifdef ENABLE_OPENMP
#include <omp.h>
//#define COMPUTE_PDE_LINEAR_SYSTEM_SOLVER_TIME
//#define PRINT_LS_MATLAB
#endif /* ENABLE_OPENMP */

/*
 * DESCRIPTION: This class deals with everything related to the discretization
 * of the PDE.
 *
 * WHAT IT DOES: It provides a way to set up the matrix and right hand side
 * of the linear system solved on newton method. Furthermore, it deals with
 * different boundary conditions and they affect the linear system. This class
 * is useless without Newton class
 */
class Discretization
{
public:
    /*
     * Constructors
     */
    Discretization(PhysicsRCD& phy_, std::vector<Bdry*> bdry_,
            const std::size_t& PDEDimension,
            const std::size_t& ConstraintDimension,
            const std::size_t& nOfSpaceMeshPoints, const double& dx_,
            bool hasPressure_ = false, bool hasVelocity_ = false);

    virtual void setUpPDEMembers(std::vector<PDEMember*> pdeMembers);
    virtual void setUpPDEMembers(const PDEMemberConfig& pdemc);

    PhysicsRCD& physicsRCD() { return phy; }
    std::size_t PDEDim() const { return PDEDimension; }
    std::size_t cstDim() const { return ConstraintDimension; }
    std::size_t nSpcMeshPts() const { return nOfSpaceMeshPoints; }
    double spcMeshStpSize() const { return dx; }
    double& spcMeshStpSize() { return dx; }
    bool hasPress() const { return hasPressure; }
    bool hasVel() const { return hasVelocity; }

    /*
     * Destructor
     */
    virtual ~Discretization();

private:
    Discretization(const Discretization&) = delete;
    Discretization& operator=(const Discretization&) = delete;

    friend class Newton;

    /*
     * PDEMembersAllocatedInside --> obviously, this variable states
     * whether the PDEMembers were allocated inside or not. This is
     * necessary in case a user wants to create its own PDEMember child
     * class
     *
     * hasPressure --> indicates if there is a pressure equation that
     * has to be splitted from the original system of balance equations
     *
     * hasVelocity --> indicates if there is a velocity equation that
     * has to be splitted from the original system of balance equations
     */
    bool PDEMembersAllocatedInside;
    const std::size_t PDEDimension;
    const std::size_t ConstraintDimension;
    const std::size_t nOfSpaceMeshPoints;
    const bool hasPressure, hasVelocity;

    /*
     * This function allocates data that is evenly allocate for every
     * class of this type, no matter if it is a child or the parent class
     */
    void AllocateVirtualData();

public:
    /*
     * x --> holds the space points
     *
     * V --> holds the unknowns of the PDE system, except for pressure
     * and velocity
     *
     * p --> holds the pressure unknown
     *
     * u --> holds the velocity unknown
     */
    double *x, *V, *p, *u;

    /*
     * This function makes the value of dt consistent with all
     * PhysicsDataHolder class instances inside this class
     */
    virtual void DT(const double& dt);

    /*
     * Function that calculates pressure and velocity at time 0, when
     * they are part of the system of PDEs
     */
    int SetInitialPressureAndVelocity();

protected:
    /*
     * RHS --> see class PDEMember for details of what this array is
     *
     * dV --> holds V_{i+1}-V_i
     *
     * Vn --> holds the unknowns at a known time. Fox example, when
     * attempting to calculate V at a time t_{n+1}: in this case, V
     * at time t_n is known, so Vn holds V evalluated at time t_n
     *
     * k --> array used only for pressure and velocity equations
     *
     * DummyMemory --> name tells it s purpose. Despite the name, this
     * memmory is necessary for some functions
     */
    double *RHS, *dV, *Vn, *k, *DummyMemory;

    /*
     * LHS --> see class PDEMember for details of what this array is
     *
     * dx --> constant that holds the distance of two consecutive points
     * in space
     */
    double* LHS;
    double dx;

    /*
     * For details about these classes, have a look to their related
     * header files
     */
    std::vector<PDEMember*> pdeMembers;
    PhysicsRCD& phy;
    PhysicsDataHolder* phyDH;
    std::vector<Bdry*> bdry;
    NUBTS* M;
    NUBTS* A;

    /*
     * This funtion solves the linear system
     *
     * JG( G(V^{n+1}) ) _{l+1} deltaV = G(V^{n+1}) _{l}
     */
    virtual int Solve_PDE()
    {
#ifdef COMPUTE_PDE_LINEAR_SYSTEM_SOLVER_TIME
        double start_ = omp_get_wtime();
        int return_value = M->solve();
        double end_ = omp_get_wtime();
        std::cerr << "Discretization::Solve_PDE() time: " << end_ - start_
                  << std::endl;
        return return_value;
#else
        return M->solve();
#endif
    }
    /*
     * This function calculates the array dV
     */
    virtual void CalculatedV();

    /*
     * Calculates Y(V^{n})
     */
    virtual void SetRHS(const double& t);

    /*
     * Insert boundary conditions for  Y(V^{n})
     */
    void SetBC_RHS(const double& t);

    /*
     * Calculates both F(V^{n+1}) and JG( G(V^{n+1}) )
     */
    virtual void SetJacAndLHS(const double& tPrev, const double& tCurr);

    /*
     * Insert boundary conditions for F(V^{n+1}) and JG( G(V^{n+1}) )
     */
    void SetBC_JacAndLHS(const double& tPrev, const double& tCurr);

    /*
     * Functions used to fill up the matrix used to solve the linear
     * system for pressure
     */
    virtual void SetPressureMatrix(const double& tPrev, const double& tCurr);

    int Solve_Pressure() { return A->solve(); }
    void SetBC_Pressure(const double& tPrev, const double& tCurr);

    /*
     * Function used to solve for Velocity
     */
    typedef void (Discretization::*VelocityEquation)(
            const double&, const double&);
    VelocityEquation Calculate_Velocity;
    void
    Calculate_Velocity_withPressure(const double& tPrev, const double& tCurr);
    void Calculate_Velocity_withoutPressure(
            const double& tPrev, const double& tCurr);
};

/*
 * DESCRIPTION: Auxiliar class for DiscretizationParallelized class
 *
 * WHAT IT DOES: Provides a set of indexes for parallelization
 */
class AssemblyIndices
{
public:
    std::size_t iStart, iEnd;
    std::size_t iPG, iCG, iNG, iPJG, iCJG, iNJG, iPRJG, iCRJG;

    void setIndices(const std::size_t& iStart_, const std::size_t& iEnd_,
            const std::size_t& sqrBcksDim, const std::size_t& rectBcksDim);
};

/*
 * DESCRIPTION: Discretization class with parallelization features
 *
 * WHAT IT DOES: Same as Discretization class
 */
class DiscretizationParallelized : public Discretization
{
public:
    /*
     * Constructors
     */
    DiscretizationParallelized(PhysicsRCD& phy_, std::vector<Bdry*> bdry_,
            const std::size_t& PDEDimension,
            const std::size_t& ConstraintDimension,
            const std::size_t& nOfSpaceMeshPoints, const double& dx_,
            bool hasPressure_ = false, bool hasVelocity_ = false);

    void setUpPDEMembers(std::vector<PDEMember*> pdeMembers) override;
    void setUpPDEMembers(const PDEMemberConfig& pdemc) override;

    /*
     * Destructor
     */
    ~DiscretizationParallelized();

private:
    DiscretizationParallelized(const DiscretizationParallelized&) = delete;
    DiscretizationParallelized& operator=(const DiscretizationParallelized&)
            = delete;

    /*
     * Number of threads that will be used while executing the internal
     * routines
     */
    static std::size_t nThreads;
    static std::size_t nPartitions;
    /*
     * array of indexes
     */
    AssemblyIndices* indx;
    /*
     * Function that allocates PhysicsDataHolder instances for each
     * one of the threads, and other parallelization data
     */
    void AllocateParallelData();

    /*
     * Conquer and Divide Method, applicable depending on the kind
     * of boundaries
     */
    CDMBTS* N;
    CDMNUBTS* NUN;

    /*
     * Constant that specifies whether Conquer and Divide Method is
     * applicable or not
     */
    bool CDMBTS_is_applicable;
    bool CDMNUBTS_is_applicable;

    /*
     * Same as the aforementioned funtions
     */
    int Solve_PDE() override
    {
#ifdef COMPUTE_PDE_LINEAR_SYSTEM_SOLVER_TIME
        double start_ = omp_get_wtime();
        int return_value;
        if (CDMBTS_is_applicable)
            return_value = N->solve();
        else {
            if (CDMNUBTS_is_applicable)
                return_value = NUN->solve();
            else
                return_value = M->solve();
        }
        double end_ = omp_get_wtime();
        std::cerr << "DiscretizationParallelized::Solve_PDE() time: "
                  << end_ - start_ << std::endl;
        return return_value;
#else
        if (CDMBTS_is_applicable)
            return N->solve();
        else {
            if (CDMNUBTS_is_applicable)
                return NUN->solve();
            else
                return M->solve();
        }
#endif
    }

    void DT(const double& dt) override;
    void CalculatedV() override;
    void SetRHS(const double& t) override;
    void SetJacAndLHS(const double& tPrev, const double& tCurr) override;
};

#endif /* SRC_DCT_DCT_H */
