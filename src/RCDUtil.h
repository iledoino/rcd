#ifndef SRC_DERIVTEST_DERIVTEST_H
#define SRC_DERIVTEST_DERIVTEST_H

#include "dct.h"
#include "physicsRCD.h"
#include <fstream>
#include <iomanip>
#include <limits>
#include <vector>

void derivtest(PhysicsRCD& phys, const std::vector<double>& leftState_,
        const std::vector<double>& rightState_, const std::size_t& PDEDim,
        const std::size_t& CSTDim, bool convTestRatio = false);

void smoothJumpSolution(double* V, double* x, std::size_t XSIZE, double XJUMP,
        double XJUMPSIZE, const std::size_t& PDEDim_,
        const std::size_t& CstDim_, const std::vector<double>& leftState_,
        const std::vector<double>& rightState_,
        std::string filesolutionname = "startsol.out");

void convertDctSolution(Discretization& dct_,
        std::vector<std::vector<double>>& sol_, std::vector<double>& x_);

#endif /* SRC_DERIVTEST_DERIVTEST_H */
