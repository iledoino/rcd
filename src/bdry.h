#ifndef SRC_BDRY_BDRY_H
#define SRC_BDRY_BDRY_H

#include "PDEMember.h"
#include "physicsRCD.h"
#include <cmath>
#include <iostream>
#include <vector>

/*
 * functions for boundary conditions, defined by the user
 */

/*
 * This class provides a model which the user should complement
 * for its own boundary conditions
 * This is an abstract class
 */
class Bdry
{
private:
    Bdry(const Bdry&) = delete;
    Bdry& operator=(const Bdry&) = delete;

    friend class Discretization;
    friend class DiscretizationParallelized;

protected:
    /*
     * V_i, V_2i, and V_3i represent the variables which the
     * boundary functions depend on
     */
    double *V_i, *V_2i, *V_3i;
    double *Vn_i, *Vn_2i, *Vn_3i;
    /*
     * f_i is the boundary funtion, which is intended to be zero.
     * Its derivatives are the square and rectangular blocks pointed
     * by Df_iDV_i, Df_iDV_2i, and Df_iDV_3i (in case this last
     * one exists). The user must take care of respecting the memory
     * allocated to the variables referenced by them
     */
    double *f_i, *Df_iDV_i, *Df_iDV_2i, *Df_iDV_3i;
    /*
     * Functions to be used only by Discretization class, acording to the type
     *of boundary condition
     */
    void setPointers(double* V_i_, double* V_2i_, double* Vn_i_, double* Vn_2i_,
            double* f_i_, double* Df_iDV_i_, double* Df_iDV_2i_);
    void setPointers(double* V_i_, double* V_2i_, double* V_3i_, double* Vn_i_,
            double* Vn_2i_, double* Vn_3i_, double* f_i_, double* Df_iDV_i_,
            double* Df_iDV_2i_, double* Df_iDV_3i_);
    /*
     * The pdeMembers_ vector store PDEMember pointers, where PDEMember
     * is a class that implements discretization formulas for the terms
     * of a PDE
     */
    std::vector<PDEMember*> pdeMembers_;
    /*
     * This constant requires one of the following values: 0, 1, or 2.
     * These correspond to the following kind of BCs:
     *
     * - 0 (default) --> BCs are periodic, or V(x_L, t) = V(x_R, t)
     *     - OBS: For this case, no work is necessary to be done by
     *       the function SetBoundaryValues. However, it is still
     *       necessary to create the function.
     *
     * - 1 --> BCs depend on two variables, as follows;
     *         - left BC:  f0 = 0, where f0 depends on V0 and V1
     *         - right BC: fM = 0, where fM depends on VMminus1 and VM
     *
     * - 2 --> BCs depend on three variables, as follows;
     *         - left BC:  f0 = 0, where f0 depends on V0, V1 and V2
     *         - right BC: fM = 0, where fM depends on VMminus2, VMminus1 and VM
     *
     * - 3 --> Value exclusively used for right BC. In this case there
     *         is no boundary condition (for upwind method)
     * Table:
     *     V0 = V( x_L, t ), V1 = V( x_L+dx, t ), V2 = V( x_L+2dx, t )
     *     VMminus2 = V( x_R-2dx, t ), VMminus1 = V( x_R-dx, t ), VM = V( x_R, t
     *)
     */
    const int BCtype;
    /*
     * This function must be implemented by the user, and is
     * responsable for filling up the boundary function values
     * and boundary derivative values
     *
     * WARNING: The pointers in this class point directly to the
     * matrix and its RHS. Therefore, it is extremely important
     * to fill up the non-zero values with 0.
     */
    virtual void
    SetBoundaryCondition(double x, double tPrev, double tCurr, std::size_t idx)
            = 0;

public:
    /*
     * Constructors
     */
    Bdry(const int& BCtype_ = 0)
        : V_i()
        , V_2i()
        , V_3i()
        , Vn_i()
        , Vn_2i()
        , Vn_3i()
        , f_i()
        , Df_iDV_i()
        , Df_iDV_2i()
        , Df_iDV_3i()
        , pdeMembers_()
        , BCtype(BCtype_)
    {
    }
    /*
     * Destructor
     */
    virtual ~Bdry();
};

class PrescribedFlux_L : public Bdry
{
private:
    PrescribedFlux_L(const PrescribedFlux_L&) = delete;
    PrescribedFlux_L& operator=(const PrescribedFlux_L&) = delete;

    void SetBoundaryCondition(
            double x, double tPrev, double tCurr, std::size_t idx) override;

protected:
    const std::size_t PDEDim, CSTDim, SYSDim;
    PhysicsRCD& physicsRCD_;
    std::vector<double> Feval_;
    double dx_;
    double pDirichlet_, uDirichlet_;

public:
    PrescribedFlux_L(const std::size_t& PDEDim_, const std::size_t& CSTDim_,
            PhysicsRCD& physicsRCD_p, std::vector<double> Feval_p, double dx_p,
            double pDirichlet_p = 0.0, double uDirichlet_p = 0.0);
    virtual ~PrescribedFlux_L() {}
};

class PrescribedFlux_R : public Bdry
{
private:
    PrescribedFlux_R(const PrescribedFlux_R&) = delete;
    PrescribedFlux_R& operator=(const PrescribedFlux_R&) = delete;

    void SetBoundaryCondition(
            double x, double tPrev, double tCurr, std::size_t idx) override;

protected:
    const std::size_t PDEDim, CSTDim, SYSDim;
    PhysicsRCD& physicsRCD_;
    std::vector<double> Feval_;
    double dx_;
    double pDirichlet_, uDirichlet_;

public:
    PrescribedFlux_R(const std::size_t& PDEDim_, const std::size_t& CSTDim_,
            PhysicsRCD& physicsRCD_p, std::vector<double> Feval_p, double dx_p,
            double pDirichlet_p = 0.0, double uDirichlet_p = 0.0);
    virtual ~PrescribedFlux_R() {}
};

/*
 * This class is to provide a means to use the following BC
 *
 * f = a*u + b*u_x + c = 0
 *
 * a, b and c depend on x and t, and are set every call of
 * SetBoundaryCondition. Notice that this class is used for setting up
 * the left boundary condition for a system of PDEs, where the variables
 * are u_1,u_2,...,u_N. Let the above u mean u_i, for i=1,2,...,N.
 * The order of error here is O(dx)
 */
class PDE_RobinBC_FO_L : public Bdry
{
private:
    PDE_RobinBC_FO_L(const PDE_RobinBC_FO_L&) = delete;
    PDE_RobinBC_FO_L& operator=(const PDE_RobinBC_FO_L&) = delete;

    /*
     * Overriden funtion from base class
     */
    void SetBoundaryCondition(
            double x, double tPrev, double tCurr, std::size_t idx) override;

protected:
    /*
     * difference of one point in space from the one right after it
     */
    const double dx;
    const std::size_t PDEDim, CSTDim, SYSDim;
    const double alpha;

    /*
     * Parameters used in this BC
     */
    std::vector<double> a, b, c;
    /*
     * This function MUST be defined by the user, once it sets the
     * values for a, b and c every call of setBoundaryCondition
     */
    virtual void SetBoundaryValues(double /* x */, double /* t */) {}

    PDE_RobinBC_FO_L(const std::size_t& PDEDim_, const std::size_t& CSTDim_,
            const double& dx_, const double alpha_ = 1.0);

public:
    PDE_RobinBC_FO_L(const std::size_t& PDEDim_, const std::size_t& CSTDim_,
            const double& dx_, const std::vector<double>& a,
            const std::vector<double>& b, const std::vector<double>& c,
            const double alpha_ = 1.0);
};

/*
 * Similar to the above class, but for right boundary
 */
class PDE_RobinBC_FO_R : public Bdry
{

private:
    PDE_RobinBC_FO_R(const PDE_RobinBC_FO_R&) = delete;
    PDE_RobinBC_FO_R& operator=(const PDE_RobinBC_FO_R&) = delete;

    /*
     * Overriden funtion from base class
     */
    void SetBoundaryCondition(
            double x, double tPrev, double tCurr, std::size_t idx) override;

protected:
    /*
     * difference of one point in space from the one right after it
     */
    const double dx;
    const std::size_t PDEDim, CSTDim, SYSDim;
    const double alpha;

    /*
     * Parameters used in this BC
     */
    std::vector<double> a, b, c;
    /*
     * This function MUST be defined by the user, once it sets the
     * values for a, b and c every call of setBoundaryCondition
     */
    virtual void SetBoundaryValues(double /* x */, double /* t */) {}

    PDE_RobinBC_FO_R(const std::size_t& PDEDim_, const std::size_t& CSTDim_,
            const double& dx_, const double alpha_ = 1.0);

public:
    PDE_RobinBC_FO_R(const std::size_t& PDEDim_, const std::size_t& CSTDim_,
            const double& dx_, const std::vector<double>& a,
            const std::vector<double>& b, const std::vector<double>& c,
            const double alpha_ = 1.0);
};

/*
 * This class is to provide a means to use the following BC
 *
 * f = a*u + b*u_x + c = 0
 *
 * a, b and c depend on x and t, and are set every call of
 * SetBoundaryCondition. Notice that this class is used for setting up
 * the left boundary condition for a system of PDEs, where the variables
 * are u_1,u_2,...,u_N. Let the above u mean u_i, for i=1,2,...,N.
 * The order of error here is O(dx^2). This kind of BC makes the linear
 * solver slower, once it introduces two more non-zero blocks, which
 * change the structure of the block-tridiagonal matrix use to time-step
 * through Newton method
 * The order of error here is O(dx^2)
 */
class PDE_RobinBC_SO_L : public Bdry
{
private:
    PDE_RobinBC_SO_L(const PDE_RobinBC_SO_L&) = delete;
    PDE_RobinBC_SO_L& operator=(const PDE_RobinBC_SO_L&) = delete;

    /*
     * Overriden funtion from base class
     */
    void SetBoundaryCondition(
            double x, double tPrev, double tCurr, std::size_t idx) override;

protected:
    /*
     * difference of one point in space from the one right after it
     */
    const double dx;
    const std::size_t PDEDim, CSTDim, SYSDim;
    const double alpha;

    /*
     * Parameters used in this BC
     */
    std::vector<double> a, b, c;
    /*
     * This function MUST be defined by the user, once it sets the
     * values for a, b and c every call of setBoundaryCondition
     */
    virtual void SetBoundaryValues(double /* x */, double /* t */) {}

    PDE_RobinBC_SO_L(const std::size_t& PDEDim_, const std::size_t& CSTDim_,
            const double& dx_, const double alpha_ = 1.0);

public:
    PDE_RobinBC_SO_L(const std::size_t& PDEDim_, const std::size_t& CSTDim_,
            const double& dx_, const std::vector<double>& a,
            const std::vector<double>& b, const std::vector<double>& c,
            const double alpha_ = 1.0);
};

/*
 * Similar to the above class, but for right boundary
 */
class PDE_RobinBC_SO_R : public Bdry
{
private:
    PDE_RobinBC_SO_R(const PDE_RobinBC_SO_R&) = delete;
    PDE_RobinBC_SO_R& operator=(const PDE_RobinBC_SO_R&) = delete;

    /*
     * Overriden funtion from base class
     */
    void SetBoundaryCondition(
            double x, double tPrev, double tCurr, std::size_t idx) override;

protected:
    /*
     * difference of one point in space from the one right after it
     */
    const double dx;
    const std::size_t PDEDim, CSTDim, SYSDim;
    const double alpha;

    /*
     * Parameters used in this BC
     */
    std::vector<double> a, b, c;
    /*
     * This function MUST be defined by the user, once it sets the
     * values for a, b and c every call of setBoundaryCondition
     */
    virtual void SetBoundaryValues(double /* x */, double /* t */) {}

    PDE_RobinBC_SO_R(const std::size_t& PDEDim_, const std::size_t& CSTDim_,
            const double& dx_, const double alpha_ = 1.0);

public:
    PDE_RobinBC_SO_R(const std::size_t& PDEDim_, const std::size_t& CSTDim_,
            const double& dx_, const std::vector<double>& a,
            const std::vector<double>& b, const std::vector<double>& c,
            const double alpha_ = 1.0);
};

/*
 * This class is to be used when there is no boundary condition. For
 * example, for upwind methods, there is no right boundary
 */
class NoBoundaryCondition : public Bdry
{
public:
    NoBoundaryCondition() : Bdry(3) {}

    void SetBoundaryCondition(double /* x */, double /* tPrev */,
            double /* tCurr */, std::size_t /* idx */) override
    {
    }
};

/*
 * This class is to be used when the boundary condition is periodic, that
 * is, when V[xL] = V[xR] for all t
 */
class PeriodicBoundaryCondition : public Bdry
{
public:
    PeriodicBoundaryCondition() : Bdry(0) {}

    void SetBoundaryCondition(double /* x */, double /* tPrev */,
            double /* tCurr */, std::size_t /* idx */) override
    {
    }
};

/*
 * This class is to provide a means to use the following BC
 *
 * a*p + b*p_x = c
 *
 * a, b and c depend on V, x and t
 * The order of error here is O(dx)
 */
class Pressure_RobinBC_FO_L : public Bdry
{

private:
    /*
     * Overriden funtion from base class
     */
    void SetBoundaryCondition(
            double x, double tPrev, double tCurr, std::size_t idx) override;

    /*
     * difference of one point in space from the one right after it
     */
    const double dx;

protected:
    /*
     * Parameters used in this BC
     */
    double a, b, c;
    /*
     * This function MUST be defined by the user, once it sets the
     * values for a, b and c every call of setBoundaryCondition
     */
    virtual void SetBoundaryValues(
            double* /* V0 */, double* /* V1 */, double /* x */, double /* t */)
    {
    }

    Pressure_RobinBC_FO_L(const double& dx_)
        : Bdry(1), dx(dx_), a(1.), b(0.), c(0.)
    {
    }

public:
    Pressure_RobinBC_FO_L(const double& dx_p, const double a_p,
            const double b_p, const double c_p)
        : Pressure_RobinBC_FO_L(dx_p)
    {
        a = a_p;
        b = b_p;
        c = c_p;
    }
    virtual ~Pressure_RobinBC_FO_L() = default;
};

/*
 * Similar to the above class, but for right boundary
 */
class Pressure_RobinBC_FO_R : public Bdry
{
private:
    /*
     * Overriden funtion from base class
     */
    void SetBoundaryCondition(
            double x, double tPrev, double tCurr, std::size_t idx) override;

    /*
     * difference of one point in space from the one right after it
     */
    const double dx;

protected:
    /*
     * Parameters used in this BC
     */
    double a, b, c;
    /*
     * This function MUST be defined by the user, once it sets the
     * values for a, b and c every call of setBoundaryCondition
     */
    virtual void SetBoundaryValues(double* /* VM */, double* /* VMminus1 */,
            double /* x */, double /* t */)
    {
    }

    Pressure_RobinBC_FO_R(const double& dx_)
        : Bdry(1), dx(dx_), a(1.), b(0.), c(0.)
    {
    }

public:
    Pressure_RobinBC_FO_R(const double& dx_p, const double a_p,
            const double b_p, const double c_p)
        : Pressure_RobinBC_FO_R(dx_p)
    {
        a = a_p;
        b = b_p;
        c = c_p;
    }
    virtual ~Pressure_RobinBC_FO_R() = default;
};

/*
 * This class is to provide a means to use the following BC
 *
 * a*p + b*p_x = c
 *
 * a, b and c depend on V, x and t. This kind of BC makes the linear
 * solver slower, once it introduces two more non-zero blocks, which
 * change the structure of the block-tridiagonal matrix use to time-step
 * through Newton method
 * The order of error here is O(dx^2)
 */
class Pressure_RobinBC_SO_L : public Bdry
{
private:
    /*
     * Overriden funtion from base class
     */
    void SetBoundaryCondition(
            double x, double tPrev, double tCurr, std::size_t idx) override;

    /*
     * difference of one point in space from the one right after it
     */
    const double dx;

protected:
    /*
     * Parameters used in this BC
     */
    double a, b, c;
    /*
     * This function MUST be defined by the user, once it sets the
     * values for a, b and c every call of setBoundaryCondition
     */
    virtual void SetBoundaryValues(double* /* V0 */, double* /* V1 */,
            double* /* V2 */, double /* x */, double /* t */)
    {
    }

    Pressure_RobinBC_SO_L(const double& dx_)
        : Bdry(2), dx(dx_), a(1.), b(0.), c(0.)
    {
    }

public:
    Pressure_RobinBC_SO_L(const double& dx_p, const double a_p,
            const double b_p, const double c_p)
        : Pressure_RobinBC_SO_L(dx_p)
    {
        a = a_p;
        b = b_p;
        c = c_p;
    }
    virtual ~Pressure_RobinBC_SO_L() = default;
};

/*
 * Similar to the above class, but for right boundary
 */
class Pressure_RobinBC_SO_R : public Bdry
{
private:
    /*
     * Overriden funtion from base class
     */
    void SetBoundaryCondition(
            double x, double tPrev, double tCurr, std::size_t idx) override;

    /*
     * difference of one point in space from the one right after it
     */
    const double dx;

protected:
    /*
     * Parameters used in this BC
     */
    double a, b, c;
    /*
     * This function MUST be defined by the user, once it sets the
     * values for a, b and c every call of setBoundaryCondition
     */
    virtual void SetBoundaryValues(double* /* VM */, double* /* VMminus1 */,
            double* /* VMminus2 */, double /* x */, double /* t */)
    {
    }

    Pressure_RobinBC_SO_R(const double& dx_)
        : Bdry(2), dx(dx_), a(1.), b(0.), c(0.)
    {
    }

public:
    Pressure_RobinBC_SO_R(const double& dx_p, const double a_p,
            const double b_p, const double c_p)
        : Pressure_RobinBC_SO_R(dx_p)
    {
        a = a_p;
        b = b_p;
        c = c_p;
    }
    virtual ~Pressure_RobinBC_SO_R() = default;
};

/*
 * This class is exclusively to set up boundary condition for velocity
 * when there is no pressure. In this case, V^{n+1} has already been
 * calculated, and there is only left to update u. This class provides a
 * way to set up u_{-1}^{n+1}, and the solution arrays V_0^{n+1} and
 * V_1^{n+1} are provided to do so
 */
class Velocity_WithoutPressure_L : public Bdry
{
private:
    /*
     * Overriden funtion from base class
     */
    void SetBoundaryCondition(
            double x, double tPrev, double tCurr, std::size_t idx) override;

protected:
    /*
     * Parameters used in this BC
     */
    double u_minus1;
    /*
     * This function MUST be defined by the user, once it sets the
     * values for a, b and c every call of setBoundaryCondition
     */
    virtual void SetBoundaryValues(
            double* /* V0 */, double* /* V1 */, double /* x */, double /* t */)
    {
    }

    Velocity_WithoutPressure_L() : Bdry(1), u_minus1() {}

public:
    Velocity_WithoutPressure_L(const double u_minus1_p)
        : Velocity_WithoutPressure_L()
    {
        u_minus1 = u_minus1_p;
    }
};
#endif /* SRC_BDRY_BDRY_H */
