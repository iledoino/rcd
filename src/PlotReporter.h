#ifndef SRC_INTERFACE_PLOTREPORTER_H
#define SRC_INTERFACE_PLOTREPORTER_H

#include "Reporter.h"
#include <memory>
#include <string>
#include <vector>

#ifndef ENABLE_OPENMP
#include <ctime>
#endif

class PlotReporter : public Reporter
{
public:
    PlotReporter();
    PlotReporter(std::vector<std::vector<std::size_t>> reportGroups_p,
            std::string xAxisName_p, std::vector<std::string> yAxesNames_p)
        : PlotReporter()
    {
        reportGroups_ = reportGroups_p;
        xAxisName_ = xAxisName_p;
        yAxesNames_ = yAxesNames_p;
    }
    virtual ~PlotReporter() noexcept = default;

    const std::vector<std::vector<std::size_t>>& reportGroups() const
    {
        return reportGroups_;
    }
    std::vector<std::vector<std::size_t>>& reportGroups()
    {
        return reportGroups_;
    }

    const std::vector<std::string>& yAxesNames() const { return yAxesNames_; }
    std::vector<std::string>& yAxesNames() { return yAxesNames_; }

    const std::string& xAxisName() const { return xAxisName_; }
    std::string& xAxisName() { return xAxisName_; }

    double& frameSecUpdate() { return frameSecUpdate_; }

    void start() override;
    bool timetomake() override;

    void spaceRange() override;
    void spaceRange(double xl, double xr) override;

protected:
#ifndef ENABLE_OPENMP
    std::clock_t prevRunTime;
#else
    double prevRunTime;
#endif
    std::vector<std::vector<std::size_t>> reportGroups_{ { 0 } };
    std::string xAxisName_{ "x" };
    std::vector<std::string> yAxesNames_{ { "unknown" } };
    std::vector<std::string> spaceNames_{ { "unknown-space" } };
    double xl_{ 0. }, xr_{ 1. };
    bool spaceRangeAutomatic_{ true };
    double frameSecUpdate_ = 0.25;
};

#endif /* SRC_INTERFACE_PLOTREPORTER_H */
